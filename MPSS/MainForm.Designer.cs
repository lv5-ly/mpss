﻿
using MPSS.Common.Model;

namespace MPSS
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.DBFileSelectPanel = new System.Windows.Forms.Panel();
            this.selectDBFileView = new MPSS.Controls.Views.SelectDBFileView();
            this.currentMonthControl = new MPSS.OptionPlayControl.CurrentMonthView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.StockAssetGridTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.StockView = new MPSS.Controls.Views.StockAssetView();
            this.QuitButton = new System.Windows.Forms.Button();
            this.StockPanel = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txbStkAssgnedPandL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txbCCPandL = new System.Windows.Forms.TextBox();
            this.StockDetailTab = new System.Windows.Forms.TabControl();
            this.StockSummaryTabPage = new System.Windows.Forms.TabPage();
            this.stockMonthlyStatistic = new MPSS.StatisticControl.StockMonthlyView();
            this.TransactionTabPage = new System.Windows.Forms.TabPage();
            this.StockTransactionView = new MPSS.Controls.Views.StockTransactionView();
            this.WatchdogTabPage = new System.Windows.Forms.TabPage();
            this.Chihuahua = new MPSS.Controls.Views.WatchdogView();
            this.ActiveOptionsTab = new System.Windows.Forms.TabPage();
            this.AOView = new MPSS.Controls.Views.ActiveOptionsView();
            this.MPSSToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pnlHeader.SuspendLayout();
            this.DBFileSelectPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.StockAssetGridTableLayoutPanel.SuspendLayout();
            this.StockPanel.SuspendLayout();
            this.StockDetailTab.SuspendLayout();
            this.StockSummaryTabPage.SuspendLayout();
            this.TransactionTabPage.SuspendLayout();
            this.WatchdogTabPage.SuspendLayout();
            this.ActiveOptionsTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlHeader.Controls.Add(this.DBFileSelectPanel);
            this.pnlHeader.Controls.Add(this.currentMonthControl);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1026, 97);
            this.pnlHeader.TabIndex = 0;
            // 
            // DBFileSelectPanel
            // 
            this.DBFileSelectPanel.Controls.Add(this.selectDBFileView);
            this.DBFileSelectPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.DBFileSelectPanel.Location = new System.Drawing.Point(0, 0);
            this.DBFileSelectPanel.Name = "DBFileSelectPanel";
            this.DBFileSelectPanel.Size = new System.Drawing.Size(192, 97);
            this.DBFileSelectPanel.TabIndex = 4;
            // 
            // selectDBFileView
            // 
            this.selectDBFileView.AutoSize = true;
            this.selectDBFileView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectDBFileView.Location = new System.Drawing.Point(0, 0);
            this.selectDBFileView.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.selectDBFileView.Name = "selectDBFileView";
            this.selectDBFileView.Size = new System.Drawing.Size(192, 97);
            this.selectDBFileView.TabIndex = 0;
            this.selectDBFileView.DBFileChanged += new System.EventHandler<System.EventArgs>(this.SelectDBFileView_DBFileChanged);
            // 
            // currentMonthControl
            // 
            this.currentMonthControl.Dock = System.Windows.Forms.DockStyle.Right;
            this.currentMonthControl.Location = new System.Drawing.Point(193, 0);
            this.currentMonthControl.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.currentMonthControl.Name = "currentMonthControl";
            this.currentMonthControl.Size = new System.Drawing.Size(833, 97);
            this.currentMonthControl.TabIndex = 3;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 97);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitContainer1.Panel1.Controls.Add(this.StockAssetGridTableLayoutPanel);
            this.splitContainer1.Panel1.Controls.Add(this.StockPanel);
            this.splitContainer1.Panel1MinSize = 50;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Panel2.Controls.Add(this.StockDetailTab);
            this.splitContainer1.Panel2MinSize = 60;
            this.splitContainer1.Size = new System.Drawing.Size(1026, 457);
            this.splitContainer1.SplitterDistance = 257;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 1;
            // 
            // StockAssetGridTableLayoutPanel
            // 
            this.StockAssetGridTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.StockAssetGridTableLayoutPanel.ColumnCount = 1;
            this.StockAssetGridTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.StockAssetGridTableLayoutPanel.Controls.Add(this.StockView, 0, 0);
            this.StockAssetGridTableLayoutPanel.Controls.Add(this.QuitButton, 0, 1);
            this.StockAssetGridTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockAssetGridTableLayoutPanel.Location = new System.Drawing.Point(0, 120);
            this.StockAssetGridTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.StockAssetGridTableLayoutPanel.Name = "StockAssetGridTableLayoutPanel";
            this.StockAssetGridTableLayoutPanel.RowCount = 2;
            this.StockAssetGridTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.StockAssetGridTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.StockAssetGridTableLayoutPanel.Size = new System.Drawing.Size(253, 333);
            this.StockAssetGridTableLayoutPanel.TabIndex = 13;
            // 
            // StockView
            // 
            this.StockView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.StockView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockView.Location = new System.Drawing.Point(6, 7);
            this.StockView.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.StockView.Name = "StockView";
            this.StockView.Size = new System.Drawing.Size(241, 278);
            this.StockView.TabIndex = 11;
            this.StockView.StockAssetListChanged += new System.EventHandler<MPSS.Common.Model.StockAssetEventArgs>(this.StockView_StockAssetListChanged);
            // 
            // QuitButton
            // 
            this.QuitButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.QuitButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.QuitButton.Location = new System.Drawing.Point(4, 295);
            this.QuitButton.Name = "QuitButton";
            this.QuitButton.Size = new System.Drawing.Size(245, 34);
            this.QuitButton.TabIndex = 12;
            this.QuitButton.Text = "Quit (It\'s Miller Time)";
            this.QuitButton.UseVisualStyleBackColor = false;
            this.QuitButton.Click += new System.EventHandler(this.QuitButton_Click);
            // 
            // StockPanel
            // 
            this.StockPanel.Controls.Add(this.label3);
            this.StockPanel.Controls.Add(this.label2);
            this.StockPanel.Controls.Add(this.txbStkAssgnedPandL);
            this.StockPanel.Controls.Add(this.label1);
            this.StockPanel.Controls.Add(this.txbCCPandL);
            this.StockPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.StockPanel.Location = new System.Drawing.Point(0, 0);
            this.StockPanel.Name = "StockPanel";
            this.StockPanel.Size = new System.Drawing.Size(253, 120);
            this.StockPanel.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Simulated P&&L";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Monthly P&&L - ALL Stk assignments";
            // 
            // txbStkAssgnedPandL
            // 
            this.txbStkAssgnedPandL.Location = new System.Drawing.Point(66, 76);
            this.txbStkAssgnedPandL.Margin = new System.Windows.Forms.Padding(2);
            this.txbStkAssgnedPandL.Name = "txbStkAssgnedPandL";
            this.txbStkAssgnedPandL.Size = new System.Drawing.Size(83, 23);
            this.txbStkAssgnedPandL.TabIndex = 3;
            this.txbStkAssgnedPandL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Monthly P&&L - ALL expired CCs";
            // 
            // txbCCPandL
            // 
            this.txbCCPandL.BackColor = System.Drawing.SystemColors.Control;
            this.txbCCPandL.Location = new System.Drawing.Point(66, 38);
            this.txbCCPandL.Margin = new System.Windows.Forms.Padding(2);
            this.txbCCPandL.Name = "txbCCPandL";
            this.txbCCPandL.Size = new System.Drawing.Size(83, 23);
            this.txbCCPandL.TabIndex = 1;
            this.txbCCPandL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // StockDetailTab
            // 
            this.StockDetailTab.Controls.Add(this.StockSummaryTabPage);
            this.StockDetailTab.Controls.Add(this.TransactionTabPage);
            this.StockDetailTab.Controls.Add(this.WatchdogTabPage);
            this.StockDetailTab.Controls.Add(this.ActiveOptionsTab);
            this.StockDetailTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockDetailTab.Location = new System.Drawing.Point(0, 0);
            this.StockDetailTab.Name = "StockDetailTab";
            this.StockDetailTab.SelectedIndex = 0;
            this.StockDetailTab.Size = new System.Drawing.Size(764, 453);
            this.StockDetailTab.TabIndex = 10;
            // 
            // StockSummaryTabPage
            // 
            this.StockSummaryTabPage.Controls.Add(this.stockMonthlyStatistic);
            this.StockSummaryTabPage.Location = new System.Drawing.Point(4, 24);
            this.StockSummaryTabPage.Name = "StockSummaryTabPage";
            this.StockSummaryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.StockSummaryTabPage.Size = new System.Drawing.Size(756, 425);
            this.StockSummaryTabPage.TabIndex = 0;
            this.StockSummaryTabPage.Text = "Summary";
            this.StockSummaryTabPage.UseVisualStyleBackColor = true;
            // 
            // stockMonthlyStatistic
            // 
            this.stockMonthlyStatistic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stockMonthlyStatistic.Location = new System.Drawing.Point(3, 3);
            this.stockMonthlyStatistic.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.stockMonthlyStatistic.Name = "stockMonthlyStatistic";
            this.stockMonthlyStatistic.Size = new System.Drawing.Size(750, 419);
            this.stockMonthlyStatistic.TabIndex = 8;
            // 
            // TransactionTabPage
            // 
            this.TransactionTabPage.Controls.Add(this.StockTransactionView);
            this.TransactionTabPage.Location = new System.Drawing.Point(4, 24);
            this.TransactionTabPage.Name = "TransactionTabPage";
            this.TransactionTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.TransactionTabPage.Size = new System.Drawing.Size(756, 401);
            this.TransactionTabPage.TabIndex = 1;
            this.TransactionTabPage.Text = "Transaction";
            this.TransactionTabPage.UseVisualStyleBackColor = true;
            // 
            // StockTransactionView
            // 
            this.StockTransactionView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockTransactionView.Location = new System.Drawing.Point(3, 3);
            this.StockTransactionView.Margin = new System.Windows.Forms.Padding(2);
            this.StockTransactionView.Name = "StockTransactionView";
            this.StockTransactionView.Size = new System.Drawing.Size(750, 395);
            this.StockTransactionView.TabIndex = 9;
            this.StockTransactionView.TransactionListChanged += new System.EventHandler<MPSS.Common.Model.StockTransactionEventArgs>(this.StockTransactionView_TransactionListChanged);
            this.StockTransactionView.TransactionClosed += new System.EventHandler<MPSS.Common.Model.StockTransactionEventArgs>(this.StockTransactionView_TransactionClosed);
            // 
            // WatchdogTabPage
            // 
            this.WatchdogTabPage.Controls.Add(this.Chihuahua);
            this.WatchdogTabPage.Location = new System.Drawing.Point(4, 24);
            this.WatchdogTabPage.Name = "WatchdogTabPage";
            this.WatchdogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.WatchdogTabPage.Size = new System.Drawing.Size(756, 401);
            this.WatchdogTabPage.TabIndex = 4;
            this.WatchdogTabPage.Text = "Watchdog";
            this.WatchdogTabPage.UseVisualStyleBackColor = true;
            // 
            // Chihuahua
            // 
            this.Chihuahua.AutoSize = true;
            this.Chihuahua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Chihuahua.Location = new System.Drawing.Point(3, 3);
            this.Chihuahua.Name = "Chihuahua";
            this.Chihuahua.Size = new System.Drawing.Size(750, 395);
            this.Chihuahua.TabIndex = 0;
            this.Chihuahua.ScanResultChanged += new System.EventHandler<MPSS.Common.Model.ScanResultEventArgs>(this.PotentialView_ScanResultChanged);
            // 
            // ActiveOptionsTab
            // 
            this.ActiveOptionsTab.Controls.Add(this.AOView);
            this.ActiveOptionsTab.Location = new System.Drawing.Point(4, 24);
            this.ActiveOptionsTab.Name = "ActiveOptionsTab";
            this.ActiveOptionsTab.Padding = new System.Windows.Forms.Padding(3);
            this.ActiveOptionsTab.Size = new System.Drawing.Size(756, 401);
            this.ActiveOptionsTab.TabIndex = 5;
            this.ActiveOptionsTab.Text = "Options";
            this.ActiveOptionsTab.UseVisualStyleBackColor = true;
            // 
            // AOView
            // 
            this.AOView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AOView.Location = new System.Drawing.Point(3, 3);
            this.AOView.Name = "AOView";
            this.AOView.Size = new System.Drawing.Size(750, 395);
            this.AOView.TabIndex = 0;
            this.AOView.TransactionClosed += new System.EventHandler<MPSS.Common.Model.StockTransactionEventArgs>(this.AOView_TransactionClosed);
            // 
            // MPSSToolTip
            // 
            this.MPSSToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.MPSSToolTip.ToolTipTitle = "Help";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1026, 554);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.pnlHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MPSS";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.pnlHeader.ResumeLayout(false);
            this.DBFileSelectPanel.ResumeLayout(false);
            this.DBFileSelectPanel.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.StockAssetGridTableLayoutPanel.ResumeLayout(false);
            this.StockPanel.ResumeLayout(false);
            this.StockPanel.PerformLayout();
            this.StockDetailTab.ResumeLayout(false);
            this.StockSummaryTabPage.ResumeLayout(false);
            this.TransactionTabPage.ResumeLayout(false);
            this.WatchdogTabPage.ResumeLayout(false);
            this.WatchdogTabPage.PerformLayout();
            this.ActiveOptionsTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolTip MPSSToolTip;
        private StatisticControl.StockMonthlyView stockMonthlyStatistic;
        private OptionPlayControl.CurrentMonthView currentMonthControl;
        private System.Windows.Forms.Panel StockPanel;
        private System.Windows.Forms.Panel DBFileSelectPanel;
        private Controls.Views.SelectDBFileView selectDBFileView;
        private Controls.Views.StockTransactionView StockTransactionView;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbCCPandL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbStkAssgnedPandL;
        private System.Windows.Forms.TabControl StockDetailTab;
        private System.Windows.Forms.TabPage StockSummaryTabPage;
        private System.Windows.Forms.TabPage TransactionTabPage;
        private Controls.Views.StockAssetView StockView;
        private System.Windows.Forms.Button QuitButton;
        private System.Windows.Forms.TableLayoutPanel StockAssetGridTableLayoutPanel;
        private System.Windows.Forms.TabPage WatchdogTabPage;
        private Controls.Views.WatchdogView Chihuahua;
        private System.Windows.Forms.TabPage ActiveOptionsTab;
        private Controls.Views.ActiveOptionsView AOView;
        //private System.Windows.Forms.TabPage c;
    }
}

