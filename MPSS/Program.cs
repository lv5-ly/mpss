using Microsoft.Extensions.DependencyInjection;
using MPSS.Properties;
using System;
using System.Windows.Forms;

namespace MPSS
{
    static class Program
    {
        private static IServiceProvider ServiceProvider { get; set; }

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Settings.Default.upgradeRequired = false;

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConfigureServices();
            // Application.Run(new MainForm());
            Application.Run((MainForm)ServiceProvider.GetService(typeof(MainForm)));
        }

        private static void ConfigureServices()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();
            services.AddSingleton<MainForm>();
            ServiceProvider = services.BuildServiceProvider();
        }

        public static void UpgradeUserSettings()
        {
            if (Settings.Default.upgradeRequired)
            {
                Settings.Default.Upgrade();
                Settings.Default.upgradeRequired = false;
                Settings.Default.Save();
            }
        }
    }
}
