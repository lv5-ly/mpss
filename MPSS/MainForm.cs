﻿using MPSS.Common.DataService;
using MPSS.Common.Model;
using MPSS.Controls.Views;
using MPSS.DataAccess;
using MPSS.Properties;
using MPSS.ViewModel;
using System;
using System.Windows.Forms;

namespace MPSS
{
    public partial class MainForm : Form
    {
        #region Properties
        private StockAssetViewModel ApplicationViewModel { get; set; }
        private readonly IStockAssetDataProvider dataProvider;
        private DBFile CurrentDatabaseFile;
        //private static IHttpClientFactory ClientFactory;
        #endregion Properties

        #region Constructor
        // public MainForm(IHttpClientFactory factory)
        public MainForm()
        {
            InitializeComponent();
            //ClientFactory = factory;

            // Put the version on the title
            GetVersion();

            // Let's retrieve from the Settings first
            CurrentDatabaseFile = GetFromSettings();

            if (!CurrentDatabaseFile.IsValid)
            {
                // Retrieve the currently selected DBFile name
                CurrentDatabaseFile = selectDBFileView.GetDBFile();
                SaveToSettings(CurrentDatabaseFile);
            }
            else
            {
                selectDBFileView.SetDBFile(CurrentDatabaseFile);
            }

            dataProvider = new SQLiteDataProvider(CurrentDatabaseFile);
            ApplicationViewModel = new StockAssetViewModel(dataProvider);

            ApplicationViewModel.Load();

            StockView.SetViewModel(ApplicationViewModel);

            StockTransactionView.SetViewModel(ApplicationViewModel);

            // Chihuahua.SetViewModel(ClientFactory, ApplicationViewModel);
            Chihuahua.SetViewModel(ApplicationViewModel);

            AOView.Refresh(ApplicationViewModel.GetActiveOptionTransactions());
        }

        private void ReloadDBfile()
        {
            if (ApplicationViewModel != null)
            {
                ApplicationViewModel.SwitchDatabaseFile(new SQLiteDataProvider(CurrentDatabaseFile));

                ApplicationViewModel.StockAssetBindingList.Clear();

                ApplicationViewModel.Load();

                StockTransactionView.SetViewModel(ApplicationViewModel);

                RefreshSimulatedPandL(ApplicationViewModel);

                MainForm_Load(null, null);

                // Now, we can update the application settings
                SaveToSettings(CurrentDatabaseFile);

                AOView.Refresh(ApplicationViewModel.GetActiveOptionTransactions());
            }
        }

        private static void SaveToSettings(DBFile databaseFile)
        {
            Settings.Default.DBFile = databaseFile.GetFileName();
            Settings.Default.Save();
        }

        /// <summary>
        /// Read from the settings, the name of the database file
        /// and construct the DBFile object out of that
        /// </summary>
        /// <returns></returns>
        private static DBFile GetFromSettings()
        {
            return new DBFile(Settings.Default.DBFile);
        }
        #endregion Constructor

        #region Private Methods
        private void GetVersion()
        {
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            this.Text = $"MPSS v{version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // When we first load the main form, we want to let the Stock Monthly Statistic control
            //  know the select stock asset
            stockMonthlyStatistic.SetStockAsset(ApplicationViewModel.CurrentSelectedAsset);
            RefreshAllPanels(ApplicationViewModel);
        }

        private void RefreshAllPanels(StockAssetViewModel vModel)
        {
            //RefreshStockAssetGrid(vModel.currentSelectedAsset);

            // We can only refresh the panel if there is 
            //  at least a transaction
            if (vModel.CurrentSelectedTransaction != null)
            {
                StockTransactionView.Refresh();
            }
            NotifyStockMonthlyStatisticControl(vModel.CurrentSelectedAsset);

            // Calculate Simulated P&L
            RefreshSimulatedPandL(vModel);

            // We also need to tell the 
            currentMonthControl.Refresh(vModel);

            AOView.Refresh(ApplicationViewModel.GetActiveOptionTransactions());
        }

        private void RefreshSimulatedPandL(StockAssetViewModel vModel)
        {
            /// Loop thru all the stock assets and add up the unrealized CC premium and Realized P&L
            /// Get the current Month and Year in integer form
            int currentMonth = DateTime.Now.Month;
            int currentYear = DateTime.Now.Year;
            decimal expiredOptionProfit = 0;


            foreach (var s in vModel.StockAssetBindingList)
            {
                expiredOptionProfit = expiredOptionProfit + s.GetExpiredPandL() + s.GetMonthlyPandL(currentMonth, currentYear);
            }
            txbCCPandL.Text = expiredOptionProfit.ToString("C");

            /// Loop thru all the stocks and add up stock assignment P&Ls
            decimal assignedStkPandL = 0;
            foreach (var s in vModel.StockAssetBindingList)
            {
                assignedStkPandL += s.GetStockAssignedPandL();
            }
            txbStkAssgnedPandL.Text = (expiredOptionProfit + assignedStkPandL).ToString("C");
        }

        private void NotifyStockMonthlyStatisticControl(StockAsset currentSelectedAsset)
        {
            // When we first load the main form, we want to let the Stock Monthly Statistic control
            //  know the select stock asset
            stockMonthlyStatistic.SetStockAsset(currentSelectedAsset);
            stockMonthlyStatistic.RefreshStockStatistics();
        }

        #endregion Private Methods

        #region Event Handlers

        /// <summary>
        /// Whenever transaction list is changed, due to:
        /// - Edit
        /// - Close
        /// - Delete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StockTransactionView_TransactionListChanged(object sender, EventArgs e)
        {
            ApplicationViewModel.Load();

            //RefreshStockAssetGrid(viewModel.currentSelectedAsset);

            NotifyStockMonthlyStatisticControl(ApplicationViewModel.CurrentSelectedAsset);

            // We also need to tell the 
            currentMonthControl.Refresh(ApplicationViewModel);

            RefreshSimulatedPandL(ApplicationViewModel);

            StockView.Refresh();
            //StockSimulatedPLChart.Refresh();
        }

        private void StockView_StockAssetListChanged(object sender, StockAssetEventArgs e)
        {
            StockTransactionView.LoadStockTransactions();

            NotifyStockMonthlyStatisticControl(ApplicationViewModel.CurrentSelectedAsset);

            // We also need to tell the 
            currentMonthControl.Refresh(ApplicationViewModel);

            RefreshSimulatedPandL(ApplicationViewModel);

            //StockTransactionProfitChart.SetViewModel(ApplicationViewModel.CurrentSelectedAsset.Transactions);

            //if (ApplicationViewModel.CurrentSelectedAsset != null)
            //{
            //    CurrentStockQuote.SetStockSymbol(ApplicationViewModel.CurrentSelectedAsset.Symbol);
            //}

            //StockSimulatedPLChart.Refresh();
            AOView.Refresh(ApplicationViewModel.GetActiveOptionTransactions());
        }

        private void SelectDBFileView_DBFileChanged(object sender, EventArgs e)
        {
            CurrentDatabaseFile = sender as DBFile;

            ReloadDBfile();
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion Event Handlers

        private void PotentialView_ScanResultChanged(object sender, ScanResultEventArgs e)
        {
            var t = $"Watchdog";
            //var bc = Color.White;
            if (e.NumberOfPossiblePlays > 0)
            {
                t += $" ({e.NumberOfPossiblePlays})";
                //bc = Color.LightGreen;

            }
            WatchdogTabPage.Text = t;
            //WatchdogTabPage.BackColor = bc;
        }

        private void StockTransactionView_TransactionClosed(object sender, StockTransactionEventArgs e)
        {
            CloseTransaction(e);
        }

        private void CloseTransaction(StockTransactionEventArgs e)
        {
            // Tell the viewModel to close out the currently selected
            //  transaction, with this child transaction
            ApplicationViewModel.CloseTransaction(e.CurrentAssetTransaction, e.ChildTransaction);

            /// Reload the ViewModel to include the closed trans before Refresh
            ApplicationViewModel.Load();

            RefreshAllPanels(ApplicationViewModel);
        }

        private void AOView_TransactionClosed(object sender, StockTransactionEventArgs e)
        {
            CloseTransaction(e);
        }
    }
}
