﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MPSS.Common.Model;

namespace MPSS.Common.DataService.Tests
{
    [TestClass]
    public class FinnHubStockDataServiceTests
    {
        private readonly IStockDataService service = new FinnHubStockDataService(new StockQuoteCache());

        [TestMethod()]
        public void GetStockPriceTest()
        {
            var result = service.GetStockPriceAsync("INO").Result;
            Assert.IsNotNull(result, "Should not be null.");
        }
    }
}