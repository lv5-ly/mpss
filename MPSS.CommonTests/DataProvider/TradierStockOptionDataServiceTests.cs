﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MPSS.Common.Model;
using System;

namespace MPSS.Common.DataProvider.Tests
{
    [TestClass()]
    public class TradierStockOptionDataServiceTests
    {
        private readonly TradierStockOptionDataService service = new(new OptionChainCache());

        [TestMethod()]
        public void GetOptionChainTest()
        {
            var expirationDate = new DateTime(2021, 04, 16);
            var result = service.GetOptionChain("AAPL", expirationDate);
            Assert.IsNotNull(result, "should not be null");
        }
    }
}