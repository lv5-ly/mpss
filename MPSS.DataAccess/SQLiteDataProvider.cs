﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using MPSS.Common.DataProvider;
using MPSS.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MPSS.DataAccess
{
    public class SQLiteDataProvider : IStockAssetDataProvider
    {
        public SQLiteDataProvider()
        {
            //using(var db = new ApplicationDbContext())
            //{
            //    if (db.Database.GetService<IRelationalDatabaseCreator>().Exists())
            //    {

            //    }
                
            //    db.AssetTransactions.Add(
            //        new AssetTransaction
            //        {
            //            //TransactionId = 1,
            //            AssetSymbol = "AAPL",
            //            Price = 1.50,
            //            TransactionAction = AssetTransaction.AssetTransactionAction.Buy,
            //            TransactionDate = DateTime.Now,
            //            Quantity = 100,
            //            TransactionType = AssetTransaction.AssetTransactionType.Share
            //        });
            //    db.SaveChanges();

            //    AddNewStockAsset(db.AssetTransactions.First());
            //}
        }

        /// <summary>
        /// Data layer is to persist this record
        /// </summary>
        /// <param name="t">Transaction</param>
        /// <returns>The persisted transaction</returns>
        public AssetTransaction AddAssetTransaction(AssetTransaction t)
        {
            EntityEntry<AssetTransaction> entry = null;
            using (var db = new ApplicationDbContext())
            {
                var sa = db.StockAssets
                    .Include("Transactions")
                    .FirstOrDefault(r => r.StockAssetId == t.Stock.StockAssetId);
                //var sa = t.Stock;
                if (t.TransactionType == AssetTransactionType.Share)
                {
                    sa.Shares += (t.TransactionAction == AssetTransactionAction.Bought) ? t.Quantity :
                        (-1 * t.Quantity);
                    db.SaveChanges();
                }
                var newTransaction = new AssetTransaction(t, sa);
                entry = db.AssetTransactions.Add(newTransaction);
                db.SaveChanges();
            }
            return entry.Entity;
        }

        public StockAsset AddNewStockAsset(AssetTransaction t)
        {
            StockAsset sa = new(t);
            using (var db = new ApplicationDbContext())
            {
                db.AssetTransactions.Add(new AssetTransaction(t, db.StockAssets.Add(sa).Entity));
                db.SaveChanges();
            }
            //StockAsset sa = new StockAsset()
            //{
            //    Shares = t.Quantity,
            //    ProfitLoss = (t.Quantity * t.Price),
            //    Symbol = t.AssetSymbol,
            //};
            //sa.StockAssetId = .Count() + 1;
            //sa.Transactions = new List<AssetTransaction>();
            //sa.Transactions.AddRange(assetTransactions.Where(t => t.AssetSymbol == sa.Symbol));
            //stockAssets.Add(sa);

            return sa;
        }

        /// <summary>
        /// Have data provider to create a StockAsset record
        /// </summary>
        /// <param name="assetSymbol">Stock symbol</param>
        /// <returns>New Stock Asset record index</returns>
        public StockAsset AddNewStockAsset(string assetSymbol)
        {
            EntityEntry<StockAsset> entry = null;
            using (var db = new ApplicationDbContext())
            {
                entry = db.StockAssets.Add(new StockAsset(assetSymbol));
                db.SaveChanges();
            }
            return entry.Entity;
        }

        public void DeleteTransaction(int transactionId)
        {
            using var db = new ApplicationDbContext();
            var t = db.AssetTransactions.FirstOrDefault(r => r.TransactionId == transactionId);
            if (t != null)
            {
                db.Remove(t);
                db.SaveChanges();
            }
        }

        public bool IsNewStockAsset(string assetSymbol)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<AssetTransaction> LoadAssetTransactions(string assetSymbol)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<StockAsset> LoadStockAssets()
        {
            IEnumerable<StockAsset> result;
            using(var db = new ApplicationDbContext())
            {
                result = db.StockAssets.Include("Transactions").ToList();
            }
            return result;
        }

        public StockAsset MergeAssetTransaction(AssetTransaction t)
        {
            throw new System.NotImplementedException();
        }

        public StockAsset ReloadStockAsset(int stockAssetId)
        {
            StockAsset asset = null;
            using(var db = new ApplicationDbContext())
            {
                asset = db.StockAssets
                    .Include("Transactions")
                    .FirstOrDefault(a => a.StockAssetId == stockAssetId);
            }
            return asset;
        }


        /// <summary>
        /// Remove a stock asset record from database, by Id
        /// </summary>
        /// <param name="stockAssetId">Stock record Id</param>
        public void RemoveStockAsset(int stockAssetId)
        {
            using var db = new ApplicationDbContext();
            var s = db.StockAssets.FirstOrDefault(r => r.StockAssetId == stockAssetId);
            if (s != null)
            {
                // Can't figure out how to tell EF to do 
                //  cascading delete, so here we have to remove 
                //  all the transactions first
                var transactions = db.AssetTransactions
                    .Where(r => r.Stock == s);
                foreach(var t in transactions)
                {
                    db.AssetTransactions.Remove(t);
                }
                db.StockAssets.Remove(s);
                db.SaveChanges();
            }
        }

        public void SaveAssetTransaction(AssetTransaction assetTransaction)
        {
            throw new System.NotImplementedException();
        }

        public void SaveStockAsset(StockAsset asset)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Update & persist the stock record
        /// Usually, only (number of) shares is only property get updated
        /// </summary>
        /// <param name="stock">Stock entity</param>
        public void UpdateStock(StockAsset stock)
        {
            using var db = new ApplicationDbContext();
            var s = db.StockAssets.FirstOrDefault(r => r.StockAssetId == stock.StockAssetId);
            if (s != null)
            {
                s.Shares = stock.Shares;
                db.SaveChanges();
            }
        }

        public void UpdateTransaction(AssetTransaction currentSelectedTransaction)
        {
            using var db = new ApplicationDbContext();
            var t = db.AssetTransactions.FirstOrDefault(r => r.TransactionId == currentSelectedTransaction.TransactionId);
            if (t != null)
            {
                // User can change following properties
                t.ExpiredDate = currentSelectedTransaction.ExpiredDate;
                t.Fee = currentSelectedTransaction.Fee;
                t.Price = currentSelectedTransaction.Price;
                t.Quantity = currentSelectedTransaction.Quantity;
                t.StrikePrice = currentSelectedTransaction.StrikePrice;
                t.TransactionDate = currentSelectedTransaction.TransactionDate;
                // t.ParentTransactionId = currentSelectedTransaction.ParentTransactionId;
                // t.TransactionType = currentSelectedTransaction.TransactionType;
                // t.TransactionAction = currentSelectedTransaction.TransactionAction;

                // The application update the following properties
                t.Status = currentSelectedTransaction.Status;
                t.Active = currentSelectedTransaction.Active;
                t.Hide = currentSelectedTransaction.Hide;

                db.SaveChanges();
            }
        }
    }
}
