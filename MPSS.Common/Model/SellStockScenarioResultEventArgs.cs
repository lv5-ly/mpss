﻿using System.Collections.Generic;

namespace MPSS.Common.Model
{
    public class SellStockScenarioResultEventArgs
    {
        public SellStockScenarioResultEventArgs(Dictionary<string, decimal> values)
        {
            Values = values;
        }

        public Dictionary<string, decimal> Values { get; }

    }
}
