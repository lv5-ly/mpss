﻿using Newtonsoft.Json;

namespace MPSS.Common.Model.FinnHub
{
    /// <summary>
    /// Quote object from FinnHub
    /// </summary>
    public class Quote
    {
        /// <summary>
        /// Open price of the day
        /// </summary>
        [JsonProperty("o")]
        public decimal OpenPrice { get; set; }


        /// <summary>
        /// High price of the day
        /// </summary>
        [JsonProperty("h")]
        public decimal HighPrice { get; set; }


        /// <summary>
        /// Low price of the day
        /// </summary>
        [JsonProperty("l")]
        public decimal LowPrice { get; set; }


        /// <summary>
        /// Current price
        /// </summary>
        [JsonProperty("c")]
        public decimal CurrentPrice { get; set; }


        /// <summary>
        /// Previous close price
        /// </summary>
        [JsonProperty("pc")]
        public decimal PreviousClosePrice { get; set; }

        [JsonIgnore]
        public decimal PriceChange
        {
            get
            {
                return CurrentPrice - PreviousClosePrice;
            }
        }
    }
}
