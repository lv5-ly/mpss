﻿using System;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Stock transaction event arguments, to use in an Stock Transaction View user custom control 
    /// </summary>
    public class StockTransactionEventArgs : EventArgs
    {
        public StockTransactionEventArgs(AssetTransaction t)
        {
            CurrentAssetTransaction = t;
        }

        public StockTransactionEventArgs(AssetTransaction t, AssetTransaction childTransaction) : this(t)
        {
            ChildTransaction = childTransaction;
        }

        public StockTransactionEventArgs(object selectedRowItem, string name)
        {
            var activeOption = selectedRowItem as ActiveOptionTransaction;
            CurrentAssetTransaction = activeOption.GetTransaction();
            SelectedContextMenuItemName = name;
        }

        public AssetTransaction CurrentAssetTransaction { get; set; }
        public AssetTransaction ChildTransaction { get; }
        public string SelectedContextMenuItemName { get; }
    }
}
