﻿using System;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Attribute for the property in a DataGridView
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DisplayFormatAttribute : Attribute
    {
        public DisplayFormatAttribute(string format)
        {
            Format = format;
        }
        public string Format { get; set; }
    }
}