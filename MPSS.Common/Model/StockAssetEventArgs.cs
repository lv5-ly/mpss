﻿using System;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Stock asset event arguments, to use in an Stock Asset View user custom control 
    /// </summary>
    public class StockAssetEventArgs : EventArgs
    {
        public StockAssetEventArgs(StockAsset sa)
        {
            SelectStockAsset = sa;
        }

        public StockAsset SelectStockAsset { get; set; }
    }
}
