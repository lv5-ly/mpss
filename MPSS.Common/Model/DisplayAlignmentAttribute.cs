﻿using System;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Attribute for the property in a DataGridView
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DisplayAlignmentAttribute : Attribute
    {
        public DisplayAlignmentAttribute(DisplayAlignment alignment)
        {
            Alignment = alignment;
        }
        public DisplayAlignment Alignment { get; set; }
    }

}
