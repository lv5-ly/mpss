﻿using Newtonsoft.Json;

namespace MPSS.Common.Model.Tradier
{
    public class StockQuotes
    {
        [JsonProperty("quote")]
        public StockQuote Quote { get; set; }
    }
}
