﻿using Newtonsoft.Json;

namespace MPSS.Common.Model.Tradier
{
    public class QuotesRoot
    {
        [JsonProperty("quotes")]
        public StockQuotes Quotes { get; set; }
    }
}
