﻿using Newtonsoft.Json;

namespace MPSS.Common.Model.Tradier
{
    public class OptionChain
    {
        [JsonProperty("options")]
        public Options Options { get; set; }
    }
}
