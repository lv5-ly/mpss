﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MPSS.Common.Model.Tradier
{
    public class Options
    {
        [JsonProperty("option")]
        public List<Option> Option { get; set; }
    }
}