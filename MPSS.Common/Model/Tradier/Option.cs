﻿using System;

namespace MPSS.Common.Model.Tradier
{
    public class Option : StockQuote
    {
        #region Public Methods
        public override string ToString()
        {
            if (OptionType == "call")
            {
                return $"Ask: {Ask:C}";
            }
            return string.Empty;
        }

        public string ToCallOptionString()
        {
            if (OptionType == "call")
            {
                var d = Convert.ToDateTime(ExpirationDate);
                return $"Exp. {d:M} @{Strike:N} Ask: {Ask:C}";
            }
            return string.Empty;
        }
        #endregion Public Methods
    }
}