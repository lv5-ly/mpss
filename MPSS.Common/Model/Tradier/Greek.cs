﻿using Newtonsoft.Json;
using System;

namespace MPSS.Common.Model.Tradier
{
    public class Greek
    {
        /// <summary>
        /// Delta
        /// </summary>
        [JsonProperty("delta")]
        public string Delta { get; set; }
        [JsonProperty("gamma")]
        public string Gamma { get; set; }
        [JsonProperty("theta")]
        public string Theta { get; set; }
        [JsonProperty("vega")]
        public string Vega { get; set; }
        [JsonProperty("rho")]
        public string Rho { get; set; }
        [JsonProperty("phi")]
        public string Phi { get; set; }
        /// <summary>
        /// Bid implied volatility
        /// </summary>
        [JsonProperty("bid_iv")]
        public string BidIV { get; set; }
        /// <summary>
        /// Mid implied volatility
        /// </summary>
        [JsonProperty("mid_iv")]
        public string MidIV { get; set; }
        /// <summary>
        /// Ask implied volatility
        /// </summary>
        [JsonProperty("ask_iv")]
        public string AskIV { get; set; }
        /// <summary>
        /// ORATS final implied volatility
        /// </summary>
        [JsonProperty("smv_vol")]
        public string SmvVol { get; set; }
        /// <summary>
        /// Date volatility data was last updated
        /// </summary>
        [JsonProperty("update_at")]
        public DateTime UpdateAt { get; set; }
    }
}