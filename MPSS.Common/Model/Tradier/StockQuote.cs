﻿using Newtonsoft.Json;
using System;

namespace MPSS.Common.Model.Tradier
{
    public class StockQuote
    {
        #region Properties
        /// <summary>
        /// Security symbol
        /// </summary>
        [JsonProperty("symbol")]
        public string Symbol { get; set; }
        /// <summary>
        /// Security detailed name or description
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// Exchange code of the security
        /// </summary>
        [JsonProperty("exch")]
        public string Exch { get; set; }
        /// <summary>
        /// Type, one of: stock, option, etf, index, mutual_fund
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
        /// <summary>
        /// Last price
        /// </summary>
        [JsonProperty("last")]
        public decimal? Last { get; set; }
        /// <summary>
        /// Change(in dollars)
        /// </summary>
        [JsonProperty("change")]
        public decimal? Change { get; set; }
        /// <summary>
        /// Current day's volume
        /// </summary>
        [JsonProperty("volume")]
        public int Volume { get; set; }
        /// <summary>
        /// Open price of the current session
        /// </summary>
        [JsonProperty("open")]
        public decimal? Open { get; set; }
        /// <summary>
        /// High price of the current session
        /// </summary>
        [JsonProperty("high")]
        public decimal? High { get; set; }
        /// <summary>
        /// Low price of the current session
        /// </summary>
        [JsonProperty("low")]
        public decimal? Low { get; set; }
        /// <summary>
        /// Close price of the current session
        /// </summary>
        [JsonProperty("close")]
        public decimal? Close { get; set; }
        /// <summary>
        /// Bid price 
        /// </summary>
        [JsonProperty("bid")]
        public decimal? Bid { get; set; }
        /// <summary>
        /// Ask price 
        /// </summary>
        [JsonProperty("ask")]
        public decimal? Ask { get; set; }
        /// <summary>
        /// Ask security of the option(if applicable)
        /// </summary>
        [JsonProperty("underlying")]
        public string Underlying { get; set; }
        /// <summary>
        /// Strike price of the option(if applicable)
        /// </summary>
        [JsonProperty("strike")]
        public decimal? Strike { get; set; }
        /// <summary>
        /// Change(in percent)
        /// </summary>
        [JsonProperty("change_percentage")]
        public decimal? ChangePercentage { get; set; }
        /// <summary>
        /// 90 day average volume of the security
        /// </summary>
        [JsonProperty("average_volume")]
        public int AverageVolume { get; set; }
        /// <summary>
        /// Volume of the last price
        /// </summary>
        [JsonProperty("last_volume")]
        public int LastVolume { get; set; }
        /// <summary>
        /// Most recent trade date
        /// </summary>
        [JsonProperty("trade_date")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime TradeDate { get; set; }
        /// <summary>
        /// Previous close price
        /// </summary>
        [JsonProperty("prevclose")]
        public decimal? PrevClose { get; set; }
        /// <summary>
        /// 52 week high price
        /// </summary>
        [JsonProperty("week_52_high")]
        public decimal? Week52High { get; set; }
        /// <summary>
        /// 52 week low price
        /// </summary>
        [JsonProperty("week_52_low")]
        public decimal? Week52Low { get; set; }
        /// <summary>
        /// Size of bid(in hundreds)
        /// </summary>
        [JsonProperty("bid_size")]
        public int BidSize { get; set; }
        /// <summary>
        /// Bid exchange code
        /// </summary>
        [JsonProperty("bidexch")]
        public string BidExch { get; set; }
        /// <summary>
        /// Date of bid price
        /// </summary>
        [JsonProperty("bid_date")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime BidDate { get; set; }
        /// <summary>
        /// Size of ask(in hundreds)
        /// </summary>
        [JsonProperty("asksize")]
        public int AskSize { get; set; }
        /// <summary>
        /// Ask exchange code
        /// </summary>
        [JsonProperty("askexch")]
        public string AskExch { get; set; }
        /// <summary>
        /// Date of ask price
        /// </summary>
        [JsonProperty("ask_date")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime AskDate { get; set; }
        /// <summary>
        /// Open interest in option(if applicable)
        /// </summary>
        [JsonProperty("open_interest")]
        public int OpenInterest { get; set; }
        /// <summary>
        /// Size of the option contract(typically 100)
        /// </summary>
        [JsonProperty("contract_size")]
        public int ContractSize { get; set; }
        /// <summary>
        /// Expiration date of option(if applicable)
        /// </summary>
        [JsonProperty("expiration_date")]
        //[JsonConverter(typeof(CustomDateTimeConverter))]
        public string ExpirationDate { get; set; }
        /// <summary>
        /// Expiration type of option(if applicable)
        /// </summary>
        [JsonProperty("expiration_type")]
        public string ExpirationType { get; set; }
        /// <summary>
        /// Type of the option, one of: put, call
        /// </summary>
        [JsonProperty("option_type")]
        public string OptionType { get; set; }
        /// <summary>
        /// Comma-delimited list of option root symbols for an underlier
        /// </summary>
        [JsonProperty("root_symbols")]
        public string RootSymbols { get; set; }
        /// <summary>
        /// Root symbol of the option(if applicable)
        /// </summary>
        [JsonProperty("root_symbol")]
        public string RootSymbol { get; set; }

        [JsonProperty("greeks")]
        public Greek Greeks { get; set; }
        #endregion Properties

        public override string ToString()
        {
            return $"Ask: {Ask:C}";
        }
    }
}
