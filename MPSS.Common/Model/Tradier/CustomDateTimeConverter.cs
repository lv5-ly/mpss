﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace MPSS.Common.Model.Tradier
{
    public class CustomDateTimeConverter : DateTimeConverterBase
    {
        //private const string format = "yyyy-MM-ddTHH:mm:ss.FFFFFFZ";

        //public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        //{
        //    writer.WriteValue(((DateTime)value).ToString(format));
        //}
        //public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        //{
        //    if (reader.Value == null)
        //        return null;

        //    var s = reader.Value.ToString();
        //    DateTime result;
        //    if (DateTime.TryParseExact(s, format, CultureInfo.InstalledUICulture, DateTimeStyles.None, out result))
        //        return result;

        //    return null;
        //}
        private static readonly DateTime _epoch = new(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteRawValue(((DateTime)value - _epoch).TotalMilliseconds + "000");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) { return null; }
            return _epoch.AddMilliseconds(((long)reader.Value * 1000) / 1000d);
        }
    }
}