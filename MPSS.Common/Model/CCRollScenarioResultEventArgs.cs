﻿using System.Collections.Generic;

namespace MPSS.Common.Model
{
    public class CCRollScenarioResultEventArgs
    {
        public CCRollScenarioResultEventArgs(Dictionary<string, decimal> values)
        {
            Values = values;
        }

        public Dictionary<string, decimal> Values { get; }
    }
}
