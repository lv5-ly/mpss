﻿using Microsoft.Extensions.Caching.Memory;

namespace MPSS.Common.Model
{
    public class StockQuoteCache
    {
        public MemoryCache Cache { get; set; }

        public StockQuoteCache()
        {
            Cache = new MemoryCache(new MemoryCacheOptions
            {
                SizeLimit = 1024
            });
        }
    }
}
