﻿using MPSS.Common.Model.Tradier;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Asset transaction, it has reference back to the stock asset
    /// One stock asset can have at least one or many transactions
    /// </summary>
    public partial class AssetTransaction
    {
        private int OriginalQuantity { get; set; } = 0;

        public readonly Dictionary<DateTime, Option> CallOptions = new();

        #region Persistent Properties
        /// <summary>
        /// Transaction Id, key
        /// </summary>
        [Key]
        public int TransactionId { get; set; } = 0;

        /// <summary>
        /// Symbol ticker
        /// 
        /// Note: 
        ///     This is a redundant, since we have other way to reference
        ///     back to the stock. May want to remove.
        /// </summary>
        public String AssetSymbol { get; set; } = string.Empty;

        /// <summary>
        /// Transaction date, when the transaction happens
        /// </summary>
        public DateTime TransactionDate { get; set; } = DateTime.Now;

        /// <summary>
        /// Only for CC and CP, the date when option is expired
        /// </summary>
        public DateTime ExpiredDate { get; set; } = DateTime.Today;

        /// <summary>
        /// Buy or Sell
        /// </summary>
        public AssetTransactionAction TransactionAction { get; set; } = AssetTransactionAction.Sold;

        /// <summary>
        /// Number of shares
        /// </summary>
        public int Quantity { get; set; } = 0;

        /// <summary>
        /// Stock Share or Option
        /// </summary>
        public AssetTransactionType TransactionType { get; set; } = AssetTransactionType.CoveredCall;

        /// <summary>
        /// Price per share
        /// </summary>
        public decimal Price { get; set; } = 0.00M;

        /// <summary>
        /// Only for CC or CP, the stock price when the option is closed/assigned
        /// </summary>
        public decimal StrikePrice { get; set; } = 0.00M;

        /// <summary>
        /// Broker fee for this transaction
        /// </summary>
        public decimal Fee { get; set; } = 0.00M;

        /// <summary>
        /// This is a flag, to indicate whether this transaction is active
        /// User control
        /// </summary>
        public Boolean Active { get; set; } = true;

        /// <summary>
        /// For any message we want to use, for this transaction
        /// User control
        /// </summary>
        public String Message { get; set; } = string.Empty;

        /// <summary>
        /// A flag to hide or not showing this transaction
        /// User control
        /// </summary>
        public Boolean Hide { get; set; } = false;

        /// <summary>
        /// If this is a CC or CP transaction, and it's closed/assigned,
        /// this status will be set as: assigned. 
        /// </summary>
        public AssetTransactionStatus Status { get; set; } = AssetTransactionStatus.Open;

        /// <summary>
        /// This is the assigned/closed/bought-back transaction Id, which is
        ///     the result of the parent CC or CP transaction
        /// </summary>
        public int ParentTransactionId { get; set; } = 0;



        public int StockAssetId { get; set; }

        /// <summary>
        /// This transaction belongs to a particular stock
        /// </summary>
        public virtual StockAsset Stock { get; set; } = null;

        /// <summary>
        /// Optional, a set of additional properties, which belong
        /// to this transaction
        /// </summary>
        public virtual ICollection<TransactionProperty> TransactionProperties { get; set; } = null;
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Empty constructor
        /// Entity Framework needs this
        /// </summary>
        public AssetTransaction()
        {
            Initialize();
        }

        /// <summary>
        /// Constructor for a typical transaction, which belongs to a stock asset
        /// </summary>
        /// <param name="t">Transaction object to copy</param>
        /// <param name="sa">Stock asset object, to which this transaction belongs</param>
        public AssetTransaction(AssetTransaction t, StockAsset sa)
        {
            if (t == null)
            {
                Initialize();
            }
            else
            {
                Clone(t);
            }
            Stock = sa ?? null;
        }

        /// <summary>
        /// This is basically a copy constructor
        /// </summary>
        /// <param name="t">Original transaction</param>
        public AssetTransaction(AssetTransaction t)
        {
            Clone(t);
        }
        #endregion Constructors

        #region Public Methods
        /// <summary>
        /// The option -CC is assigned, thus we need to perform
        /// - Expire the transaction (worthless)
        /// - Sell the shares at the strike price
        /// </summary>
        public void Assigned()
        {
            Active = false;
            Price = 0M;
            TransactionAction = AssetTransactionAction.Assigned;
        }

        /// <summary>
        /// A transfer method to update the transaction quantity, and also detect
        /// whether the quantity is really been updated
        /// </summary>
        /// <param name="quantityInText">The quantity in text format, directly from the dialog</param>
        public void UpdateQuantity(string quantityInText)
        {
            this.OriginalQuantity = this.Quantity;

            this.Quantity = int.TryParse(
                quantityInText, out int q) ? q : this.OriginalQuantity;
        }

        public TransactionProperty UpdateUpperStrikePrice(decimal v)
        {
            return CreateAndUpdateIfNotExist(TransactionPropertyType.UpperStrikePrice, v.ToString());
        }

        public TransactionProperty UpdateSpreadOptionType(SpreadOptionType spreadOptionType)
        {
            return CreateAndUpdateIfNotExist(TransactionPropertyType.SpreadOptionType, spreadOptionType.ToString());
        }

        public TransactionProperty UpdateLowerStrikePrice(decimal v)
        {
            return CreateAndUpdateIfNotExist(TransactionPropertyType.LowerStrikePrice, v.ToString());
        }

        /// <summary>
        /// Create a transaction property and add into the list of properties
        /// for this transaction
        /// </summary>
        /// <param name="t">Transaction Spread option property type</param>
        public void AddSpreadOptionType(SpreadOptionType t)
        {
            TransactionProperties.Add(new TransactionProperty(TransactionPropertyType.SpreadOptionType, t.ToString()));
        }

        /// <summary>
        /// Retrieve a property, which belongs to this transaction
        /// </summary>
        /// <typeparam name="T">Type for this property value</typeparam>
        /// <param name="tpt">Property type</param>
        /// <returns>The property value</returns>
        public T GetProperty<T>(TransactionPropertyType tpt)
        {
            var property = TransactionProperties
                .FirstOrDefault(p => p.PropertyType == tpt);
            T value = property != null ? property.GetTypedValue<T>() : default;

            return value;
        }

        /// <summary>
        /// Create Upper and Lower strike price properties for 
        /// this transaction
        /// </summary>
        /// <param name="secondaryStrikePrice"></param>
        public void AddStrikePrices(decimal secondaryStrikePrice)
        {
            TransactionProperties.Add(CreateUpperStrikePriceProperty(secondaryStrikePrice));
            TransactionProperties.Add(CreateLowerStrikePriceProperty(secondaryStrikePrice));

        }

        /// <summary>
        /// Check to see if the transaction is currently Active
        /// </summary>
        /// <returns>True if the transaction is ACTIVE, false otherwise</returns>
        public bool IsActive()
        {
            return Active;
        }

        internal bool OptionWithinThreshold(Option option)
        {
            var percentage = (int)((option.Ask * 100) / Price);
            return percentage < 25;
        }

        /// <summary>
        /// Check to see if today is the expiration date
        /// </summary>
        /// <returns>True if today is the expiration date, false otherwise</returns>
        public bool IsExpireToday()
        {
            return TransactionDate == DateTime.Today;
        }

        /// <summary>
        /// An utility to reverse the transaction action
        /// </summary>
        /// <returns>The opposite action</returns>
        public AssetTransactionAction OppositeAction()
        {
            return TransactionAction == AssetTransactionAction.Sold ?
                AssetTransactionAction.Bought :
                AssetTransactionAction.Sold;
        }

        /// <summary>
        /// Mark/set this transaction as Closed-Out
        /// </summary>
        /// <param name="price">The price for buying back the SOLD option play</param>
        /// <param name="fee">The fee that broker charges</param>
        /// <param name="shares">Number of shares</param>
        /// <param name="closeOutDate">Closing out date</param>
        public void CloseOut(decimal price, decimal fee, int shares, DateTime closeOutDate)
        {
            Fee = fee;
            Quantity = shares;
            Price = price;
            ExpiredDate = closeOutDate;
            Active = false;
        }

        internal bool ExpiringOptionPlay()
        {
            return Active && IsOptionPlay() && IsExpireToday();
        }

        /// <summary>
        /// Check to see if this is an option play:
        ///     - BOUGHT-CALL, BOUGHT-PUT
        ///     - SOLD-CALL, SOLD-PUT
        /// </summary>
        /// <returns>True if this is an option play, false otherwsie</returns>
        public bool IsOptionPlay()
        {
            return (TransactionType != AssetTransactionType.Share);
        }

        /// <summary>
        /// Mark/set this transaction as expired
        /// </summary>
        public void Expire()
        {
            Active = false;
            Price = 0M;
            TransactionAction = AssetTransactionAction.Closed;
        }

        /// <summary>
        /// Check to see if this transaction has a parent 
        /// </summary>
        /// <returns>True if this transaction has parent, false otherwise</returns>
        public bool HasParentTransaction()
        {
            return TransactionId > 0;
        }

        internal bool IsAssigned()
        {
            return this.TransactionAction == AssetTransactionAction.Assigned;
        }

        /// <summary>
        /// Check to see if this transaction is a parent of another transaction
        /// </summary>
        /// <param name="transactions">A list of transactions</param>
        /// <returns>True if this is a parent of another transaction, false otherwise</returns>
        public bool IsParent(ICollection<AssetTransaction> transactions)
        {
            return transactions.Any(t => t.ParentTransactionId == TransactionId);
        }

        /// <summary>
        /// A fucntion to check this transaction is whether a
        /// bought/sold share transaction.
        /// </summary>
        /// <returns>True if this transaction is a bought/sold share, false otherwise</returns>
        public bool IsShareTransaction()
        {
            return (TransactionType == AssetTransactionType.Share &&
                (TransactionAction == AssetTransactionAction.Bought ||
                TransactionAction == AssetTransactionAction.Sold));
        }

        /// <summary>
        /// An utility function to verify if Parent (this) transaction
        /// has the same amount of shares as child's
        /// </summary>
        /// <param name="child">Child transaction</param>
        /// <returns>True if parent transaction has same amount of shares, false otherwise</returns>
        public bool HasSameShare(AssetTransaction child)
        {
            return Quantity == child.Quantity;
        }

        /// <summary>
        /// An utility function to verify if Parent (this) transaction
        /// has more shares than child's
        /// </summary>
        /// <param name="child">Child transaction</param>
        /// <returns>True if parent transaction has more share, false otherwise</returns>
        public bool HasMoreShare(AssetTransaction child)
        {
            return Quantity > child.Quantity;
        }


        /// <summary>
        /// An utility method to specify whether the quantity has been changed
        /// </summary>
        /// <returns>True if the quantity has been updated (ie. UpdateQuantity method was invoked)</returns>
        public bool HasQuantityUpdated()
        {
            return this.OriginalQuantity != this.Quantity;
        }
        #endregion

        #region Non-Persistent Properties
        /// <summary>
        /// To support UI component to display transaction type, which is a combination
        ///     of both ACTION and TYPE
        /// </summary>
        [NotMapped]
        public string TransactionDisplayType
        {
            get
            {
                var s = $"{TransactionAction.Description().ToUpper()}";
                s += $" - {TransactionType.Description().ToUpper()}";
                s += IsSpreadPlay
                    ? $" {OptionSpreadType.Description().ToUpper()}"
                    : string.Empty;
                return s;
            }
        }

        [NotMapped]
        public bool IsActiveCoveredCall
        {
            get { return TransactionType == AssetTransactionType.CoveredCall && Status == AssetTransactionStatus.Open; }
        }

        [NotMapped]
        [Browsable(false)]
        public bool IsBoughtShare
        {
            get
            {
                return TransactionAction == AssetTransactionAction.Bought &&
                      TransactionType == AssetTransactionType.Share;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public bool IsCoveredCall
        {
            get
            {
                return TransactionAction == AssetTransactionAction.Sold &&
                      TransactionType == AssetTransactionType.CoveredCall;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public bool Inactive
        {
            get
            {
                return !Active;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public string ReviewOptionAction
        {
            get
            {
                if (CallOptions.Count > 0)
                {
                    return IsPlayPossible
                        ? " CONSIDER "
                        : " ignore ";
                }
                return string.Empty;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public bool IsPlayPossible
        {
            get
            {
                return HasValidOptionChain && ReviewOptionPercentage < 25;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public Option CallOption
        {
            get
            {
                return CallOptions.Count > 0
                    ? CallOptions.First().Value
                    : null;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public int ReviewOptionPercentage
        {
            get
            {
                var o = CallOptions.First().Value;
                return (int)((o.Ask ?? 0) * 100 / Price);
            }
        }

        [NotMapped]
        [Browsable(false)]
        public bool HasValidOptionChain
        {
            get
            {
                return CallOptions.Count > 0 &&
                    CallOptions.First().Value != null;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public decimal UnrealizedPL
        {
            get
            {
                // Calculate unrealized gain, diplay negative if it is a long option
                decimal unrealized_gain = 0;
                switch (TransactionAction)
                {
                    case AssetTransactionAction.Sold:
                        unrealized_gain = (Price * Quantity) - Fee;
                        break;
                    case AssetTransactionAction.Bought:
                        unrealized_gain = -1 * ((Price * Quantity) + Fee);
                        break;
                }
                return (unrealized_gain);
            }
        }

        [NotMapped]
        [Browsable(false)]
        public decimal Breakeven
        {
            get
            {
                decimal even_price = 0;
                switch (TransactionAction)
                {
                    case AssetTransactionAction.Sold:
                        {
                            switch (TransactionType)
                            {
                                case AssetTransactionType.CreditSpread:
                                    if (OptionSpreadType == SpreadOptionType.Put)
                                    {
                                        even_price = UpperStrikePrice - Price;
                                    }
                                    if (OptionSpreadType == SpreadOptionType.Call)
                                    {
                                        even_price = LowerStrikePrice + Price;
                                    }
                                    break;
                                case AssetTransactionType.CoveredCall:
                                    even_price = StrikePrice - Price;
                                    break;
                                case AssetTransactionType.CoveredPut:
                                    even_price = StrikePrice - Price;
                                    break;
                            }
                        }
                        break;
                    case AssetTransactionAction.Bought:
                        {
                            switch (TransactionType)
                            {
                                case AssetTransactionType.DebitSpread:
                                    if (OptionSpreadType == SpreadOptionType.Put)
                                    {
                                        even_price = UpperStrikePrice - Price;
                                    }
                                    if (OptionSpreadType == SpreadOptionType.Call)
                                    {
                                        even_price = LowerStrikePrice + Price;
                                    }
                                    break;
                                case AssetTransactionType.CoveredCall:
                                    even_price = StrikePrice + Price;
                                    break;
                                case AssetTransactionType.CoveredPut:
                                    even_price = StrikePrice + Price;
                                    break;
                            }
                        }
                        break;
                }
                return (even_price);
            }
        }

        [NotMapped]
        [Browsable(false)]
        public bool HasDualStrikePrices
        {
            get
            {
                return (TransactionProperties.Any(p => p.PropertyType == TransactionPropertyType.UpperStrikePrice) &&
                    TransactionProperties.Any(p => p.PropertyType == TransactionPropertyType.LowerStrikePrice));
            }
        }

        /// <summary>
        /// Check to see if this is a credit/debit spread play transaction
        /// </summary>
        /// <returns>True if this is a spread play, false otherwise</returns>
        [NotMapped]
        [Browsable(false)]
        public bool IsSpreadPlay
        {
            get
            {
                return TransactionType == AssetTransactionType.CreditSpread ||
                    TransactionType == AssetTransactionType.DebitSpread;
            }
        }

        /// <summary>
        /// The upper strike price of a spread option transaction
        /// If the transaciton is NOT a spread option transaction, then
        /// the upper strike price will be 0.00
        /// </summary>
        /// <returns>Upper strike price</returns>
        [NotMapped]
        [Browsable(false)]
        public decimal UpperStrikePrice
        {
            get
            {
                return IsSpreadPlay
                    ? GetProperty<decimal>(TransactionPropertyType.UpperStrikePrice)
                    : 0.00M;
            }

            set
            {
                CreateAndUpdateIfNotExist(TransactionPropertyType.UpperStrikePrice, value.ToString());
            }
        }

        /// <summary>
        /// The lower strike price of a spread option transaction
        /// If the transaciton is NOT a spread option transaction, then
        /// the lower strike price will be 0.00
        /// </summary>
        /// <returns>Lower strike price</returns>
        [NotMapped]
        [Browsable(false)]
        public decimal LowerStrikePrice
        {
            get
            {
                return IsSpreadPlay
                    ? GetProperty<decimal>(TransactionPropertyType.LowerStrikePrice)
                    : 0.00M;
            }

            set
            {
                CreateAndUpdateIfNotExist(TransactionPropertyType.LowerStrikePrice, value.ToString());
            }
        }

        /// <summary>
        /// The option spread type property, OptionSpreadCall or OptionSpreadPut
        /// If the transaciton is NOT a spread option transaction, then
        /// the this will be Unknown
        /// </summary>
        /// <returns>Option spread type</returns>
        [NotMapped]
        [Browsable(false)]
        public SpreadOptionType OptionSpreadType
        {
            get
            {
                return IsSpreadPlay
                    ? GetProperty<SpreadOptionType>(TransactionPropertyType.SpreadOptionType)
                    : SpreadOptionType.Invalid;
            }
        }
        #endregion

        #region Override Methods
        public override string ToString()
        {
            var m = $"{AssetSymbol}";
            if (TransactionType == AssetTransactionType.CoveredCall)
            {
                m += $" (Exp. {ExpiredDate:M} @{StrikePrice:N} {Price:C})";
            }
            return m;
        }
        #endregion

        #region Private Methods
        private TransactionProperty CreateAndUpdateIfNotExist(TransactionPropertyType t, string v)
        {
            var property = TransactionProperties.FirstOrDefault(p => p.PropertyType == t);
            if (property == null)
            {
                property = new TransactionProperty(t);
                TransactionProperties.Add(property);
            }
            property.Value = v;

            return property;
        }

        /// <summary>
        /// Create a transaction property, of type LowerStrikePrice
        /// </summary>
        /// <param name="secondaryStrikePrice">The second strike price</param>
        /// <returns>LowerStrikePrice transaction property</returns>
        private TransactionProperty CreateLowerStrikePriceProperty(decimal secondaryStrikePrice)
        {
            return new TransactionProperty()
            {
                PropertyType = TransactionPropertyType.LowerStrikePrice,
                Value = Math.Min(StrikePrice, secondaryStrikePrice).ToString(),
                Transaction = this
            };
        }

        /// <summary>
        /// Create a transaction property, of type UpperStrikePrice
        /// </summary>
        /// <param name="secondaryStrikePrice">The second strike price</param>
        /// <returns>UpperStrikePrice transaction property</returns>
        private TransactionProperty CreateUpperStrikePriceProperty(decimal secondaryStrikePrice)
        {
            return new TransactionProperty()
            {
                PropertyType = TransactionPropertyType.UpperStrikePrice,
                Value = Math.Max(StrikePrice, secondaryStrikePrice).ToString(),
                Transaction = this
            };
        }

        /// <summary>
        /// Initialize the object instance
        /// </summary>
        private void Initialize()
        {
            AssetSymbol = string.Empty;
            TransactionAction = AssetTransactionAction.Sold;
            Price = 0M;
            Quantity = 0;
            TransactionType = AssetTransactionType.CoveredCall;
            TransactionDate = DateTime.Now;
            Fee = 0M;
            ExpiredDate = DateTime.Now.AddDays(14);
            StrikePrice = 0M;
            Active = true;
            Hide = false;
            Message = string.Empty;
            Status = AssetTransactionStatus.Open;

            ParentTransactionId = 0;

            TransactionProperties = new List<TransactionProperty>();
        }

        /// <summary>
        /// Copy or clone transaction
        /// </summary>
        /// <param name="t">Transaction object to clone</param>
        private void Clone(AssetTransaction t)
        {
            AssetSymbol = t.AssetSymbol;
            TransactionAction = t.TransactionAction;
            Price = t.Price;
            Quantity = t.Quantity;
            TransactionType = t.TransactionType;
            TransactionDate = t.TransactionDate;
            Fee = t.Fee;
            ExpiredDate = t.ExpiredDate;
            StrikePrice = t.StrikePrice;
            Active = t.Active;
            Hide = t.Hide;
            Message = t.Message;
            Status = t.Status;

            // Shallow clone, only pointing to the same Stock object
            Stock = t.Stock;
            StockAssetId = t.StockAssetId;


            ParentTransactionId = t.ParentTransactionId;

            // Do not shallow-clone the properties
            TransactionProperties = new List<TransactionProperty>();// t.TransactionProperties;

            // Transfer the properties, if they don't have property ID
            foreach (var p in t.TransactionProperties)
            {
                if (p.TransactionPropertyId == 0)
                {
                    var newProperty = new TransactionProperty(p)
                    {
                        Transaction = this
                    };
                    TransactionProperties.Add(newProperty);
                }
            }
        }
        #endregion
    }
}
