﻿namespace MPSS.Common.Model
{
    public interface IRowContextMenu
    {
        bool Validate(string menuItemName);
    }
}