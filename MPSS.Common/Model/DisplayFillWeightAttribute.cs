﻿using System;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Attribute for the property in a DataGridView
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DisplayFillWeightAttribute : Attribute
    {
        public DisplayFillWeightAttribute(int width)
        {
            Width = width;
        }
        public int Width { get; set; } = 100;
    }
}
