﻿using System.ComponentModel;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Credit or Debit Spread option type, of CALL or PUT
    /// </summary>
    public enum SpreadOptionType
    {
        [Description("Invalid")]
        Invalid,
        [Description("Call")]
        Call,
        [Description("Put")]
        Put
    }
}
