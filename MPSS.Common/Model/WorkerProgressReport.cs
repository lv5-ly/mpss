﻿using MPSS.Common.Model.Tradier;
using System;

namespace MPSS.Common.Model
{
    /// <summary>
    /// A transient class for the BackgroundWorker to report progress
    /// scanning the stock option chain
    /// </summary>
    public class WorkerProgressReport
    {
        public string Message { get; set; }
        public Option Option { get; }
        public StockAsset Stock { get; }
        public AssetTransaction Transaction { get; }
        //private bool ProgressResult = false;

        public WorkerProgressReport(string m)
        {
            Message = m;
        }

        public WorkerProgressReport(string m, Option o, StockAsset s) : this(m)
        {
            Option = o;
            Stock = s;
        }

        public WorkerProgressReport(string m, Option o, AssetTransaction t) : this(m)
        {
            Option = o;
            Transaction = t;
        }

        /// <summary>
        /// This constructor is used to report an active CC transaction with 
        ///     current call-option
        /// </summary>
        /// <param name="t">Active CC transaction</param>
        public WorkerProgressReport(AssetTransaction t) : this(null, null, t)
        {
            Message = $"\tFor {Transaction}, ";
            if (Transaction.CallOption != null)
            {
                Message += $"you may want to {Transaction.ReviewOptionAction} this {Transaction.CallOption} ({Transaction.ReviewOptionPercentage}%)";
            }
            else
            {
                Message += "a call option is not availble.";
            }
        }

        /// <summary>
        /// This constructor is used to report an active stock
        ///     with stock option chain
        /// </summary>
        /// <param name="s">Active stock</param>
        public WorkerProgressReport(StockAsset s) : this(null, null, s)
        {
            Message = $"\tFor {Stock}, ";
            if (Stock.CallOption != null)
            {
                Message += $"you may want to {Stock.ReviewOptionAction} this {Stock.CallOption.ToCallOptionString()} ({Stock.ReviewOptionPercentage}%)";
            }
            else
            {
                Message += "a call-option is not available.";
            }
        }

        public WorkerProgressReport(StockAsset s, DateTime d) : this(s)
        {
            Message = $"\tFor {Stock}, ";
            if (Stock.CallOption != null)
            {
                Message += $"you may want to {Stock.ReviewOptionAction} this {Stock.CallOption.ToCallOptionString()} ({Stock.ReviewOptionPercentage}%)";
            }
            else
            {
                Message += $"a call-option is not available for this expiration date {d.ToShortDateString()}";
            }
        }

        public WorkerProgressReport(StockAsset s, StockQuote quote) : this(s)
        {
            Message = $"\tFor {Stock}, ";
            if (quote != null && quote.Ask != null)
            {
                Message += $"current market quote: {quote.Ask:C}";
            }
            else
            {
                Message += $"current market quote is not available.";
            }
        }

    }
}
