﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MPSS.Common.Model
{
    public class TransactionProperty
    {
        #region Migration
        static readonly string TABLE_NAME = "TransactionProperties";
        public static readonly string CREATE_TABLE = $@"CREATE TABLE IF NOT EXISTS {TABLE_NAME} (
            TransactionPropertyId INTEGER NOT NULL CONSTRAINT PK_TransactionProperties PRIMARY KEY AUTOINCREMENT,
            CreationTime TEXT NOT NULL,
            PropertyType INTEGER NOT NULL,
            Value TEXT NOT NULL,
            TransactionId INTEGER NULL,
            CONSTRAINT FK_AssetTransactions_TransactionId FOREIGN KEY(TransactionId) 
            REFERENCES AssetTransactions (TransactionId) ON DELETE RESTRICT)";
        #endregion

        #region Constructor
        public TransactionProperty() { }

        public TransactionProperty(TransactionPropertyType t) : this()
        {
            PropertyType = t;
        }

        public TransactionProperty(TransactionPropertyType t, string v) : this(t)
        {
            Value = v;
        }

        public TransactionProperty(TransactionProperty p) : this(p.PropertyType, p.Value) { }
        #endregion

        #region Persistent Properties
        [Key]
        public int TransactionPropertyId { get; set; } = 0;
        public DateTime CreationTime { get; set; } = DateTime.Now;
        public TransactionPropertyType PropertyType { get; set; } = TransactionPropertyType.Unknown;
        public string Value { get; set; } = string.Empty;
        public int TransactionId { get; set; } = 0;
        public virtual AssetTransaction Transaction { get; set; } = null;
        #endregion

        #region Public Methods        
        /// <summary>
        /// An utility to convert to typed value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <returns>Typed value</returns>
        public T GetTypedValue<T>()
        {
            if (typeof(T).IsEnum)
            {
                return (T)Enum.Parse(typeof(T), Value);
            }
            else
            {
                return (T)Convert.ChangeType(Value, typeof(T));
            }
        }
        #endregion
    }
}
