﻿using System;
using System.ComponentModel;
using System.Drawing;

namespace MPSS.Common.Model
{
    /// <summary>
    /// For each active option transaction, show the following
    ///     Symbol/Ticker
    ///     Shares/contracts
    ///     Price/premium
    ///     Transaction Type
    ///     Strike price
    ///     Expiration date
    ///     Comments
    ///     Unrealized P&L
    /// </summary>
    public class ActiveOptionTransaction :
        IMultiColumnFormat,
        IRowContextMenu
    {
        #region Private Properties
        private readonly AssetTransaction Transaction;
        #endregion

        #region Constructors
        public ActiveOptionTransaction(AssetTransaction t)
        {
            Transaction = t;
        }
        #endregion

        #region Public Properties
        [Description("Stock ticker/symbol")]
        [DisplayFillWeightAttribute(75)]
        public string Symbol { get { return Transaction.AssetSymbol; } }

        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("Number of shares")]
        [DisplayFormat("N0")]
        [DisplayFillWeightAttribute(70)]
        public int Shares
        {
            get
            {
                return Transaction.TransactionAction == AssetTransactionAction.Sold
                    ? -1 * Transaction.Quantity
                    : Transaction.Quantity;
            }
        }

        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("Premium or cost per share")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(75)]
        public decimal Price { get { return Transaction.Price; } }

        [Description("Option type")]
        [DisplayName("Transaction Type")]
        public string TransactionType
        {
            get
            {
                var result = $"{Transaction.TransactionDisplayType}";
                if (Transaction.IsSpreadPlay)
                {
                    result += $"\n({Transaction.OptionSpreadType.Description().ToUpper()})";
                }
                return result;
            }
        }


        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("Strike price")]
        [DisplayName("Strike Price")]
        [DisplayFillWeightAttribute(80)]
        public string StrikePrice
        {
            get
            {
                var result = $"{Transaction.StrikePrice:C}";
                if (Transaction.IsSpreadPlay &&
                    Transaction.HasDualStrikePrices)
                {
                    result = $"{Transaction.LowerStrikePrice:C}-{Transaction.UpperStrikePrice:C}";
                }
                return result;
            }
        }

        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("The break-even point")]
        [DisplayName("Breakeven")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(75)]
        public decimal BreakEvenPoint
        {
            get
            {
                // TODO
                //  Provide the calculation for Unrealized P&L
                //  for this month
                return Transaction.Breakeven;
            }
        }

        [Description("Option expiration date")]
        [DisplayFormat("d")]
        [DisplayName("Expire Date")]
        public DateTime ExpireDate { get { return Transaction.ExpiredDate; } }

        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("Additional comment/message")]
        public string Comment { get { return Transaction.Message; } }

        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("Unrealized Profit & Loss")]
        [DisplayName("Unrealized P&L")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(75)]
        public decimal UnrealizedPL
        {
            get
            {
                // TODO
                //  Provide the calculation for Unrealized P&L
                //  for this month
                return Transaction.UnrealizedPL;
            }
        }
        #endregion

        #region Public Methods
        public AssetTransaction GetTransaction()
        {
            return Transaction;
        }
        #endregion

        #region IMultiColumnFormat Interface
        /// <summary>
        /// Retrieve background color for this row
        /// </summary>
        /// <returns>Color instance</returns>
        public Color GetBackgroundColor()
        {
            return this.UnrealizedPL > 0M
                ? Color.FromName("PaleGreen")
                : Color.FromName("LightSalmon");
        }
        #endregion

        #region IRowContextMenu
        public bool Validate(string menuItemName)
        {
            var valid = true;
            if (menuItemName == "ScenarioMenuItem")
            {
                valid = Transaction.IsCoveredCall;
            }
            return valid;
        }
        #endregion

        #region Private Methods
        //private string GetStrikePrices()
        //{
        //    decimal upperStrikePrice = Transaction.GetProperty<decimal>(TransactionPropertyType.UpperStrikePrice);
        //    decimal lowerStrikePrice = Transaction.GetProperty<decimal>(TransactionPropertyType.LowerStrikePrice);

        //    return $"({lowerStrikePrice:C} - {upperStrikePrice:C})";
        //}
        #endregion
    }
}
