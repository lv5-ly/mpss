﻿using System.Drawing;

namespace MPSS.Common.Model
{
    public interface IMultiColumnFormat
    {
        Color GetBackgroundColor();
    }
}
