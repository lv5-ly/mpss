﻿using System.IO;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Database file
    /// </summary>
    public class DBFile
    {
        private readonly FileInfo fileInfo;

        /// <summary>
        /// Make sure the DBFile exists and valid
        /// </summary>
        public bool IsValid { get { return fileInfo != null && fileInfo.Exists; } }

        /// <summary>
        /// Constructor, with the filename
        /// </summary>
        /// <param name="fileName">Filename, full path</param>
        public DBFile(string fileName)
        {
            this.fileInfo = new FileInfo(fileName);
        }

        /// <summary>
        /// Override the typical ToString, to returns only
        /// the file name
        /// </summary>
        /// <returns>File name</returns>
        public override string ToString()
        {
            return Path.GetFileNameWithoutExtension(fileInfo.FullName);
        }

        /// <summary>
        /// Full path filename
        /// </summary>
        /// <returns>Full path file name</returns>
        public string GetFileName()
        {
            return fileInfo.FullName;
        }
    }
}
