﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

#nullable disable

namespace MPSS.Common.Model
{
    public partial class MPSSContext : DbContext
    {
        public string DatabaseFilename { get; set; } = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\MPSS.db";
        private static bool _created = false;
        public virtual DbSet<AssetTransaction> AssetTransactions { get; set; }
        public virtual DbSet<StockAsset> StockAssets { get; set; }
        public virtual DbSet<TransactionProperty> TransactionProperties { get; set; }

        public MPSSContext()
        {
        }

        public MPSSContext(string filename, bool reload = false)
        {
            if (reload)
            {
                _created = false;
            }

            DatabaseFilename = filename;
            if (!_created)
            {
                if (!Database.GetService<IRelationalDatabaseCreator>().Exists())
                {
                    Database.EnsureCreated();
                }
                CreateTableIfNotExist(TransactionProperty.CREATE_TABLE);
                MigrateData();

                _created = true;
            }
        }

        /// <summary>
        /// Perform data migration if needed
        /// </summary>
        private void MigrateData()
        {
            // Only migrate data if the TransactionProperties table is empty
            //  and we have spread option plays in the AssetTransactions table
            if (!TransactionProperties.Any() &&
                AssetTransactions.Any(t => (t.TransactionType == AssetTransactionType.CreditSpread ||
                                                        t.TransactionType == AssetTransactionType.DebitSpread)))
            {
                var spreadOptionTransactions = from t in AssetTransactions
                                               where (t.TransactionType == AssetTransactionType.CreditSpread ||
                                                        t.TransactionType == AssetTransactionType.DebitSpread)
                                               select t;
                Regex rgx = new(@"([0-9]+.[0-9].)", RegexOptions.IgnoreCase);
                foreach (var t in spreadOptionTransactions)
                {
                    MatchCollection matches = rgx.Matches(t.Message);
                    if (matches.Count == 2)
                    {
                        decimal secondStrikePrice = Convert.ToDecimal(matches.Last().Value);
                        t.AddStrikePrices(secondStrikePrice);
                    }
                }
                SaveChanges();
            }
        }

        public MPSSContext(DbContextOptions<MPSSContext> options)
            : base(options)
        {
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite(@$"Data Source={DatabaseFilename}");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssetTransaction>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.HasIndex(e => e.StockAssetId, "IX_AssetTransactions_StockAssetId");

                entity.Property(e => e.ExpiredDate).IsRequired();

                entity.Property(e => e.TransactionDate).IsRequired();

                entity.HasOne(d => d.Stock)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.StockAssetId);
            });

            OnModelCreatingPartial(modelBuilder);

        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        private void CreateTableIfNotExist(string createTableQuery)
        {
            using SqliteConnection sqlite = new($"Data Source={DatabaseFilename}");
            try
            {
                sqlite.Open();  //Initiate connection to the db
                SqliteCommand cmd = sqlite.CreateCommand();
                cmd.CommandText = createTableQuery;
                var result = cmd.ExecuteNonQuery().ToString();
            }
            finally
            {
                sqlite.Close();
            }
        }
    }
}
