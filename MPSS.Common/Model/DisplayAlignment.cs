﻿namespace MPSS.Common.Model
{
    public enum DisplayAlignment
    {
        Left,
        Center,
        Right
    }
}
