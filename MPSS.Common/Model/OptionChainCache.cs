﻿using Microsoft.Extensions.Caching.Memory;

namespace MPSS.Common.Model
{
    public class OptionChainCache
    {
        public MemoryCache Cache { get; set; }

        public OptionChainCache()
        {
            Cache = new MemoryCache(new MemoryCacheOptions
            {
                SizeLimit = 1024
            });
        }
    }
}