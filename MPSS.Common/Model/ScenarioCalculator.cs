﻿using System.Collections.Generic;

namespace MPSS.Common.Model
{
    /// <summary>
    /// A calculator to calculate various scenarios.
    /// Each scenario can have many calculation values.
    /// </summary>
    public class ScenarioCalculator
    {
        #region Private Properties
        private AssetTransaction CurrentAssetTransaction { get; }
        #endregion

        #region Public Properties
        public const string C_CCROLL_REPL = "Current Expired -CC P&L";
        public const string C_CCROLL_CSA = "Current Stk Assigned P&L";
        public const string C_CCROLL_RPL = "Current Rolled Realized P&L";
        public const string N_CCROLL_NECC = "New Expired -CC P&L";
        public const string N_CCROLL_NSA = "New Stk Assigned P&L";
        public const string N_CCROLL_RPL = "New Rolled Realized P&L";

        public const string BBCCTOSELL_RPL = "Realized P&L";
        public const string BBCCTOSELL_REPL = "Current Expired CC P&L";
        public const string BBCCTOSELL_CSA = "Current Stck Assigned P&L";

        public const string SELLSTOCK_OP_RPL = "Original Stock price - P&L";
        public const string SELLSTOCK_AP_RPL = "Adjusted Stock price - P&L";
        #endregion

        #region Constructors
        public ScenarioCalculator(AssetTransaction currentAssetTransaction)
        {
            CurrentAssetTransaction = currentAssetTransaction;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Calculate the CC-Roll scenario.
        /// 
        /// User buys back the covered call which is about to expire, to sell a new 
        /// cover call which expires at a much later date.
        /// 
        /// This scenario calculates the followings P&L: 
        /// - If user applies this CC-Roll scenario (RPL)
        /// - If user lets the -CC expired and stocks assigned (CSA)
        /// - If user applies this CC-Roll scenario, and the new covered call expires (NECC)
        /// - If user applies this CC-Roll scenario, and the new covered call get assigned (NSA)
        /// </summary>
        /// <param name="scenarioCalculationType">The scenario calculation type: RPL, CSA, NECC, or NSA</param>
        /// <param name="buyBackPremium">Buy-back CC premium</param>
        /// <param name="newCCPremium">New -CC premium</param>
        /// <param name="newStrikePrice">New strike price</param>
        /// <returns>Calculation value</returns>
        private decimal Calculate(string scenarioCalculationType,
            decimal buyBackPremium, decimal newCCPremium, decimal newStrikePrice)
        {
            // The current stock and transaction is available
            //  via property: this.CurrentAssetTransaction
            decimal result = 0M;
            decimal stkCostPrice = this.CurrentAssetTransaction.Stock.GetAverageStockPrice();

            switch (scenarioCalculationType)
            {
                case C_CCROLL_REPL:
                    // Price of -CC for Realized P&L
                    result = CurrentAssetTransaction.Price;
                    break;
                case C_CCROLL_RPL:
                    // The difference between the -CC and +CC
                    result = (CurrentAssetTransaction.Price - buyBackPremium);
                    break;
                case C_CCROLL_CSA:
                    // The difference between the strike and the premium, subtract cstock sot for P&L
                    result = (CurrentAssetTransaction.StrikePrice + CurrentAssetTransaction.Price - stkCostPrice);
                    break;
                case N_CCROLL_NECC:
                    // Price of new -CC premium todisplayt new Expored -CC P&L
                    result = newCCPremium;
                    break;
                case N_CCROLL_NSA:
                    // Assigned stock strike price - stock cost + new --CC premium
                    result = newStrikePrice - stkCostPrice + newCCPremium;
                    break;
                case N_CCROLL_RPL:
                    // New CC Rolled Realized P&L => The sum of  between the -CC and +CC
                    result = (newStrikePrice - stkCostPrice + newCCPremium)+ (CurrentAssetTransaction.Price - buyBackPremium);
                    break;
            }
            return result * CurrentAssetTransaction.Quantity;
        }

        /// <summary>
        /// Calculate the Sell Stock scnenario
        /// 
        /// User just sells all the stock at the current market price.
        /// 
        /// This scenario calculates the followings P&L: 
        /// - The realized P&L, using the original stock price (OP_RPL)
        /// - The realized P&L, using the adjusted stock price (AP_RPL)
        /// </summary>
        /// <param name="scenarioCalculationType">The scenario calculation type</param>
        /// <param name="marketPrice">The current market price of this stock</param>
        /// <returns>Calculation value</returns>
        private decimal Calculate(string scenarioCalculationType, decimal marketPrice)
        {
            // The current stock and transaction is available
            //  via property: this.CurrentAssetTransaction
            decimal result = 0M, originalPrice = 0M, adjustedPrice = 0M;

            if (CurrentAssetTransaction.Stock != null)
            {
                originalPrice = CurrentAssetTransaction.Stock.GetAverageStockPrice();
                adjustedPrice = CurrentAssetTransaction.Stock.AdjCost;
            }
            switch (scenarioCalculationType)
            {
                case SELLSTOCK_OP_RPL:
                    result = marketPrice - originalPrice;
                    break;
                case SELLSTOCK_AP_RPL:
                    result = marketPrice - adjustedPrice;
                    break;
            }
            return result * CurrentAssetTransaction.Quantity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenarioCalculationType"></param>
        /// <param name="buyBackPremium"></param>
        /// <param name="marketPrice"></param>
        /// <returns></returns>
        private decimal Calculate(string scenarioCalculationType,
            decimal buyBackPremium, decimal marketPrice)
        {
            /*var result = scenarioCalculationType == BBCCTOSELL_RPL
                ? (CurrentAssetTransaction.Price - buyBackPremium) + marketPrice
                : 0M;
            return result * CurrentAssetTransaction.Quantity;
            */
            decimal result = 0M;
            decimal stkCostPrice = this.CurrentAssetTransaction.Stock.GetAverageStockPrice();
            switch (scenarioCalculationType)
            {
                case BBCCTOSELL_RPL:
                    /// P&L after +CC, sell at market price subtract stock cost
                    result = CurrentAssetTransaction.Price - buyBackPremium + marketPrice - stkCostPrice;
                    break;
                case BBCCTOSELL_CSA:
                    // The difference between the strike and the premium, subtract cstock sot for P&L
                    result = (CurrentAssetTransaction.StrikePrice + CurrentAssetTransaction.Price - stkCostPrice);
                    break;
                case BBCCTOSELL_REPL:
                    // Price of -CC for Realized P&L
                    result = CurrentAssetTransaction.Price;
                    break;
            }
            return result * CurrentAssetTransaction.Quantity;
        }

        #endregion

        #region Public Methods
        public Dictionary<string, decimal> CalculateCCRoll(
            decimal BuyBackPremium, decimal NewCCPremium, decimal NewStrikePrice)
        {
            var result = new Dictionary<string, decimal>
            {
                { C_CCROLL_REPL, Calculate(C_CCROLL_REPL, BuyBackPremium, NewCCPremium, NewStrikePrice) },
                { C_CCROLL_RPL, Calculate(C_CCROLL_RPL, BuyBackPremium, NewCCPremium, NewStrikePrice) },
                { C_CCROLL_CSA, Calculate(C_CCROLL_CSA, BuyBackPremium, NewCCPremium, NewStrikePrice) },
                { N_CCROLL_NECC, Calculate(N_CCROLL_NECC, BuyBackPremium, NewCCPremium, NewStrikePrice) },
                { N_CCROLL_NSA, Calculate(N_CCROLL_NSA, BuyBackPremium, NewCCPremium, NewStrikePrice) },
                { N_CCROLL_RPL, Calculate(N_CCROLL_RPL, BuyBackPremium, NewCCPremium, NewStrikePrice) }
            };

            return result;
        }

        public Dictionary<string, decimal> CalculateSellStock(decimal marketPrice)
        {
            var result = new Dictionary<string, decimal>
            {
                { SELLSTOCK_OP_RPL, Calculate(SELLSTOCK_OP_RPL, marketPrice) },
                { SELLSTOCK_AP_RPL, Calculate(SELLSTOCK_AP_RPL, marketPrice) }
            };

            return result;
        }

        public Dictionary<string, decimal> CalculateBBCCToSell(decimal BuyBackPremium, decimal MarketPrice)
        {
            var result = new Dictionary<string, decimal>
            {
                { BBCCTOSELL_REPL, Calculate(BBCCTOSELL_REPL, BuyBackPremium, MarketPrice) },
                { BBCCTOSELL_CSA, Calculate(BBCCTOSELL_CSA, BuyBackPremium, MarketPrice) },
                { BBCCTOSELL_RPL, Calculate(BBCCTOSELL_RPL, BuyBackPremium, MarketPrice) }
            };

            return result;
        }
        #endregion
    }
}
