﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPSS.Common.Model
{
    public class AnnualSummary :
        IMultiColumnFormat,
        IRowContextMenu
    {
        private decimal[] values = new decimal[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        #region Public Properties
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of January")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Jan { get { return values[0]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of February")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Feb { get { return values[1]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of March")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Mar { get { return values[2]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of April")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Apr { get { return values[3]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of May")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal May { get { return values[4]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of June")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Jun { get { return values[5]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of July")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Jul { get { return values[6]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of August")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Aug { get { return values[7]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of September")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Sep { get { return values[8]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of October")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Oct { get { return values[9]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of November")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Nov { get { return values[10]; } }
        [DisplayAlignment(DisplayAlignment.Right)]
        [Description("P&L of December")]
        [DisplayFormat("C")]
        [DisplayFillWeightAttribute(70)]
        public decimal Dec { get { return values[11]; } }

        public Color GetBackgroundColor()
        {
            return Color.FromName("white");
        }

        public bool Validate(string menuItemName)
        {
            return true;
        }

        public void Clear(int m)
        {
            values[m] = 0;
        }

        public void Add(int m, decimal v)
        {
            values[m] += v;
        }
        #endregion
    }
}
