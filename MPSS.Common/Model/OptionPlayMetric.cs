﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Data for a row of Option Play Grid
    ///     Contracts: number of contracts, 1 = 100 shares
    ///     Type: Call or Put
    ///     Premium: the price per share for this option
    ///     Un-realized: the outstanding value
    ///     Realized: the value when this option is closed
    /// </summary>
    public class OptionPlayMetric
    {
        private StockAsset currentStock = null;
        private readonly AssetTransactionAction transactionAction = AssetTransactionAction.Closed;
        private readonly AssetTransactionType transactionType = AssetTransactionType.Share;

        #region Constructors

        public OptionPlayMetric()
        {

        }
        public OptionPlayMetric(
            StockAsset stock,
            AssetTransactionAction tAction,
            AssetTransactionType tType)

        {
            currentStock = stock;
            transactionAction = tAction;
            transactionType = tType;

            Refresh();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Number of contracts, 1 = 100 shares
        /// </summary>
        public int Contracts { get; set; }

        /// <summary>
        /// Call or Put
        /// </summary>
        public string ContractType { get; set; }

        /// <summary>
        /// The price per share for this option
        /// </summary>
        public decimal Premium { get; set; }

        /// <summary>
        /// The outstanding or open option value
        /// </summary>
        public decimal UnrealizedValue { get; set; }

        /// <summary>
        /// The value when this option is closed
        /// </summary>
        public decimal RealizedValue { get; set; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// This method to set the current stock instance
        /// </summary>
        /// <param name="stock">Stock object instance</param>
        public void SetCurrentStock(StockAsset stock)
        {
            // Save this off, so we can do the calculation when needed
            currentStock = stock;

            Refresh();
        }
        #endregion Methods

        #region Private Methods
        /// <summary>
        /// Recalculate all the metrics
        /// </summary>
        private void Refresh()
        {
            /// Get the current Month and Year in integer form
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;

            // This is all trasactions of the same kind (action/type)
            var allMatchedTransactions = AllMatchedTransactions();

            Contracts = allMatchedTransactions
                .Where(t => (!t.Active && t.ExpiredDate.Year == year && t.ExpiredDate.Month == month) ||
                            (t.Active))
                .Select(t => (t.Quantity / 100))
                .Sum();

            ContractType = $"{transactionAction.Description().ToUpper()} - {transactionType.Description().ToUpper()}";

            // Sum up all the premium collected
            Premium = allMatchedTransactions
                .Where(t => (!t.Active && t.ExpiredDate.Year == year && t.ExpiredDate.Month == month) ||
                            (t.Active))
                .Select(t => (t.Price))
                .Sum();

            // Calculate unrealized P & L
            decimal unrealized = allMatchedTransactions
                .Where(t => t.Active)
                .Select(t => (t.Price * t.Quantity))
                .Sum();
            if (transactionAction == AssetTransactionAction.Bought && unrealized != 0)
            {
                UnrealizedValue = unrealized * -1;
            }
            else
            {
                UnrealizedValue = unrealized;
            }

            // Calculate realized P & L
            decimal fees = allMatchedTransactions
                .Where(t => !t.Active && t.ExpiredDate.Year == year && t.ExpiredDate.Month == month)
                .Select(t => (t.Fee))
                .Sum();
            decimal realized = allMatchedTransactions
                .Where(t => !t.Active && t.ExpiredDate.Year == year && t.ExpiredDate.Month == month)
                .Select(t => (t.Price * t.Quantity))
                .Sum();
            if (transactionAction == AssetTransactionAction.Bought && realized != 0)
            {
                RealizedValue = (realized + fees) * -1;
            }
            else
            {
                RealizedValue = realized - fees;
            }
        }

        /// <summary>
        /// Returns all the transaction which matched this
        ///     instance's action & type
        /// </summary>
        /// <returns>List of matched transactions</returns>
        private IEnumerable<AssetTransaction> AllMatchedTransactions()
        {
            return currentStock
                .Transactions
                .Where(t => t.TransactionType == transactionType && t.TransactionAction == transactionAction);
        }

        #endregion Private Methods
    }
}
