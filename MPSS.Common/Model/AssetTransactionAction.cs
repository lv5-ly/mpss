﻿namespace MPSS.Common.Model
{
    /// <summary>
    /// Enum for a transaction action: Bought, Sold, Closed (?), Edit (only for UI), Delete (UI), Assigned
    /// </summary>
    public enum AssetTransactionAction
    {
        Bought,
        Sold,
        Closed,
        Edit,
        Delete,
        Assigned
    }
}
