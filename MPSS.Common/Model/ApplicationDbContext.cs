﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace MPSS.Common.Model
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ApplicationDbContext : DbContext
    {
        private string databaseFilename;
        private static bool _created = false;
        private static bool _migrated = false;

        /// <summary>
        /// StockAssets table
        /// </summary>
        public DbSet<StockAsset> StockAssets { get; set; }

        /// <summary>
        /// AssetTransactions table
        /// </summary>
        public DbSet<AssetTransaction> AssetTransactions { get; set; }

        public ApplicationDbContext(string filename)
        {
            databaseFilename = filename;
            if (!_created)
            {
                if (!Database.GetService<IRelationalDatabaseCreator>().Exists())
                {
                    Database.EnsureCreated();
                }
                _created = true;
            }

            if (!_migrated)
            {
                // Migrate();
                _migrated = true;
            }
        }

        //private void Migrate()
        //{

        //    throw new NotImplementedException();
        //}

        protected override void OnConfiguring(DbContextOptionsBuilder optionbuilder)
        {
            if (string.IsNullOrEmpty(databaseFilename))
            {
                databaseFilename = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\MPSS.db";
            }
            optionbuilder.UseSqlite(@$"Data Source={databaseFilename}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AssetTransaction>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.HasIndex(e => e.StockAssetId, "IX_AssetTransactions_StockAssetId");

                entity.Property(e => e.ExpiredDate).IsRequired();

                entity.Property(e => e.TransactionDate).IsRequired();

                entity.HasOne(d => d.Stock)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.StockAssetId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
