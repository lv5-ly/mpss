﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPSS.Common.Model
{
    public class AnnualSummaryYear
    {
        private int year = DateTime.Now.Year;
        public int Year {
            get { return year; }

            set {
                year = value;
            } 
        }
    }
}
