﻿namespace MPSS.Common.Model
{
    /// <summary>
    /// Various transaction property types
    /// </summary>
    public enum TransactionPropertyType
    {
        Unknown,            // Uninitialized value
        UpperStrikePrice,   // The upper strike price, for spread play
        LowerStrikePrice,   // The lower strike price, for spread play
        MarketQuotePrice,   // The market quote price, at the time this transaction is created 
        SpreadOptionType,   // Spread Option type CALL or PUT
    }
}