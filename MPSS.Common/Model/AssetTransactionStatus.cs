﻿namespace MPSS.Common.Model
{
    public enum AssetTransactionStatus
    {
        Open,       // This CC or CP is open, in-play
        Assigned,   // This CC or CP is assigned/closed
        Expired,    // This CC or CP is expired, shares intact
        BoughtBack  // This CC or CP is bought-back
    }
}
