﻿using System.ComponentModel;

namespace MPSS.Common.Model
{
    /// <summary>
    /// Enum for transaction applicable type: share, CC, or CP
    /// </summary>
    public enum AssetTransactionType
    {
        [Description("Share")]
        Share,
        [Description("Call")]
        CoveredCall,
        [Description("Put")]
        CoveredPut,
        [Description("Credit Spread")]
        CreditSpread,
        [Description("Debit Spread")]
        DebitSpread
    }
}
