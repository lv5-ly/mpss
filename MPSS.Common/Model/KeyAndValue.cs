﻿namespace MPSS.Common.Model
{
    public class KeyAndValue
    {
        #region Public Properties
        public string Key { get; set; } = "Key";
        public string Value { get; set; } = "Value";
        #endregion

        #region Constructors
        public KeyAndValue(string k, string v)
        {
            Key = k;
            Value = v;
        }
        #endregion

        //private KeyValuePair<string, string> Item { get; set; } = new KeyValuePair<string, string>();

    }
}
