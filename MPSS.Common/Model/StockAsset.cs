﻿using MPSS.Common.Model.Tradier;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MPSS.Common.Model
{
    /// <summary>
    /// This class represent a stock asset, for the database to manage
    /// </summary>
    public class StockAsset
    {
        #region Constructors
        /// <summary>
        /// Empty constructor
        /// Entity Framework needs this
        /// </summary>
        public StockAsset()
        {
            Shares = 0;
            ProfitLoss = 0M;
            Symbol = "none";
            Transactions = new List<AssetTransaction>();
        }

        /// <summary>
        /// Constructor based on a transaction;
        /// </summary>
        /// <param name="t"></param>
        public StockAsset(AssetTransaction t)
        {
            // This is a bit tricky, if this is an option play, the quantity 
            //  for this stock is 0
            Shares = t.IsOptionPlay() || t.IsSpreadPlay ? 0 : t.Quantity;
            ProfitLoss = (t.Quantity * t.Price);
            Symbol = t.AssetSymbol;
            Transactions = new List<AssetTransaction>();
        }
        #endregion Constructors

        #region Properties
        [NotMapped]
        [Browsable(false)]
        public Dictionary<DateTime, IEnumerable<Option>> Options = new();

        /// <summary>
        /// The Key for this record in database
        /// </summary>
        [Key]
        public int StockAssetId { get; set; }

        /// <summary>
        /// Stock symbol or ticker
        /// </summary>
        public String Symbol { get; set; }

        /// <summary>
        /// Retrieve a list of active option play transactions, which are
        /// expiring today
        /// </summary>
        /// <returns>List of expiring transactions</returns>
        public IEnumerable<AssetTransaction> GetExpiringOptionPlayTransactions()
        {
            return Transactions
                .Where(t => t.ExpiringOptionPlay());
        }

        /// <summary>
        /// Total number of shares for this stock symbol
        /// </summary>
        public int Shares { get; set; }

        /// <summary>
        /// Check and return if this stock has transaction which match
        /// the following action and type
        /// </summary>
        /// <param name="a">Transaction action</param>
        /// <param name="t">Transaction type</param>
        /// <returns>True if there is a match, False otherwise</returns>
        public bool HasThisPlay(AssetTransactionAction a, AssetTransactionType t)
        {
            return Transactions.Any(r => (r.TransactionAction == a && r.TransactionType == t) &&
                                         ((r.Active) ||
                                          (!r.Active && r.ExpiredDate.Month == DateTime.Now.Month &&
                                           r.ExpiredDate.Year == DateTime.Now.Year))
                                   );
        }

        /// <summary>
        /// Current P&L for this stock
        /// 
        /// Notes: 
        ///     Not sure if we need this, if we calculate as needed
        /// </summary>
        [Browsable(false)]
        public decimal ProfitLoss { get; set; }

        /// <summary>
        /// A flag to indicate whether this stock is currently active
        /// User control
        /// </summary>
        [Browsable(false)]
        public Boolean Active { get; set; }

        /// <summary>
        /// A message to display, when associated with this stock
        /// User control
        /// </summary>
        [Browsable(false)]
        public String Message { get; set; }

        /// <summary>
        /// A flag to hide or not showing this stock
        /// User control, or indirectly set
        /// </summary>
        [Browsable(false)]
        public Boolean Hide { get; set; }

        /// <summary>
        /// This provides the one-to-many relationship with the transactions
        /// </summary>
        [Browsable(false)]
        public virtual ICollection<AssetTransaction> Transactions { get; set; }

        [NotMapped]
        [Browsable(false)]
        public decimal AdjCost
        {
            get
            {
                var adjustedCost = 0M;

                // If this stock does not have any bought-share transaction, it will throw
                //  exception; so we need to skip and return a 0.00 for adjusted cost
                if (this.HasBoughtShareTransaction())
                {
                    /// Get all CC premium we collect
                    decimal CCPlusPremium = Transactions
                        .Where(t => !t.Active &&
                         (t.TransactionType == AssetTransactionType.CoveredCall || t.TransactionType == AssetTransactionType.CoveredPut)
                         && (t.TransactionAction == AssetTransactionAction.Sold))
                        .Select(t => t.Price * t.Quantity)
                        .Sum();
                    /// Get all the CC buy back
                    decimal CCMinusPremium = Transactions
                        .Where(t => !t.Active &&
                         (t.TransactionType == AssetTransactionType.CoveredCall || t.TransactionType == AssetTransactionType.CoveredPut)
                         && (t.TransactionAction == AssetTransactionAction.Bought))
                        .Select(t => t.Price * t.Quantity)
                        .Sum();


                    var originalCost = 0M;

                    if (this.Shares > 0)
                    {
                        // Get the average (bought) share price
                        originalCost = this.GetAverageStockPrice();

                        // Calculate the Stock Adjuted cost
                        adjustedCost = ((originalCost * this.Shares) - CCPlusPremium + CCMinusPremium) / this.Shares;
                    }
                }
                return adjustedCost;

            }
        }

        /// <summary>
        /// This property provides the Adjusted Cost value 
        /// 
        /// TODO:
        ///     Replace with correct formula
        /// </summary>
        [NotMapped]             // Let Entity Framework, not to read or write this property
        [Browsable(true)]       // Let the UI/Grid to show this property
        public string AdjustedCost
        {
            get
            {
                string ac = string.Empty;

                // If this stock does not have any bought-share transaction, it will throw
                //  exception; so we need to skip and return a 0.00 for adjusted cost
                if (this.HasBoughtShareTransaction())
                {
                    /// Get all CC premium we collect
                    decimal CCPlusPremium = Transactions
                        .Where(t => !t.Active &&
                         (t.TransactionType == AssetTransactionType.CoveredCall || t.TransactionType == AssetTransactionType.CoveredPut)
                         && (t.TransactionAction == AssetTransactionAction.Sold))
                        .Select(t => t.Price * t.Quantity)
                        .Sum();
                    /// Get all the CC buy back
                    decimal CCMinusPremium = Transactions
                        .Where(t => !t.Active &&
                         (t.TransactionType == AssetTransactionType.CoveredCall || t.TransactionType == AssetTransactionType.CoveredPut)
                         && (t.TransactionAction == AssetTransactionAction.Bought))
                        .Select(t => t.Price * t.Quantity)
                        .Sum();

                    var adjustedCost = 0M;
                    var originalCost = 0M;

                    if (this.Shares > 0)
                    {
                        // Get the average (bought) share price
                        originalCost = this.GetAverageStockPrice();

                        // Calculate the Stock Adjuted cost
                        adjustedCost = ((originalCost * this.Shares) - CCPlusPremium + CCMinusPremium) / this.Shares;
                    }

                    // The adjusted cost the the difference
                    ac = $"{originalCost:N}  /  {adjustedCost:N}";
                }
                return ac;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public string ReviewOptionAction
        {
            get
            {
                var o = CallOption;
                if (o != null)
                {
                    return IsPlayPossible
                        ? " CONSIDER "
                        : " ignore ";
                }
                return string.Empty;
            }
        }

        [NotMapped]
        [Browsable(false)]
        public int ReviewOptionPercentage
        {
            get
            {
                var o = CallOption;
                if (o != null)
                {
                    var q = LatestQuote != null
                        ? Math.Max((decimal)AdjCost, (decimal)LatestQuote.Ask)
                        : AdjCost;
                    return (int)(o.Ask ?? o.Ask.Value * 100 / q);
                }
                return 0;
            }
        }


        [NotMapped]
        [Browsable(false)]
        public Option CallOption
        {
            get
            {
                if (Options != null && Options.Count > 0)
                {
                    var q = (LatestQuote != null) && (LatestQuote.Ask != null)
                        ? Math.Max((decimal)AdjCost, (decimal)LatestQuote.Ask)
                        : AdjCost;
                    var options = Options.First().Value;
                    return options?
                        .Where(o => o.OptionType == "call" && o.Strike > q)
                        .OrderByDescending(o => o.Ask)
                        .FirstOrDefault();
                }
                return null;
            }
        }


        /// <summary>
        /// Latest market quote data
        /// </summary>
        [NotMapped]
        [Browsable(false)]
        public StockQuote LatestQuote { get; set; }

        [NotMapped]
        [Browsable(false)]
        public bool IsPlayPossible
        {
            get
            {
                return (ReviewOptionPercentage > 5) || (CallOption?.Ask > 2M);
            }
        }
        #endregion Properties

        #region Methods
        public Boolean IsActivelyTraded()
        {
            return (this.Shares > 0 || this.HasActiveTrans());
        }

        public Boolean HasActiveTrans()
        {
            Boolean activeTrans = false;
            foreach (var t in this.Transactions)
            {
                if (t.Active || 
                    ((t.ExpiredDate.Month >= DateTime.Now.Month) &&
                     (t.ExpiredDate.Year >= DateTime.Now.Year)))
                {
                    activeTrans = true;
                }
            }
            return activeTrans;

        }

        public decimal GetTotalPandL()
        {
            /// Return ALL time P&L for the stock
            decimal profit = Transactions
                .Where(t => !t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Sold)
                )
                .Select(t => t.Quantity * t.Price - t.Fee)
                .Sum();
            decimal loss = Transactions
                .Where(t => !t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Bought)
                )
                .Select(t => t.Quantity * t.Price + t.Fee)
                .Sum();
            return profit - loss;
        }
        public decimal GetYTDPandL()
        {
            /// Only return P&L for the current year
            int year = DateTime.Now.Year;

            decimal profit = Transactions
                .Where(t => !t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Sold)
                      && (t.ExpiredDate.Year == year)
                )
                .Select(t => t.Quantity * t.Price - t.Fee)
                .Sum();
            decimal loss = Transactions
                .Where(t => !t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Bought)
                      && (t.ExpiredDate.Year == year)
                )
                .Select(t => t.Quantity * t.Price + t.Fee)
                .Sum();
            return profit - loss;
        }
        public decimal GetMonthlyPandL(int month, int year)
        {
            /// Only return P&L for the desired month

            decimal profit = Transactions
                .Where(t => !t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Sold)
                      && (t.ExpiredDate.Month == month)
                      && (t.ExpiredDate.Year == year)
                )
                .Select(t => t.Quantity * t.Price - t.Fee)
                .Sum();
            decimal loss = Transactions
                .Where(t => !t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Bought)
                      && (t.ExpiredDate.Month == month)
                      && (t.ExpiredDate.Year == year)
                )
                .Select(t => t.Quantity * t.Price + t.Fee)
                .Sum();
            return profit - loss;
        }

        public decimal GetExpiredPandL()
        {
            /// Get the current Month and Year in integer form
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;

            decimal ExpiredProfit = Transactions
                .Where(t => t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Sold)
                      && (t.TransactionType != AssetTransactionType.Share)
                      && (t.ExpiredDate.Month == month)
                      && (t.ExpiredDate.Year == year)
                )
                .Select(t => t.Quantity * t.Price - t.Fee)
                .Sum();

            decimal ExpiredDebit = Transactions
                .Where(t => t.Active &&
                      (t.TransactionAction == AssetTransactionAction.Bought)
                      && (t.TransactionType != AssetTransactionType.Share)
                      && (t.ExpiredDate.Month == month)
                      && (t.ExpiredDate.Year == year)
                )
                .Select(t => t.Quantity * t.Price + t.Fee)
                .Sum();

            return (ExpiredProfit - ExpiredDebit);
        }

        public decimal GetStockAssignedPandL()
        {
            /// Get the current Month and Year in integer form
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            decimal stkCost = 0;
            decimal stkAssignedPandL = 0;

            if (this.Shares > 0)
            {
                var activeTransactionPrices = Transactions
                    .Where(t => t.Active &&
                          (t.TransactionAction == AssetTransactionAction.Bought)
                          && (t.TransactionType == AssetTransactionType.Share)
                    )
                    .Select(t => t.Price);

                if (activeTransactionPrices.Count() > 1)
                {
                    stkCost = activeTransactionPrices.Average();
                }
                else if (activeTransactionPrices.Count() == 1)
                {
                    stkCost = activeTransactionPrices.First();
                }
                else
                {
                    stkCost = 0;
                }


                /// Get the original stock cost
                //stkCost = Transactions
                //    .Where(t => t.Active &&
                //          (t.TransactionAction == AssetTransactionAction.Bought)
                //          && (t.TransactionType == AssetTransactionType.Share)
                //    )
                //    .Select(t => t.Price)
                //    .Average();

                stkAssignedPandL = Transactions
                    .Where(t => t.Active &&
                          (t.TransactionAction == AssetTransactionAction.Sold)
                          && (t.TransactionType == AssetTransactionType.CoveredCall)
                          && (t.ExpiredDate.Month == month)
                          && (t.ExpiredDate.Year == year)
                    )
                    .Select(t => t.Quantity * (t.StrikePrice - stkCost) - t.Fee)
                    .Sum();
            }

            return stkAssignedPandL;

        }

        public int GetAvailableShares()
        {
            return Shares;
        }

        public bool CanSellCoveredCall()
        {
            return Shares >= 100;
        }

        public bool CanSellCoveredPut()
        {
            return Shares >= 100;
        }

        public decimal TotalStockBought()
        {
            return Transactions
                .Where(t => (t.TransactionType == AssetTransactionType.Share) && (t.TransactionAction == AssetTransactionAction.Bought))
                .Select(t => (t.Price * t.Quantity) + t.Fee)
                .Sum();
        }

        public decimal TotalStockSold()
        {
            return Transactions
                .Where(t => (t.TransactionType == AssetTransactionType.Share) && (t.TransactionAction == AssetTransactionAction.Sold))
                .Select(t => (t.Price * t.Quantity) - t.Fee)
                .Sum();
        }

        public decimal GetUnrealizedValue()
        {
            return Math.Abs(TotalStockBought() - TotalStockSold());
        }

        public decimal GetInThePocket()
        {
            return TotalStockSold();
        }

        public override string ToString()
        {
            return $"{Symbol} Orig/Adj Cost ({AdjustedCost})";
        }

        /*public Double GetMonthlyProfit()
        {
            return GetThisMonthCoveredCallAndPutProfit() + GetThisMonthStockSoldProfit();
        }
        */
        //private decimal GetThisMonthStockSoldProfit()
        //{
        //    // TODO:
        //    //  Calculate the average price of the stock bought
        //    //  The different of the sold and bought price
        //    //  Multiple to the quantity
        //    var averageBoughtPrice = GetAverageBoughtPrice();
        //    return Transactions.
        //        Where(t =>
        //            (t.TransactionType == AssetTransactionType.Share &&
        //             t.TransactionAction == AssetTransactionAction.Sold) &&
        //             t.TransactionDate.Month == DateTime.Today.Month)
        //        .Select(t => ((t.Price - averageBoughtPrice) * t.Quantity) - t.Fee)
        //        .Sum();
        //}

        //private decimal GetAverageBoughtPrice()
        //{
        //    return Transactions.
        //        Where(t =>
        //            (t.TransactionType == AssetTransactionType.Share &&
        //             t.TransactionAction == AssetTransactionAction.Bought) &&
        //             t.TransactionDate.Month == DateTime.Today.Month)
        //        .Select(t => (t.Price + t.Fee) / t.Quantity)
        //        .Average();
        //}

        //private decimal GetThisMonthCoveredCallAndPutProfit()
        //{
        //    // TODO:
        //    //  Have to take in consideration of when the CC or CP were being bought back
        //    //  
        //    return Transactions.
        //        Where(t =>
        //            (t.TransactionType == AssetTransactionType.CoveredCall ||
        //             t.TransactionType == AssetTransactionType.CoveredPut) &&
        //             t.TransactionDate.Month == DateTime.Today.Month)
        //        .Select(t => (t.Price * t.Quantity) - t.Fee)
        //        .Sum();
        //}

        public decimal GetThisMonthSpent()
        {
            return Transactions.
                Where(t =>
                    (t.TransactionType == AssetTransactionType.Share &&
                     t.TransactionAction == AssetTransactionAction.Bought) &&
                     t.TransactionDate.Month == DateTime.Today.Month)
                .Select(t => (t.Price * t.Quantity) + t.Fee)
                .Sum();
        }

        /// <summary>
        /// Calculate the average stock price based on all
        /// bought-share transactions. However, if there is
        /// not a bought-share transaction, this returns a
        /// 0.00 value.
        /// </summary>
        /// <returns>Average stock price or 0, if there is none bought-share or shares are all sold </returns>
        public decimal GetAverageStockPrice()
        {
            decimal totalCost = 0;
            decimal numberOfShares = 0;
            decimal costPerShare = 0;

            if (HasBoughtShareTransaction() && Shares > 0)
            {
                totalCost = GetAllBoughtShareTransactions().Select(t => (t.Price * t.Quantity) )
                                               .Sum();
                numberOfShares = GetAllBoughtShareTransactions().Select(t => t.Quantity)
                                               .Sum();
                costPerShare = totalCost / numberOfShares;
            }
            return costPerShare;
        }

        /// <summary>
        /// Retrieve a list of BOUGHT-SHARE transactions
        /// </summary>
        /// <returns>All BOUGHT-SHARE transactions</returns>
        private IEnumerable<AssetTransaction> GetAllBoughtShareTransactions()
        {
            return Transactions
                .Where(t =>
                    t.TransactionType == AssetTransactionType.Share &&
                    t.TransactionAction == AssetTransactionAction.Bought);
        }

        /// <summary>
        /// Check to see if there is any bought-share transaction
        /// </summary>
        /// <returns>True if there is a bought-share transaction, false otherwise</returns>
        private bool HasBoughtShareTransaction()
        {
            return Transactions.Any(t =>
                t.TransactionAction == AssetTransactionAction.Bought &&
                t.TransactionType == AssetTransactionType.Share);
        }

        /// <summary>
        /// Returns number of shares by iterating through all
        /// current transactions of BOUGHT/SOLD SHARE
        /// </summary>
        /// <returns>Number of shares</returns>
        public int GetNumberOfShares()
        {
            return Transactions
                .Where(t =>
                    t.TransactionType == AssetTransactionType.Share &&
                    (t.TransactionAction == AssetTransactionAction.Bought || t.TransactionAction == AssetTransactionAction.Sold))
                .Select(t =>
                    t.Quantity * (t.TransactionAction == AssetTransactionAction.Sold ? -1 : 1))
                .Sum();
        }
        #endregion Methodssecure (voltage)
    }
}
