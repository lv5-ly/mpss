﻿using System.Collections.Generic;

namespace MPSS.Common.Model
{
    public class BBCCToSellScenarioResultEventArgs
    {
        public BBCCToSellScenarioResultEventArgs(Dictionary<string, decimal> values)
        {
            Values = values;
        }

        public Dictionary<string, decimal> Values { get; }
    }
}
