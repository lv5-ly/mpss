﻿using System;

namespace MPSS.Common.Model
{
    public class ScanResultEventArgs : EventArgs
    {
        public readonly int NumberOfPossiblePlays;
        public ScanResultEventArgs(int n)
        {
            NumberOfPossiblePlays = n;
        }
    }
}
