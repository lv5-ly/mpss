﻿using MPSS.Common.DataService;
using MPSS.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace MPSS.ViewModel
{
    /// <summary>
    /// This View Model class contains data and behavior to support UI components
    /// - Data for the UI components
    ///     - List of stocks
    ///     - Stock's statistics/metrics
    /// - Behavior to support a stock model: 
    ///     - Load/persist stock data
    ///     - CRUD operation on a stock
    ///     
    /// Notes:
    ///     - This view model does not have behavior or data for stock's 
    ///     transaction, see TransactionViewModel
    /// </summary>
    public class StockAssetViewModel
    {
        #region Private Properties

        private IStockAssetDataProvider dataProvider;

        #endregion Private Properties

        #region Public Properties

        public StockAsset CurrentSelectedAsset { get; private set; }
        public BindingList<StockAsset> StockAssetBindingList = new();
        public AssetTransaction CurrentSelectedTransaction { get; private set; }
        public bool HasStockAsset { get { return CurrentSelectedAsset != null; } }
        public List<StockAsset> AllStocks = new();

        #endregion Public Properties

        #region Public Methods

        public StockAssetViewModel(IStockAssetDataProvider provider)
        {
            this.dataProvider = provider;
        }
        public IEnumerable<AssetTransaction> GetAllActiveCoveredCallTransactions()
        {
            return StockAssetBindingList
                .SelectMany(s => s.Transactions)
                .Where(t => t.Active && t.IsCoveredCall);
        }
        public IEnumerable<StockAsset> GetAllActiveNonCoveredCallTransactions()
        {
            return StockAssetBindingList
                .Where(s => s.CanSellCoveredCall())
                .Select(s => s);
        }
        public IEnumerable<StockAsset> GetStockData()
        {
            return dataProvider.LoadStockAssets();
        }
        public BindingList<StockAsset> GetStockAssetBindingList()
        {
            return StockAssetBindingList;
        }
        /// <summary>
        /// A method to load data into this model-view's properties.
        /// </summary>
        public void Load(int sortOrder = 0)
        {
            StockAssetBindingList.Clear();
            AllStocks.Clear();

            var sortedList = GetSortedStockAssets(sortOrder);
            foreach (var stockAsset in sortedList)
            {
                // Let's load this stock into a separate list
                // This list will be used to do calculation, if it 
                //  needs all stocks

                AllStocks.Add(stockAsset);

                // Then we can check and add it into the StockAssetBindingList
                //  which is for the Stock Asset Grid to show
                if (stockAsset.IsActivelyTraded())
                {
                    StockAssetBindingList.Add(stockAsset);
                }
            }

            // Set the default currently selected stock asset
            CurrentSelectedAsset = StockAssetBindingList.FirstOrDefault();
        }

        /// <summary>
        /// Use this method to retrieve the Stock object, which has
        /// the matching symbol
        /// </summary>
        /// <param name="symbol">Stock symbol</param>
        /// <returns>Stock object</returns>
        public StockAsset GetStockBySymbol(string symbol)
        {
            return StockAssetBindingList.FirstOrDefault(s => s.Symbol == symbol);
        }

        private IEnumerable<StockAsset> GetSortedStockAssets(int sortOrder)
        {
            return sortOrder == 0 ?
                dataProvider.LoadStockAssets() :
                sortOrder == 1 ?
                dataProvider.LoadStockAssets().OrderBy(s => s.Symbol) :
                dataProvider.LoadStockAssets().OrderByDescending(s => s.Symbol);
        }
        public AssetTransaction AddTransaction(AssetTransaction t)
        {
            // First, we need to find out whether this transaction belongs to 
            //  a stock we currently have.
            CurrentSelectedAsset = AllStocks.FirstOrDefault(s => s.Symbol == t.AssetSymbol);
            if (CurrentSelectedAsset == null)
            {
                // This transaction belongs to a new stock, so we need to
                //  add this stock to the database first.
                CurrentSelectedAsset = dataProvider.AddNewStockAsset(t);
            }
            else
            {
                t.Stock = CurrentSelectedAsset;
                CurrentSelectedTransaction = dataProvider.AddAssetTransaction(t);
            }

            return CurrentSelectedTransaction;
        }
        public void SwitchDatabaseFile(IStockAssetDataProvider provider)
        {
            dataProvider = provider;
        }
        public void UpdateTransaction(AssetTransaction currentSelectedTransaction)
        {
            dataProvider.UpdateTransaction(currentSelectedTransaction);

            if (currentSelectedTransaction.HasQuantityUpdated())
            {
                CurrentSelectedAsset.Shares = CurrentSelectedAsset.GetNumberOfShares();
                dataProvider.UpdateStock(CurrentSelectedAsset);
            }
        }
        public StockAsset GetSelectedAsset()
        {
            return CurrentSelectedAsset;
        }
        public void SelectStockAsset(StockAsset selectedAsset)
        {
            // currentSelectedAsset = StockAssetBindingList.FirstOrDefault(a => a.StockAssetId == selectedAsset.StockAssetId);
            CurrentSelectedAsset = selectedAsset;
        }
        public IEnumerable<AssetTransaction> GetCurrentSelectedStockTransactions()
        {
            return CurrentSelectedAsset?.Transactions;
        }
        public void SelectTransaction(AssetTransaction selectedTransaction)
        {
            CurrentSelectedTransaction = CurrentSelectedAsset.Transactions.FirstOrDefault(t => t.TransactionId == selectedTransaction.TransactionId);
        }
        /// <summary>
        /// Remove the currently select transaction
        /// </summary>
        public void DeleteTransaction()
        {
            if (TransactionHasPartner(out AssetTransaction partner))
            {
                partner.Active = true;
                UpdateTransaction(partner);
            }

            // Let's find and remove it from the current stock's transaction list
            CurrentSelectedAsset.Transactions.Remove(CurrentSelectedTransaction);

            // Now, we can tell the database to remove it from the transaction table
            dataProvider.DeleteTransaction(CurrentSelectedTransaction.TransactionId);

            // We need to recalculate the number of shares, for this stock; basically
            //  we re-iterate through all the transaction, and if it's a Bought, we add
            //  if it's a Sold, we subtract
            CurrentSelectedAsset.Shares = UpdateStockShare(CurrentSelectedAsset);

            // Since the currentSelectedTransaction was removed, we need to pick a new one, so
            // let's pick the first one
            CurrentSelectedTransaction = CurrentSelectedAsset.Transactions.FirstOrDefault();
        }

        /// <summary>
        /// This method return a list of transactions
        ///     - Which belongs to an active stock asset
        ///     - An active & option play transaction
        /// </summary>
        /// <returns>A list of transactions</returns>
        public IEnumerable<AssetTransaction> GetActiveOptionTransactions()
        {
            return StockAssetBindingList                    // Starting from this stock asset list
                .SelectMany(s => s.Transactions)            // Select all the transaction (of the active stock)    
                .Where(t => t.Active && t.IsOptionPlay())   // Filter out all inactive and non-option play transaction
                .Select(t => t);                            // Select the transaction which passes all the criteria
        }

        public StockAsset GetCurrentlySelectedStock()
        {
            return CurrentSelectedAsset;
        }
        /// <summary>
        /// Remove the stock from the data binding list, and also use the 
        /// dataprovider to remove it from the database
        /// </summary>
        public void RemoveStock()
        {
            // Let's remove it from the database first
            dataProvider.RemoveStockAsset(CurrentSelectedAsset.StockAssetId);

            // Now we can update the data binding list, which will change 
            //  the data grid in the UI
            StockAssetBindingList.Remove(CurrentSelectedAsset);

            // We need to select a current select asset, if any
            CurrentSelectedAsset = StockAssetBindingList.FirstOrDefault();
        }
        /// <summary>
        /// Close out parent and child transactions
        /// </summary>
        /// <param name="parent">Parent transaction</param>
        /// <param name="child">Child transaction</param>
        public void CloseTransaction(AssetTransaction parent, AssetTransaction child)
        {
            //  When this is a SOLD-SHARE child transaction
            //  If the parent has more shares than the child transaction, then we need
            //  to auto-generate a split parent transaction, to keep the original parent
            //  transaction active, and adjust the shares correctly.
            if (parent.HasMoreShare(child))
            {
                // First, we need the change the quantity of the original
                //  transaction
                parent.Quantity -= child.Quantity;
                parent.Active = true;
                UpdateTransaction(parent);

                // Create a proxy BOUGHT-SHARE transaction, to serve
                //  as the parent for the child SOLD-SHARE transaction,
                //  so both can be Inactive for P&L calculation
                //  to be correct
                var proxyParent = AddTransaction(
                    new AssetTransaction(parent)
                    {
                        Quantity = child.Quantity,
                        Active = false,
                        ExpiredDate = child.ExpiredDate
                    });
                // We update the child's parent transaction Id ,to
                //  point to the proxy parent
                child.ParentTransactionId = proxyParent.TransactionId;
            }
            else if (parent.HasSameShare(child))
            {
                // Close out the parent trasaction
                parent.Active = false;
                parent.ExpiredDate = child.ExpiredDate;
                UpdateTransaction(parent);
            }

            // Add the child transaction
            var childTransaction = AddTransaction(child);

            // For assigned transaction, create a new SOLD-SHARE transaction
            if (childTransaction.IsAssigned())
            {
                var assignedTransaction = CreateAssignedTransaction(parent, childTransaction.TransactionId);
                AddTransaction(assignedTransaction);
            }

            // Update stock's share quantity
            CurrentSelectedAsset.Shares = UpdateStockShare(childTransaction.Stock);
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Based on the parent transaction, whether a -CC or -CP, the assigned
        /// transaction should be a SOLD-SHARE or BOUGHT-SHARE
        /// </summary>
        /// <param name="parent">Parent or the option transaction, whether a -CP or -CC </param>
        /// <param name="transactionId">The assigned transaction Id</param>
        /// <returns>The assigned transaction</returns>
        private static AssetTransaction CreateAssignedTransaction(AssetTransaction parent, int transactionId)
        {
            return new AssetTransaction()
            {
                // For -CC, the assigned will result in a SOLD-SHARE; but -CP, the assigned
                //  will result in a BOUGHT-SHARE transaction
                TransactionAction = parent.TransactionType == AssetTransactionType.CoveredCall
                    ? AssetTransactionAction.Sold
                    : AssetTransactionAction.Bought,
                TransactionType = AssetTransactionType.Share,
                AssetSymbol = parent.AssetSymbol,
                Price = parent.StrikePrice,
                Quantity = parent.Quantity,
                TransactionDate = DateTime.Now,
                StockAssetId = parent.StockAssetId,
                ExpiredDate = DateTime.Now,
                ParentTransactionId = transactionId
            };
        }

        /// <summary>
        /// Check to see if this transaction is part of pair
        /// </summary>
        /// <param name="partner">The partner transaction</param>
        /// <returns>True if this transactin is part of a pair, false otherwise</returns>
        private bool TransactionHasPartner(out AssetTransaction partner)
        {
            partner = null;
            if (CurrentSelectedTransaction.HasParentTransaction())
            {
                partner = CurrentSelectedAsset
                    .Transactions
                    .FirstOrDefault(t => t.TransactionId == CurrentSelectedTransaction.ParentTransactionId);
            }
            else if (CurrentSelectedTransaction.IsParent(CurrentSelectedAsset.Transactions))
            {
                partner = CurrentSelectedAsset
                    .Transactions
                    .FirstOrDefault(t => t.ParentTransactionId == CurrentSelectedTransaction.TransactionId);
            }
            return partner != null;
        }
        /// <summary>
        /// A function to update stock's share quantity
        /// </summary>
        /// <param name="stock">Stock object</param>
        private int UpdateStockShare(StockAsset stock)
        {
            // We need to recalculate the number of shares, for this stock; basically
            //  we re-iterate through all the transaction, and if it's a Bought, we add
            //  if it's a Sold, we subtract
            int shares = stock.GetNumberOfShares();

            if (shares != stock.Shares)
            {
                stock.Shares = shares;
                dataProvider.UpdateStock(stock);
            }

            return shares;
        }

        #endregion Private Methods
    }
}
