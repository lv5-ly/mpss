﻿using MPSS.Common.Model;
using System.ComponentModel;

namespace MPSS.Common.ViewModel
{
    /// <summary>
    /// This View Model class contains data and behavior to support 
    /// stock transactions UI components
    /// - Data for the UI components
    ///     - List of stock transactions
    ///     - Transaction's statistics/metrics
    /// - Behavior to support a stock transaction model: 
    ///     - Load/persist stock transaction data
    ///     - CRUD operation on a stock transaction
    ///     
    /// Notes:
    ///     - This view model does not have behavior or data for stock's, 
    ///     see StockAssetViewModel
    /// </summary>    
    public class TransactionViewModel
    {
        //private readonly IStockAssetDataProvider dataProvider;
        //private readonly BindingList<AssetTransaction> StockTransactionBindingList = new();
        internal readonly AssetTransaction Transaction;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="t">Transaction object</param>
        public TransactionViewModel(AssetTransaction t)
        {
            this.Transaction = t;
        }

        /// <summary>
        /// 
        /// </summary>
        public string TransactionDate
        {
            get
            {
                return
                    $"{Transaction.TransactionDate:MMM/dd}";
            }
        }

        public string TransactionDisplayType
        {
            get
            {
                return $"{Transaction.TransactionAction.Description().ToUpper()} - {Transaction.TransactionType.Description().ToUpper()}";
            }
        }


        public int DisplayQuantity
        {
            get
            {
                return (Transaction.TransactionAction == AssetTransactionAction.Sold &&
                    (Transaction.TransactionType == AssetTransactionType.Share ||
                    Transaction.TransactionType == AssetTransactionType.CoveredCall ||
                    Transaction.TransactionType == AssetTransactionType.CoveredPut)) ?
                    -1 * Transaction.Quantity : Transaction.Quantity;
            }
        }

        //[DisplayName("Price/Premium")]
        public string Price
        {
            get
            {
                {
                    return Transaction.IsOptionPlay() ?
                        $"(Exp. {Transaction.ExpiredDate:MMM/dd} @{Transaction.StrikePrice:N} {Transaction.Price:C})" :
                    /// $"{Transaction.Price:C}{Environment.NewLine}({Transaction.Fee:C} Fee, Strike @ {Transaction.StrikePrice:C})" :
                    $"{Transaction.Price:C}";

                }
            }
        }

        public string Notes
        {
            get
            {
                return Transaction.Message;
            }
        }


        [Browsable(false)]
        public bool IsInactive { get { return !Transaction.Active; } }

        /// <summary>
        /// Matches the transaction Id
        /// </summary>
        /// <param name="transactionId">Transaction Id to compare with</param>
        /// <returns>True if the transaction Id is matched, false otherwise</returns>
        public bool HasId(int transactionId)
        {
            return Transaction.TransactionId == transactionId;
        }

        public AssetTransaction GetTransaction()
        {
            return Transaction;
        }

        public bool IsActive()
        {
            return Transaction.Active;
        }

        public int GetTransactionId()
        {
            return Transaction.TransactionId;
        }
    }
}
