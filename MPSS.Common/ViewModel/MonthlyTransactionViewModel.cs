﻿using MPSS.Common.Model;

namespace MPSS.Common.ViewModel
{
    public class MonthlyTransactionViewModel : TransactionViewModel
    {
        public MonthlyTransactionViewModel(AssetTransaction t) : base(t)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public string Symbol
        {
            get
            {
                return Transaction.AssetSymbol.ToUpper();
            }
        }


    }
}
