﻿using MPSS.Common.Model.FinnHub;
using System.Threading.Tasks;

namespace MPSS.Common.DataService
{
    /// <summary>
    /// The interface to provide stock real-time data
    /// </summary>
    public interface IStockDataService
    {
        /// <summary>
        /// Retrieve current stock price
        /// </summary>
        /// <param name="symbol">Stock symbol</param>
        /// <returns>Stock price</returns>
        Task<Quote> GetStockPriceAsync(string symbol);
    }
}
