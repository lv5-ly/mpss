﻿using Microsoft.Extensions.Caching.Memory;
using MPSS.Common.Model;
using MPSS.Common.Model.FinnHub;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MPSS.Common.DataService
{
    /// <summary>
    /// The implementation to provide stock real-time data from FinnHub.io
    /// API service
    /// </summary>
    public class FinnHubStockDataService : IStockDataService
    {
        // Cache the stock data
        private readonly MemoryCache cache;

        // Sandbox API Key on FinnHub
        private readonly string API_KEY = "sandbox_c1h3jdf48v6t9ghtltmg";
        private readonly string BASE_URI = "api/v1/";
        private readonly string URL = "https://finnhub.io/";

        public FinnHubStockDataService(StockQuoteCache sqc)
        {
            cache = sqc.Cache;
        }

        /// <summary>
        /// Retrieve current stock price
        /// </summary>
        /// <param name="symbol">Stock symbol</param>
        /// <returns>Stock price</returns>
        public async Task<Quote> GetStockPriceAsync(string symbol)
        {
            //if (cache.Contains(symbol))
            if (!cache.TryGetValue(symbol, out Quote cacheEntry))
            {
                var Url = $"{URL}{BASE_URI}quote?token={API_KEY}&symbol={symbol}";

                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Get, Url))
                using (var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead))
                {
                    response.EnsureSuccessStatusCode();

                    var stream = await response.Content.ReadAsStringAsync();
                    // result = JsonConvert.DeserializeObject<Quote>(stream);
                    cacheEntry = JsonConvert.DeserializeObject<Quote>(stream);
                }
                //var policy = new CacheItemPolicy()
                //{
                //    AbsoluteExpiration = DateTime.Now.AddMinutes(1),
                //    SlidingExpiration = TimeSpan.FromSeconds(30),
                //    UpdateCallback = UpdateCallback
                //};
                //cache.Add(symbol, result, policy);

                // Key not in cache, so get data.
                //cacheEntry = DateTime.Now.TimeOfDay.ToString();

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    // Set cache entry size by extension method.
                    .SetSize(1)
                    // Keep in cache for this time, reset time if accessed.
                    .SetSlidingExpiration(TimeSpan.FromSeconds(3));

                // Set cache entry size via property.
                // cacheEntryOptions.Size = 1;

                // Save data in cache.
                cache.Set(symbol, cacheEntry, cacheEntryOptions);
            }

            return cacheEntry;
        }

        //public void UpdateCallback(CacheEntryUpdateArguments arguments)
        //{
        //    Quote cacheObject = (Quote)arguments.Source.Get(arguments.Key);

        //}
    }
}
