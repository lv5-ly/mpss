﻿using Microsoft.Extensions.Caching.Memory;
using MPSS.Common.Model;
using MPSS.Common.Model.Tradier;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace MPSS.Common.DataProvider
{
    public class TradierStockOptionDataService
    {
        // Cache the stock data
        private readonly MemoryCache cache;

        // Sandbox API Key on FinnHub
        private readonly static string API_KEY = "7kUwThxYKHG2p7dy3csZe3WLKb4K";
        private readonly static string BASE_URL = "https://sandbox.tradier.com/v1/";
        private readonly static string OPTION_CHAIN = $"{BASE_URL}markets/options/chains?";
        private readonly static string QUOTE = $"{BASE_URL}markets/quotes?";
        private readonly static string OPTION_CHAIN_URL = $"{OPTION_CHAIN}greeks=true&";
        private readonly static string QUOTE_URL = $"{QUOTE}greeks=true&";

        private readonly IHttpClientFactory factory;

        public TradierStockOptionDataService(OptionChainCache occ)
        {
            cache = occ.Cache;
        }

        public TradierStockOptionDataService(IHttpClientFactory f)
        {
            factory = f;
        }

        /// <summary>
        /// A method to retrieve option chain data from Tradier
        /// </summary>
        /// <param name="ticker">Stock ticker/symbol</param>
        /// <param name="expirationDate">Option expiration date</param>
        /// <returns>OptionChain object</returns>
        public OptionChain GetOptionChain(string symbol, DateTime expirationDate)
        {
            OptionChain optionChain = null;
            var Url = $"{OPTION_CHAIN_URL}symbol={symbol}&expiration={expirationDate:yyyy-MM-dd}";
            try
            {
                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Get, Url))
                {
                    request.Headers.Add("Authorization", $"Bearer {API_KEY}");
                    request.Headers.Add("Accept", "application/json");

                    using var response = client.Send(request, HttpCompletionOption.ResponseContentRead);
                    response.EnsureSuccessStatusCode();

                    var content = response.Content.ReadAsStringAsync().Result;
                    optionChain = JsonConvert.DeserializeObject<OptionChain>(content);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unexpected exception! {ex.Message}");
            }
            return optionChain;
        }

        /// <summary>
        /// A method to retrieve stock quote
        /// </summary>
        /// <param name="ticker">Stock ticker/symbol</param>
        /// <returns>OptionChain object</returns>
        public QuotesRoot GetQuote(string symbol)
        {
            QuotesRoot quote = null;
            var Url = $"{QUOTE_URL}symbols={symbol}";
            try
            {
                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Get, Url))
                {
                    request.Headers.Add("Authorization", $"Bearer {API_KEY}");
                    request.Headers.Add("Accept", "application/json");

                    using var response = client.Send(request, HttpCompletionOption.ResponseContentRead);
                    response.EnsureSuccessStatusCode();

                    var content = response.Content.ReadAsStringAsync().Result;
                    quote = JsonConvert.DeserializeObject<QuotesRoot>(content);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unexpected exception! {ex.Message}");
            }
            return quote;
        }
    }
}
