﻿using MPSS.Common.Model.FinnHub;
using System.Threading.Tasks;

namespace MPSS.Common.DataService
{
    /// <summary>
    /// The implementation to provide stock real-time data from AlphaVantage
    /// API service
    /// </summary>

    public class AlphaVantageStockDataService : IStockDataService
    {
        //private readonly MemoryCache _cache = new(new MemoryCacheOptions()
        //{            
        //    SizeLimit = 1024
        //});

        /// <summary>
        /// Retrieve current stock price
        /// </summary>
        /// <param name="symbol">Stock symbol</param>
        /// <returns>Stock price</returns>
        public async Task<Quote> GetStockPriceAsync(string symbol)
        {
            await Task.CompletedTask;
            return null;
        }
    }
}
