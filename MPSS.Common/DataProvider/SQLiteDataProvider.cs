﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using MPSS.Common.DataService;
using MPSS.Common.Model;
using System.Collections.Generic;
using System.Linq;

namespace MPSS.DataAccess
{
    public class SQLiteDataProvider : IStockAssetDataProvider
    {
        private readonly DBFile databaseFile;

        public SQLiteDataProvider(DBFile dbFile)
        {
            databaseFile = dbFile;
        }

        /// <summary>
        /// Data layer is to persist this record
        /// </summary>
        /// <param name="t">Transaction</param>
        /// <returns>The persisted transaction</returns>
        public AssetTransaction AddAssetTransaction(AssetTransaction t)
        {
            EntityEntry<AssetTransaction> entry = null;
            //using (var db = new ApplicationDbContext(databaseFile.GetFileName()))
            using (var db = new MPSSContext(databaseFile.GetFileName()))
            {
                var sa = db.StockAssets
                    .Include("Transactions")
                    .FirstOrDefault(r => r.StockAssetId == t.Stock.StockAssetId);
                //var sa = t.Stock;
                if (t.TransactionType == AssetTransactionType.Share)
                {
                    sa.Shares += (t.TransactionAction == AssetTransactionAction.Bought) ? t.Quantity :
                        (-1 * t.Quantity);
                    db.SaveChanges();
                }
                var newTransaction = new AssetTransaction(t, sa);
                entry = db.AssetTransactions.Add(newTransaction);
                db.SaveChanges();
            }
            return entry.Entity;
        }

        public StockAsset AddNewStockAsset(AssetTransaction t)
        {
            StockAsset sa = new(t);
            //using (var db = new ApplicationDbContext(databaseFile.GetFileName()))
            using (var db = new MPSSContext(databaseFile.GetFileName()))
            {
                db.AssetTransactions.Add(new AssetTransaction(t, db.StockAssets.Add(sa).Entity));
                db.SaveChanges();
            }
            return sa;
        }

        public void DeleteTransaction(int transactionId)
        {
            // using var db = new ApplicationDbContext(databaseFile.GetFileName());
            using var db = new MPSSContext(databaseFile.GetFileName());
            var t = db.AssetTransactions.FirstOrDefault(r => r.TransactionId == transactionId);
            if (t != null)
            {
                // Let's try to remove any foreign key transaction property records, if any
                db.TransactionProperties.RemoveRange(
                    db.TransactionProperties.Where(r => r.TransactionId == t.TransactionId));

                db.Remove(t);
                db.SaveChanges();
            }
        }

        public IEnumerable<StockAsset> LoadStockAssets()
        {
            IEnumerable<StockAsset> result;
            //using (var db = new ApplicationDbContext(databaseFile.GetFileName()))
            using (var db = new MPSSContext(databaseFile.GetFileName(), true))
            {
                result = db.StockAssets
                    .Include("Transactions")
                    .Include("Transactions.TransactionProperties")
                    .ToList();
            }
            return result;
        }

        /// <summary>
        /// Remove a stock asset record from database, by Id
        /// </summary>
        /// <param name="stockAssetId">Stock record Id</param>
        public void RemoveStockAsset(int stockAssetId)
        {
            //using var db = new ApplicationDbContext(databaseFile.GetFileName());
            using var db = new MPSSContext(databaseFile.GetFileName());
            var s = db.StockAssets.FirstOrDefault(r => r.StockAssetId == stockAssetId);
            if (s != null)
            {
                // Can't figure out how to tell EF to do 
                //  cascading delete, so here we have to remove 
                //  all the transactions first
                var transactions = db.AssetTransactions
                    .Where(r => r.Stock == s);
                foreach (var t in transactions)
                {
                    RemoveTransactionProperties(t, db);
                    db.AssetTransactions.Remove(t);
                }
                db.StockAssets.Remove(s);
                db.SaveChanges();
            }
        }

        private static void RemoveTransactionProperties(AssetTransaction t, MPSSContext db)
        {
            var properties = db.TransactionProperties
                .Where(p => p.TransactionId == t.TransactionId);
            foreach (var p in properties)
            {
                db.TransactionProperties.Remove(p);
            }
        }

        /// <summary>
        /// Update & persist the stock record
        /// Usually, only (number of) shares is only property get updated
        /// </summary>
        /// <param name="stock">Stock entity</param>
        public void UpdateStock(StockAsset stock)
        {
            //using var db = new ApplicationDbContext(databaseFile.GetFileName());
            using var db = new MPSSContext(databaseFile.GetFileName());
            var s = db.StockAssets.FirstOrDefault(r => r.StockAssetId == stock.StockAssetId);
            if (s != null)
            {
                s.Shares = stock.Shares;
                db.SaveChanges();
            }
        }

        public void UpdateTransaction(AssetTransaction currentSelectedTransaction)
        {
            //using var db = new ApplicationDbContext(databaseFile.GetFileName());
            using var db = new MPSSContext(databaseFile.GetFileName());
            var t = db.AssetTransactions.FirstOrDefault(r => r.TransactionId == currentSelectedTransaction.TransactionId);
            if (t != null)
            {
                // User can change following properties
                t.ExpiredDate = currentSelectedTransaction.ExpiredDate;
                t.Fee = currentSelectedTransaction.Fee;
                t.Price = currentSelectedTransaction.Price;
                t.Quantity = currentSelectedTransaction.Quantity;
                t.StrikePrice = currentSelectedTransaction.StrikePrice;
                t.TransactionDate = currentSelectedTransaction.TransactionDate;
                // t.ParentTransactionId = currentSelectedTransaction.ParentTransactionId;
                // t.TransactionType = currentSelectedTransaction.TransactionType;
                // t.TransactionAction = currentSelectedTransaction.TransactionAction;

                // The application update the following properties
                t.Status = currentSelectedTransaction.Status;
                t.Active = currentSelectedTransaction.Active;
                t.Hide = currentSelectedTransaction.Hide;

                t.Message = currentSelectedTransaction.Message;

                if (t.TransactionProperties != null)
                {
                    t.TransactionProperties.Clear();
                }
                else
                {
                    t.TransactionProperties = new List<TransactionProperty>();
                }
                foreach (var p in currentSelectedTransaction.TransactionProperties)
                {
                    t.TransactionProperties.Add(p);
                }
                db.SaveChanges();
            }
        }
    }
}
