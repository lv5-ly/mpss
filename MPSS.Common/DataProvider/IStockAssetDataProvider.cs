﻿using MPSS.Common.Model;
using System.Collections.Generic;

namespace MPSS.Common.DataService
{
    public interface IStockAssetDataProvider
    {
        IEnumerable<StockAsset> LoadStockAssets();
        //void SaveStockAsset(StockAsset asset);
        //IEnumerable<AssetTransaction> LoadAssetTransactions(String assetSymbol);
        //void SaveAssetTransaction(AssetTransaction assetTransaction);

        /// <summary>
        /// Data layer is to persist this record
        /// </summary>
        /// <param name="t">Transaction</param>
        /// <returns>The persisted transaction</returns>
        AssetTransaction AddAssetTransaction(AssetTransaction t);
        //bool IsNewStockAsset(string assetSymbol);
        StockAsset AddNewStockAsset(AssetTransaction t);
        //StockAsset MergeAssetTransaction(AssetTransaction t);
        void UpdateTransaction(AssetTransaction currentSelectedTransaction);
        void DeleteTransaction(int transactionId);
        //StockAsset ReloadStockAsset(int stockAssetId);

        /// <summary>
        /// Update & persist the stock record
        /// Usually, only (number of) shares is only property get updated
        /// </summary>
        /// <param name="stock">Stock entity</param>
        void UpdateStock(StockAsset currentSelectedAsset);

        /// <summary>
        /// Remove a stock asset record from database, by Id
        /// </summary>
        /// <param name="stockAssetId">Stock record Id</param>
        void RemoveStockAsset(int stockAssetId);

        /// <summary>
        /// Have data provider to create a StockAsset record
        /// </summary>
        /// <param name="assetSymbol">Stock symbol</param>
        /// <returns>New Stock Asset object</returns>
        // StockAsset AddNewStockAsset(string assetSymbol);
    }
}
