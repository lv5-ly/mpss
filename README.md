# The Name
This project is a small utility application to keep track of profit and loss for an active stock trader, this implements Mr. Paint's Spreadsheet, thus the name MPSS.

# Description
This is written in C#, as a Windows Desktop Application, using .NET 5. The decision to make this as a desktop app instead of Web Application is easy to make, since it will be used by all of us, and we all have
- Windows 10 OS laptop or desktop
- We want to keep the data private, that means we do not want the data stored in remote server
- We want data to be isolated, that means, one user cannot (physically) have access to other's data
- Easy to implement and distribute

# Download & Install
Here is the link for you to download & install: https://mpss.lv5.org

Keep in mind that your computer will warn you about installing a software without any certificate. 
Since we cannot afford to acquire a Software certificate, you have to click to accept the risk and 
install.

# How To Use
We want this application to be intuitive and easy to operate; however we want describe the typical work-flow, based on Mr. Paint's spreadsheet:
1. User enters the data when he just bought or acquired stock shares (stock symbol, shares, price, fee)  
1. Once the user has the shares, he can then make a time-limit covered call on those shares
1. After that, user can enter additional information on that covered call, when the call is closed/filled

Based on the entered data, the application calculates and presents P&L

## Typical Scenario
Here is just one of a typical scenario on how you would use this application
1. Bought Shares. 
	You just bought shares with your broker, so you click on Bought button to enter this data
1. Option Play
	If you have more than 100 shares, it's more likely that you will try to do a Covered Call option trade; 
	and if you did, you want to click on this Option Play button to enter the information. Once entered you 
	will see the updated P&L in the Summary and Monthly panels.
1. Close
	If you SOLD any share, or your option play just expired, you want to select the appropriate transaction, 
	and click on Close button, to enter the information. Your monthly P&L will be updated for you.
