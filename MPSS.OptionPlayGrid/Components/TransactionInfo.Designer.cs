﻿
namespace MPSS.Controls.Components
{
    partial class TransactionInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GB = new System.Windows.Forms.GroupBox();
            this.AdjustedPriceLabel = new System.Windows.Forms.Label();
            this.OriginalPriceLabel = new System.Windows.Forms.Label();
            this.TPremium = new System.Windows.Forms.Label();
            this.TStrike = new System.Windows.Forms.Label();
            this.TExpire = new System.Windows.Forms.Label();
            this.TDate = new System.Windows.Forms.Label();
            this.TType = new System.Windows.Forms.Label();
            this.TSymbol = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.GB.SuspendLayout();
            this.SuspendLayout();
            // 
            // GB
            // 
            this.GB.Controls.Add(this.AdjustedPriceLabel);
            this.GB.Controls.Add(this.OriginalPriceLabel);
            this.GB.Controls.Add(this.TPremium);
            this.GB.Controls.Add(this.TStrike);
            this.GB.Controls.Add(this.TExpire);
            this.GB.Controls.Add(this.TDate);
            this.GB.Controls.Add(this.TType);
            this.GB.Controls.Add(this.TSymbol);
            this.GB.Controls.Add(this.CloseButton);
            this.GB.Location = new System.Drawing.Point(0, 0);
            this.GB.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.GB.Name = "GB";
            this.GB.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.GB.Size = new System.Drawing.Size(900, 210);
            this.GB.TabIndex = 2;
            this.GB.TabStop = false;
            this.GB.Text = "Option Play Transaction";
            // 
            // AdjustedPriceLabel
            // 
            this.AdjustedPriceLabel.Location = new System.Drawing.Point(483, 86);
            this.AdjustedPriceLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.AdjustedPriceLabel.Name = "AdjustedPriceLabel";
            this.AdjustedPriceLabel.Size = new System.Drawing.Size(219, 30);
            this.AdjustedPriceLabel.TabIndex = 8;
            this.AdjustedPriceLabel.Text = "Adjusted Price: $123.45";
            this.AdjustedPriceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OriginalPriceLabel
            // 
            this.OriginalPriceLabel.Location = new System.Drawing.Point(483, 38);
            this.OriginalPriceLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.OriginalPriceLabel.Name = "OriginalPriceLabel";
            this.OriginalPriceLabel.Size = new System.Drawing.Size(219, 30);
            this.OriginalPriceLabel.TabIndex = 7;
            this.OriginalPriceLabel.Text = "Original Price: $123.45";
            this.OriginalPriceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TPremium
            // 
            this.TPremium.AutoSize = true;
            this.TPremium.Location = new System.Drawing.Point(315, 86);
            this.TPremium.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TPremium.Name = "TPremium";
            this.TPremium.Size = new System.Drawing.Size(97, 30);
            this.TPremium.TabIndex = 6;
            this.TPremium.Text = "For $2.50";
            // 
            // TStrike
            // 
            this.TStrike.AutoSize = true;
            this.TStrike.Location = new System.Drawing.Point(19, 152);
            this.TStrike.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TStrike.Name = "TStrike";
            this.TStrike.Size = new System.Drawing.Size(153, 30);
            this.TStrike.TabIndex = 5;
            this.TStrike.Text = "Strike at $20.50";
            // 
            // TExpire
            // 
            this.TExpire.AutoSize = true;
            this.TExpire.Location = new System.Drawing.Point(200, 152);
            this.TExpire.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TExpire.Name = "TExpire";
            this.TExpire.Size = new System.Drawing.Size(198, 30);
            this.TExpire.TabIndex = 4;
            this.TExpire.Text = "Expire on 8/20/2021";
            // 
            // TDate
            // 
            this.TDate.AutoSize = true;
            this.TDate.Location = new System.Drawing.Point(171, 86);
            this.TDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TDate.Name = "TDate";
            this.TDate.Size = new System.Drawing.Size(140, 30);
            this.TDate.TabIndex = 3;
            this.TDate.Text = "On 8/20/2021";
            // 
            // TType
            // 
            this.TType.AutoSize = true;
            this.TType.Location = new System.Drawing.Point(171, 38);
            this.TType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TType.Name = "TType";
            this.TType.Size = new System.Drawing.Size(228, 30);
            this.TType.TabIndex = 2;
            this.TType.Text = "SOLD COVERED - CALL";
            // 
            // TSymbol
            // 
            this.TSymbol.Location = new System.Drawing.Point(19, 38);
            this.TSymbol.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.TSymbol.Name = "TSymbol";
            this.TSymbol.Size = new System.Drawing.Size(142, 78);
            this.TSymbol.TabIndex = 1;
            this.TSymbol.Text = "Symbol";
            this.TSymbol.UseVisualStyleBackColor = true;
            // 
            // CloseButton
            // 
            this.CloseButton.Location = new System.Drawing.Point(705, 138);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(173, 58);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // TransactionInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.GB);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "TransactionInfo";
            this.Size = new System.Drawing.Size(902, 210);
            this.Load += new System.EventHandler(this.TransactionInfo_Load);
            this.GB.ResumeLayout(false);
            this.GB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GB;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Label TPremium;
        private System.Windows.Forms.Label TStrike;
        private System.Windows.Forms.Label TExpire;
        private System.Windows.Forms.Label TDate;
        private System.Windows.Forms.Label TType;
        private System.Windows.Forms.Button TSymbol;
        private System.Windows.Forms.Label AdjustedPriceLabel;
        private System.Windows.Forms.Label OriginalPriceLabel;
    }
}
