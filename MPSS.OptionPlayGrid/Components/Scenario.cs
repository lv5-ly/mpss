﻿using MPSS.Common.Model;
using System;
using System.Windows.Forms;

namespace MPSS.Controls.Components
{
    public class Scenario : UserControl
    {
        internal ScenarioCalculator Calculator;

        public void SetCalculator(ScenarioCalculator calculator)
        {
            this.Calculator = calculator;
        }

        public decimal ToDecimal(string v)
        {
            return string.IsNullOrEmpty(v)
                ? 0M
                : Convert.ToDecimal(v);
        }
    }
}
