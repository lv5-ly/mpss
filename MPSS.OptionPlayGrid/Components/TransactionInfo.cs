﻿using MPSS.Common.Model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MPSS.Controls.Components
{
    public partial class TransactionInfo : UserControl
    {
        private AssetTransaction transaction { get; set; } = null;
        public string Title
        {
            set
            {
                GB.Text = value;
            }
        }

        public AssetTransaction Transaction { set { transaction = value; } }

        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("Close button clicked")]
        public event EventHandler<StockTransactionEventArgs> CloseButtonClick;
        #endregion Public Properties

        private static void RaiseEvent(
            EventHandler<StockTransactionEventArgs> eventHandler,
            object sender,
            StockTransactionEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }

        public TransactionInfo()
        {
            InitializeComponent();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            var eventArgs = new StockTransactionEventArgs(transaction);
            RaiseEvent(CloseButtonClick, this, eventArgs);
        }

        private void TransactionInfo_Load(object sender, EventArgs e)
        {
            if (transaction.Stock != null)
            {
                var originalPrice = transaction.Stock.GetAverageStockPrice();
                var adjustedPrice = transaction.Stock.AdjCost;
                OriginalPriceLabel.Text = $"Original Price: {originalPrice:C}";
                AdjustedPriceLabel.Text = $"Adjusted Price: {adjustedPrice:C}";
                OriginalPriceLabel.Visible = true;
                AdjustedPriceLabel.Visible = true;
            }
            else
            {
                OriginalPriceLabel.Visible = false;
                AdjustedPriceLabel.Visible = false;
            }
            TSymbol.Text = $"{transaction?.AssetSymbol.ToUpper()}";
            TType.Text = $"{transaction?.TransactionDisplayType}";
            TDate.Text = $"On {transaction.TransactionDate.ToShortDateString()}";
            TPremium.Text = $"For {transaction.Price:C}";
            TStrike.Text = $"Strike at {transaction.StrikePrice:C}";
            if (transaction.IsSpreadPlay)
            {
                TStrike.Text = $"Strike at {transaction.LowerStrikePrice:C} - {transaction.UpperStrikePrice:C}";
            }
            TExpire.Text = $"{(transaction.Quantity/100).ToString("D")} contracts Expire on {transaction.ExpiredDate.ToShortDateString()}";
        }
    }
}
