﻿
namespace MPSS.Controls.Components
{
    partial class CCRollScenario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CCRoll_CalculateButton = new System.Windows.Forms.Button();
            this.StrikePrice = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SellNewCCPremium = new System.Windows.Forms.TextBox();
            this.SellingCCPremium = new System.Windows.Forms.Label();
            this.BuyBackCCPremium = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CCRoll_CalculateButton);
            this.groupBox2.Controls.Add(this.StrikePrice);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.SellNewCCPremium);
            this.groupBox2.Controls.Add(this.SellingCCPremium);
            this.groupBox2.Controls.Add(this.BuyBackCCPremium);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(522, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Premiums && Strike Price";
            // 
            // CCRoll_CalculateButton
            // 
            this.CCRoll_CalculateButton.Location = new System.Drawing.Point(411, 55);
            this.CCRoll_CalculateButton.Name = "CCRoll_CalculateButton";
            this.CCRoll_CalculateButton.Size = new System.Drawing.Size(101, 29);
            this.CCRoll_CalculateButton.TabIndex = 8;
            this.CCRoll_CalculateButton.Text = "Calculate";
            this.CCRoll_CalculateButton.UseVisualStyleBackColor = true;
            this.CCRoll_CalculateButton.Click += new System.EventHandler(this.CCRoll_CalculateButton_Click);
            // 
            // StrikePrice
            // 
            this.StrikePrice.Location = new System.Drawing.Point(231, 59);
            this.StrikePrice.Name = "StrikePrice";
            this.StrikePrice.Size = new System.Drawing.Size(44, 23);
            this.StrikePrice.TabIndex = 5;
            this.StrikePrice.Text = "0.00";
            this.StrikePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.StrikePrice.TextChanged += new System.EventHandler(this.StrikePrice_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Strike Price";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SellNewCCPremium
            // 
            this.SellNewCCPremium.Location = new System.Drawing.Point(95, 59);
            this.SellNewCCPremium.Name = "SellNewCCPremium";
            this.SellNewCCPremium.Size = new System.Drawing.Size(44, 23);
            this.SellNewCCPremium.TabIndex = 3;
            this.SellNewCCPremium.Text = "0.00";
            this.SellNewCCPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SellNewCCPremium.TextChanged += new System.EventHandler(this.SellNewCCPremium_TextChanged);
            // 
            // SellingCCPremium
            // 
            this.SellingCCPremium.AutoSize = true;
            this.SellingCCPremium.Location = new System.Drawing.Point(13, 62);
            this.SellingCCPremium.Name = "SellingCCPremium";
            this.SellingCCPremium.Size = new System.Drawing.Size(80, 15);
            this.SellingCCPremium.TabIndex = 2;
            this.SellingCCPremium.Text = "-CC Premium";
            this.SellingCCPremium.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyBackCCPremium
            // 
            this.BuyBackCCPremium.Location = new System.Drawing.Point(95, 30);
            this.BuyBackCCPremium.Name = "BuyBackCCPremium";
            this.BuyBackCCPremium.Size = new System.Drawing.Size(44, 23);
            this.BuyBackCCPremium.TabIndex = 1;
            this.BuyBackCCPremium.Text = "0.00";
            this.BuyBackCCPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.BuyBackCCPremium.TextChanged += new System.EventHandler(this.BuyBackCCPremium_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "+CC Premium";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CCRollScenario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Name = "CCRollScenario";
            this.Size = new System.Drawing.Size(523, 104);
            this.Load += new System.EventHandler(this.CCRollScenario_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button CCRoll_CalculateButton;
        private System.Windows.Forms.TextBox StrikePrice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SellNewCCPremium;
        private System.Windows.Forms.Label SellingCCPremium;
        private System.Windows.Forms.TextBox BuyBackCCPremium;
        private System.Windows.Forms.Label label1;
    }
}
