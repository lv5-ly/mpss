﻿using MPSS.Common.Model;
using System.Windows.Forms;

namespace MPSS.Controls.Components
{
    public partial class KeyAndValueGrid : UserControl
    {
        private string LeftHeader { get; set; } = "If/When";
        private string RightHeader { get; set; } = "Value";

        public KeyAndValueGrid()
        {
            InitializeComponent();
        }
        public string Title { get { return GroupBox.Text; } set { GroupBox.Text = value; } }

        internal void SetBindingData(SortableBindingList<KeyAndValue> bindingList)
        {
            DataGrid.DataSource = new BindingSource(bindingList, null);
        }

        internal void SetHeaders(string leftHeader, string rightHeader)
        {
            LeftHeader = leftHeader;
            RightHeader = rightHeader;
        }

        private void DataGrid_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (DataGrid.Columns.Count > 0)
            {
                DataGrid.Columns[0].HeaderText = LeftHeader;
                DataGrid.Columns[1].HeaderText = RightHeader;
            }
        }
    }
}
