﻿
namespace MPSS.Controls.Components
{
    partial class BuyBackCCToSellScenario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BBCCToSell_CalculateButton = new System.Windows.Forms.Button();
            this.MarketPrice = new System.Windows.Forms.TextBox();
            this.SellingCCPremium = new System.Windows.Forms.Label();
            this.BuyBackCCPremium = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BBCCToSell_CalculateButton
            // 
            this.BBCCToSell_CalculateButton.Enabled = false;
            this.BBCCToSell_CalculateButton.Location = new System.Drawing.Point(411, 55);
            this.BBCCToSell_CalculateButton.Name = "BBCCToSell_CalculateButton";
            this.BBCCToSell_CalculateButton.Size = new System.Drawing.Size(101, 29);
            this.BBCCToSell_CalculateButton.TabIndex = 8;
            this.BBCCToSell_CalculateButton.Text = "Calculate";
            this.BBCCToSell_CalculateButton.UseVisualStyleBackColor = true;
            this.BBCCToSell_CalculateButton.Click += new System.EventHandler(this.BBCCToSell_CalculateButton_Click);
            // 
            // MarketPrice
            // 
            this.MarketPrice.Location = new System.Drawing.Point(95, 59);
            this.MarketPrice.Name = "MarketPrice";
            this.MarketPrice.Size = new System.Drawing.Size(44, 23);
            this.MarketPrice.TabIndex = 3;
            this.MarketPrice.Text = "0.00";
            this.MarketPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.MarketPrice.TextChanged += new System.EventHandler(this.MarketPrice_TextChanged);
            // 
            // SellingCCPremium
            // 
            this.SellingCCPremium.AutoSize = true;
            this.SellingCCPremium.Location = new System.Drawing.Point(19, 62);
            this.SellingCCPremium.Name = "SellingCCPremium";
            this.SellingCCPremium.Size = new System.Drawing.Size(73, 15);
            this.SellingCCPremium.TabIndex = 2;
            this.SellingCCPremium.Text = "Market Price";
            this.SellingCCPremium.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BuyBackCCPremium
            // 
            this.BuyBackCCPremium.Location = new System.Drawing.Point(95, 30);
            this.BuyBackCCPremium.Name = "BuyBackCCPremium";
            this.BuyBackCCPremium.Size = new System.Drawing.Size(44, 23);
            this.BuyBackCCPremium.TabIndex = 1;
            this.BuyBackCCPremium.Text = "0.00";
            this.BuyBackCCPremium.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.BuyBackCCPremium.TextChanged += new System.EventHandler(this.BuyBackCCPremium_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "+CC Premium";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BBCCToSell_CalculateButton);
            this.groupBox2.Controls.Add(this.MarketPrice);
            this.groupBox2.Controls.Add(this.SellingCCPremium);
            this.groupBox2.Controls.Add(this.BuyBackCCPremium);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(1, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(522, 100);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Premiums && Market Price";
            // 
            // BuyBackCCToSellScenario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Name = "BuyBackCCToSellScenario";
            this.Size = new System.Drawing.Size(523, 104);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BBCCToSell_CalculateButton;
        private System.Windows.Forms.TextBox MarketPrice;
        private System.Windows.Forms.Label SellingCCPremium;
        private System.Windows.Forms.TextBox BuyBackCCPremium;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}
