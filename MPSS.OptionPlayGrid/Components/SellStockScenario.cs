﻿using MPSS.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MPSS.Controls.Components
{
    public partial class SellStockScenario : Scenario
    {
        public bool IsResultReady
        {
            get
            {
                var CurrentMarketPrice = ToDecimal(MarketPrice.Text);
                return CurrentMarketPrice > 0M;
            }
        }

        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("Sell Stock Scenario Result Ready")]
        public event EventHandler<SellStockScenarioResultEventArgs> ScenarioResultReady;
        #endregion Public Properties

        private static void RaiseEvent(
            EventHandler<SellStockScenarioResultEventArgs> eventHandler,
            object sender, SellStockScenarioResultEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }

        public SellStockScenario()
        {
            InitializeComponent();
        }

        private void MarketPrice_TextChanged(object sender, EventArgs e)
        {
            SellStock_CalculateButton.Enabled = IsResultReady;
        }

        private void SellStock_CalculateButton_Click(object sender, EventArgs e)
        {
            Dictionary<string, decimal> result = Calculator.CalculateSellStock(
                Convert.ToDecimal(MarketPrice.Text));

            var args = new SellStockScenarioResultEventArgs(result);
            RaiseEvent(ScenarioResultReady, this, args);
        }
    }
}
