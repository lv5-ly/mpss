﻿
namespace MPSS.Controls.Components
{
    partial class SellStockScenario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.MarketPrice = new System.Windows.Forms.TextBox();
            this.MarketPriceLabel = new System.Windows.Forms.Label();
            this.SellStock_CalculateButton = new System.Windows.Forms.Button();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.MarketPrice);
            this.groupBox7.Controls.Add(this.MarketPriceLabel);
            this.groupBox7.Controls.Add(this.SellStock_CalculateButton);
            this.groupBox7.Location = new System.Drawing.Point(0, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(522, 100);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Stock Market Price";
            // 
            // MarketPrice
            // 
            this.MarketPrice.Location = new System.Drawing.Point(101, 22);
            this.MarketPrice.Name = "MarketPrice";
            this.MarketPrice.Size = new System.Drawing.Size(44, 23);
            this.MarketPrice.TabIndex = 10;
            this.MarketPrice.TextChanged += new System.EventHandler(this.MarketPrice_TextChanged);
            // 
            // MarketPriceLabel
            // 
            this.MarketPriceLabel.AutoSize = true;
            this.MarketPriceLabel.Location = new System.Drawing.Point(25, 26);
            this.MarketPriceLabel.Name = "MarketPriceLabel";
            this.MarketPriceLabel.Size = new System.Drawing.Size(73, 15);
            this.MarketPriceLabel.TabIndex = 9;
            this.MarketPriceLabel.Text = "Market Price";
            this.MarketPriceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SellStock_CalculateButton
            // 
            this.SellStock_CalculateButton.Enabled = false;
            this.SellStock_CalculateButton.Location = new System.Drawing.Point(411, 55);
            this.SellStock_CalculateButton.Name = "SellStock_CalculateButton";
            this.SellStock_CalculateButton.Size = new System.Drawing.Size(101, 29);
            this.SellStock_CalculateButton.TabIndex = 8;
            this.SellStock_CalculateButton.Text = "Calculate";
            this.SellStock_CalculateButton.UseVisualStyleBackColor = true;
            this.SellStock_CalculateButton.Click += new System.EventHandler(this.SellStock_CalculateButton_Click);
            // 
            // SellStockScenario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox7);
            this.Name = "SellStockScenario";
            this.Size = new System.Drawing.Size(523, 104);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox MarketPrice;
        private System.Windows.Forms.Label MarketPriceLabel;
        private System.Windows.Forms.Button SellStock_CalculateButton;
    }
}
