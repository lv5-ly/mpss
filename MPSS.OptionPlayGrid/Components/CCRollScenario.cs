﻿using MPSS.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MPSS.Controls.Components
{
    public partial class CCRollScenario : Scenario
    {
        public bool IsResultReady
        {
            get
            {
                var MinusCC = ToDecimal(BuyBackCCPremium.Text);
                var PlusCC = ToDecimal(SellNewCCPremium.Text);
                var NewStrikePrice = ToDecimal(StrikePrice.Text);
                return MinusCC > 0M && PlusCC > 0M && NewStrikePrice > 0M;
            }
        }

        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("CC Roll Scenario Result Ready")]
        public event EventHandler<CCRollScenarioResultEventArgs> ScenarioResultReady;
        #endregion Public Properties

        public CCRollScenario()
        {
            InitializeComponent();
        }

        private static void RaiseEvent(
            EventHandler<CCRollScenarioResultEventArgs> eventHandler,
            object sender, CCRollScenarioResultEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }


        private void CCRoll_CalculateButton_Click(object sender, EventArgs e)
        {
            Dictionary<string, decimal> result = Calculator.CalculateCCRoll(
                Convert.ToDecimal(BuyBackCCPremium.Text),
                Convert.ToDecimal(SellNewCCPremium.Text),
                Convert.ToDecimal(StrikePrice.Text));

            var args = new CCRollScenarioResultEventArgs(result);
            RaiseEvent(ScenarioResultReady, this, args);
        }

        private void CCRollScenario_Load(object sender, EventArgs e)
        {
            CCRoll_CalculateButton.Enabled = IsResultReady;
        }

        private void BuyBackCCPremium_TextChanged(object sender, EventArgs e)
        {
            CCRoll_CalculateButton.Enabled = IsResultReady;
        }

        private void SellNewCCPremium_TextChanged(object sender, EventArgs e)
        {
            CCRoll_CalculateButton.Enabled = IsResultReady;
        }

        private void StrikePrice_TextChanged(object sender, EventArgs e)
        {
            CCRoll_CalculateButton.Enabled = IsResultReady;
        }
    }
}
