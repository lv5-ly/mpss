﻿using MPSS.Common.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MPSS.Controls.Components
{
    public partial class BuyBackCCToSellScenario : Scenario
    {
        public bool IsResultReady
        {
            get
            {
                var PlusCC = ToDecimal(BuyBackCCPremium.Text);
                var CurrentMarketPrice = ToDecimal(MarketPrice.Text);
                return PlusCC > 0M && CurrentMarketPrice > 0M;
            }
        }

        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("Buy-back CC To Sell Scenario Result Ready")]
        public event EventHandler<BBCCToSellScenarioResultEventArgs> ScenarioResultReady;
        #endregion Public Properties

        private static void RaiseEvent(
            EventHandler<BBCCToSellScenarioResultEventArgs> eventHandler,
            object sender, BBCCToSellScenarioResultEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }


        public BuyBackCCToSellScenario()
        {
            InitializeComponent();
        }

        private void BBCCToSell_CalculateButton_Click(object sender, EventArgs e)
        {
            Dictionary<string, decimal> result = Calculator.CalculateBBCCToSell(
                Convert.ToDecimal(BuyBackCCPremium.Text),
                Convert.ToDecimal(MarketPrice.Text));

            var args = new BBCCToSellScenarioResultEventArgs(result);
            RaiseEvent(ScenarioResultReady, this, args);
        }

        private void BuyBackCCPremium_TextChanged(object sender, EventArgs e)
        {
            BBCCToSell_CalculateButton.Enabled = IsResultReady;
        }

        private void MarketPrice_TextChanged(object sender, EventArgs e)
        {
            BBCCToSell_CalculateButton.Enabled = IsResultReady;
        }
    }
}
