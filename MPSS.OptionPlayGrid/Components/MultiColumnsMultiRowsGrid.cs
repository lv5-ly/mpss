﻿using MPSS.Common.Model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MPSS.Controls.Components
{
    public partial class MultiColumnsMultiRowsGrid : UserControl
    {
        private IRowContextMenu SelectedRowContextMenu;
        private Object SelectedRowItem;

        #region Public Properties

        [Browsable(true)]
        [Category("Action")]
        [Description("User double-clicks on a row")]
        public event EventHandler<StockTransactionEventArgs> RowDoubleClick;

        [Browsable(true)]
        [Category("Action")]
        [Description("User clicks on a context menu item")]
        public event EventHandler<StockTransactionEventArgs> ContextMenuItemClicked;
        public string Title { get { return GroupBox.Text; } set { GroupBox.Text = value; } }

        #endregion Public Properties

        #region Constructors

        public MultiColumnsMultiRowsGrid()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        internal void SetBindingData(SortableBindingList<ActiveOptionTransaction> activeOptionList)
        {
            DataGrid.DataSource = new BindingSource(activeOptionList, null);
        }

        internal void SetBindingData(SortableBindingList<AnnualSummary> l)
        {
            DataGrid.DataSource = new BindingSource(l, null);
        }

        #endregion

        #region Private Methods

        private static void RaiseEvent(EventHandler<StockTransactionEventArgs> eventHandler, object sender, StockTransactionEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }

        #endregion

        #region Event Handlers

        private void DataGrid_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            var dg = sender as DataGridView;

            dg.EnableHeadersVisualStyles = false;

            var type = ListBindingHelper.GetListItemType(dg.DataSource);
            var properties = TypeDescriptor.GetProperties(type);
            foreach (DataGridViewColumn column in dg.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Automatic;

                // Set up format and alignment based on type
                //var columnType = GetColumnValueType(column);
                //if(columnType == typeof(decimal))
                //{
                //    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //    // column.DefaultCellStyle.Format = "C";
                //}
                //else if (columnType == typeof(DateTime))
                //{
                //    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //    // column.DefaultCellStyle.Format = "d";
                //}
                //else if (columnType == typeof(int))
                //{
                //    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //    // column.DefaultCellStyle.Format = "N0";
                //}
                //else
                //{
                //    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                //}


                var p = properties[column.DataPropertyName];
                if (p != null)
                {
                    var format = p.Attributes[typeof(DisplayFormatAttribute)] as DisplayFormatAttribute;
                    column.DefaultCellStyle.Format = format?.Format;

                    var da = p.Attributes[typeof(DisplayAlignmentAttribute)] as DisplayAlignmentAttribute;
                    column.DefaultCellStyle.Alignment = da?.Alignment == DisplayAlignment.Left
                        ? DataGridViewContentAlignment.MiddleLeft
                        : da?.Alignment == DisplayAlignment.Right
                        ? DataGridViewContentAlignment.MiddleRight
                        : DataGridViewContentAlignment.MiddleCenter;

                    DisplayFillWeightAttribute rw = (DisplayFillWeightAttribute)p.Attributes[typeof(DisplayFillWeightAttribute)];
                    column.FillWeight = rw == null ? 100 : rw.Width;

                    column.ToolTipText = p.Description;
                }
            }
            dg.ClearSelection();
            dg.TabStop = false;
        }

        private void DataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var dg = sender as DataGridView;
            if (e.RowIndex >= 0 && dg.SelectedRows.Count == 1)
            {
                var activeOption = dg.Rows[e.RowIndex].DataBoundItem as ActiveOptionTransaction;
                var eventArgs = new StockTransactionEventArgs(activeOption.GetTransaction());
                RaiseEvent(RowDoubleClick, sender, eventArgs);
            }
        }

        private void DataGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var dg = sender as DataGridView;

            dg.ClearSelection();
        }

        private void DataGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var dg = sender as DataGridView;
            var row = dg.Rows[e.RowIndex];
            var iMCF = row.DataBoundItem as IMultiColumnFormat;
            row.DefaultCellStyle.BackColor = iMCF.GetBackgroundColor();
        }

        private void DataGrid_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            var dg = sender as DataGridView;
            var row = dg.Rows[e.RowIndex];
            SelectedRowItem = row.DataBoundItem;
            SelectedRowContextMenu = row.DataBoundItem as IRowContextMenu;
        }

        private void GridContextMenu_Opening(object sender, CancelEventArgs e)
        {
            var menu = sender as ContextMenuStrip;
            foreach (ToolStripMenuItem menuItem in menu.Items)
            {
                menuItem.Enabled = SelectedRowContextMenu.Validate(menuItem.Name);
            }
        }

        private void GridContextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var menuItem = e.ClickedItem;
            var eventArgs = new StockTransactionEventArgs(SelectedRowItem, menuItem.Name);
            RaiseEvent(ContextMenuItemClicked, sender, eventArgs);
        }
        #endregion
    }
}
