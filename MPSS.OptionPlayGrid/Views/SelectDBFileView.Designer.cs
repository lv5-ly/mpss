﻿
namespace MPSS.Controls.Views
{
    partial class SelectDBFileView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectDBFileGroupBox = new System.Windows.Forms.GroupBox();
            this.txbDBFileName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SelectDBFileComboBox = new System.Windows.Forms.ComboBox();
            this.SelectDBFileGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SelectDBFileGroupBox
            // 
            this.SelectDBFileGroupBox.Controls.Add(this.txbDBFileName);
            this.SelectDBFileGroupBox.Controls.Add(this.label1);
            this.SelectDBFileGroupBox.Controls.Add(this.SelectDBFileComboBox);
            this.SelectDBFileGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelectDBFileGroupBox.Location = new System.Drawing.Point(0, 0);
            this.SelectDBFileGroupBox.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.SelectDBFileGroupBox.Name = "SelectDBFileGroupBox";
            this.SelectDBFileGroupBox.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.SelectDBFileGroupBox.Size = new System.Drawing.Size(315, 194);
            this.SelectDBFileGroupBox.TabIndex = 1;
            this.SelectDBFileGroupBox.TabStop = false;
            this.SelectDBFileGroupBox.Text = "Select DB File";
            // 
            // txbDBFileName
            // 
            this.txbDBFileName.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.txbDBFileName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbDBFileName.Location = new System.Drawing.Point(10, 85);
            this.txbDBFileName.Multiline = true;
            this.txbDBFileName.Name = "txbDBFileName";
            this.txbDBFileName.ReadOnly = true;
            this.txbDBFileName.Size = new System.Drawing.Size(292, 81);
            this.txbDBFileName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-278, -67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // SelectDBFileComboBox
            // 
            this.SelectDBFileComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectDBFileComboBox.FormattingEnabled = true;
            this.SelectDBFileComboBox.Items.AddRange(new object[] {
            "Select file...",
            "MPSS.dbml"});
            this.SelectDBFileComboBox.Location = new System.Drawing.Point(10, 38);
            this.SelectDBFileComboBox.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.SelectDBFileComboBox.Name = "SelectDBFileComboBox";
            this.SelectDBFileComboBox.Size = new System.Drawing.Size(292, 38);
            this.SelectDBFileComboBox.TabIndex = 0;
            this.SelectDBFileComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectDBFileComboBox_SelectedIndexChanged);
            // 
            // SelectDBFileView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SelectDBFileGroupBox);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "SelectDBFileView";
            this.Size = new System.Drawing.Size(315, 194);
            this.SelectDBFileGroupBox.ResumeLayout(false);
            this.SelectDBFileGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox SelectDBFileGroupBox;
        private System.Windows.Forms.ComboBox SelectDBFileComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbDBFileName;
    }
}
