﻿
namespace MPSS.StatisticControl
{
    partial class StockMonthlyView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StockMonthlyStatisticGroupBox = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txbStkTotalPandL = new System.Windows.Forms.TextBox();
            this.txbStkYTDPandL = new System.Windows.Forms.TextBox();
            this.txbSimPandLStkAssign = new System.Windows.Forms.TextBox();
            this.txbSimPandLCC = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txbMonthlyPLRatio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbMonthlyPL = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbShares = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txbTicker = new System.Windows.Forms.TextBox();
            this.tbxAverageStockPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.optionPlayControl = new MPSS.OptionPlayControl.OptionPlayView();
            this.StockMonthlyViewPanel = new System.Windows.Forms.Panel();
            this.StockMonthlyStatisticGroupBox.SuspendLayout();
            this.StockMonthlyViewPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // StockMonthlyStatisticGroupBox
            // 
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label10);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label9);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label8);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbStkTotalPandL);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbStkYTDPandL);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbSimPandLStkAssign);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbSimPandLCC);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label7);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label6);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label5);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbMonthlyPLRatio);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label4);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbMonthlyPL);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label3);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbShares);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label2);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.txbTicker);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.tbxAverageStockPrice);
            this.StockMonthlyStatisticGroupBox.Controls.Add(this.label1);
            this.StockMonthlyStatisticGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.StockMonthlyStatisticGroupBox.Location = new System.Drawing.Point(0, 0);
            this.StockMonthlyStatisticGroupBox.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.StockMonthlyStatisticGroupBox.Name = "StockMonthlyStatisticGroupBox";
            this.StockMonthlyStatisticGroupBox.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.StockMonthlyStatisticGroupBox.Size = new System.Drawing.Size(1429, 168);
            this.StockMonthlyStatisticGroupBox.TabIndex = 0;
            this.StockMonthlyStatisticGroupBox.TabStop = false;
            this.StockMonthlyStatisticGroupBox.Text = "Stock Statistics";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 9.857143F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(584, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 32);
            this.label10.TabIndex = 18;
            this.label10.Text = "Monthly";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(265, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 30);
            this.label9.TabIndex = 17;
            this.label9.Text = "Stk Total P&&L";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(271, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 30);
            this.label8.TabIndex = 16;
            this.label8.Text = "Stk YTD P&&L";
            // 
            // txbStkTotalPandL
            // 
            this.txbStkTotalPandL.Location = new System.Drawing.Point(405, 121);
            this.txbStkTotalPandL.Name = "txbStkTotalPandL";
            this.txbStkTotalPandL.Size = new System.Drawing.Size(128, 35);
            this.txbStkTotalPandL.TabIndex = 15;
            this.txbStkTotalPandL.Text = "$999,999.99";
            // 
            // txbStkYTDPandL
            // 
            this.txbStkYTDPandL.Location = new System.Drawing.Point(405, 76);
            this.txbStkYTDPandL.Name = "txbStkYTDPandL";
            this.txbStkYTDPandL.Size = new System.Drawing.Size(128, 35);
            this.txbStkYTDPandL.TabIndex = 14;
            this.txbStkYTDPandL.Text = "$999.999.00";
            // 
            // txbSimPandLStkAssign
            // 
            this.txbSimPandLStkAssign.Location = new System.Drawing.Point(1107, 70);
            this.txbSimPandLStkAssign.Name = "txbSimPandLStkAssign";
            this.txbSimPandLStkAssign.Size = new System.Drawing.Size(134, 35);
            this.txbSimPandLStkAssign.TabIndex = 13;
            this.txbSimPandLStkAssign.Text = "$999,999.99";
            this.txbSimPandLStkAssign.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txbSimPandLCC
            // 
            this.txbSimPandLCC.Location = new System.Drawing.Point(936, 70);
            this.txbSimPandLCC.Name = "txbSimPandLCC";
            this.txbSimPandLCC.Size = new System.Drawing.Size(139, 35);
            this.txbSimPandLCC.TabIndex = 12;
            this.txbSimPandLCC.Text = "$999,999.99";
            this.txbSimPandLCC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1084, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(192, 30);
            this.label7.TabIndex = 11;
            this.label7.Text = "Sim Stk Assign P&&L";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(943, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(123, 30);
            this.label6.TabIndex = 10;
            this.label6.Text = "Sim CC P&&L";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(842, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 30);
            this.label5.TabIndex = 9;
            this.label5.Text = "P&&L %";
            // 
            // txbMonthlyPLRatio
            // 
            this.txbMonthlyPLRatio.Location = new System.Drawing.Point(843, 70);
            this.txbMonthlyPLRatio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbMonthlyPLRatio.Name = "txbMonthlyPLRatio";
            this.txbMonthlyPLRatio.Size = new System.Drawing.Size(73, 35);
            this.txbMonthlyPLRatio.TabIndex = 8;
            this.txbMonthlyPLRatio.Text = "100%";
            this.txbMonthlyPLRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(734, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 30);
            this.label4.TabIndex = 7;
            this.label4.Text = "P&&L";
            // 
            // txbMonthlyPL
            // 
            this.txbMonthlyPL.Location = new System.Drawing.Point(694, 70);
            this.txbMonthlyPL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbMonthlyPL.Name = "txbMonthlyPL";
            this.txbMonthlyPL.Size = new System.Drawing.Size(135, 35);
            this.txbMonthlyPL.TabIndex = 6;
            this.txbMonthlyPL.Text = "$999.999.99";
            this.txbMonthlyPL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 30);
            this.label3.TabIndex = 5;
            this.label3.Text = "Shares";
            // 
            // txbShares
            // 
            this.txbShares.Location = new System.Drawing.Point(139, 78);
            this.txbShares.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbShares.Name = "txbShares";
            this.txbShares.Size = new System.Drawing.Size(89, 35);
            this.txbShares.TabIndex = 4;
            this.txbShares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9.857143F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(189, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ticker";
            // 
            // txbTicker
            // 
            this.txbTicker.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txbTicker.Location = new System.Drawing.Point(271, 35);
            this.txbTicker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbTicker.Name = "txbTicker";
            this.txbTicker.Size = new System.Drawing.Size(89, 35);
            this.txbTicker.TabIndex = 2;
            this.txbTicker.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbxAverageStockPrice
            // 
            this.tbxAverageStockPrice.BackColor = System.Drawing.SystemColors.Window;
            this.tbxAverageStockPrice.Location = new System.Drawing.Point(139, 120);
            this.tbxAverageStockPrice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.tbxAverageStockPrice.Name = "tbxAverageStockPrice";
            this.tbxAverageStockPrice.ReadOnly = true;
            this.tbxAverageStockPrice.Size = new System.Drawing.Size(89, 35);
            this.tbxAverageStockPrice.TabIndex = 1;
            this.tbxAverageStockPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 121);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Avg Price";
            // 
            // optionPlayControl
            // 
            this.optionPlayControl.AutoScroll = true;
            this.optionPlayControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.optionPlayControl.Location = new System.Drawing.Point(0, 168);
            this.optionPlayControl.Margin = new System.Windows.Forms.Padding(0);
            this.optionPlayControl.Name = "optionPlayControl";
            this.optionPlayControl.Size = new System.Drawing.Size(1429, 762);
            this.optionPlayControl.TabIndex = 16;
            // 
            // StockMonthlyViewPanel
            // 
            this.StockMonthlyViewPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.StockMonthlyViewPanel.Controls.Add(this.optionPlayControl);
            this.StockMonthlyViewPanel.Controls.Add(this.StockMonthlyStatisticGroupBox);
            this.StockMonthlyViewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockMonthlyViewPanel.Location = new System.Drawing.Point(0, 0);
            this.StockMonthlyViewPanel.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.StockMonthlyViewPanel.Name = "StockMonthlyViewPanel";
            this.StockMonthlyViewPanel.Size = new System.Drawing.Size(1433, 962);
            this.StockMonthlyViewPanel.TabIndex = 17;
            // 
            // StockMonthlyView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.StockMonthlyViewPanel);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "StockMonthlyView";
            this.Size = new System.Drawing.Size(1433, 962);
            this.StockMonthlyStatisticGroupBox.ResumeLayout(false);
            this.StockMonthlyStatisticGroupBox.PerformLayout();
            this.StockMonthlyViewPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox StockMonthlyStatisticGroupBox;
        private System.Windows.Forms.TextBox tbxAverageStockPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbTicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbShares;
        private System.Windows.Forms.TextBox txbMonthlyPL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbMonthlyPLRatio;
        private OptionPlayControl.OptionPlayView optionPlayControl;
        private System.Windows.Forms.Panel StockMonthlyViewPanel;
        private System.Windows.Forms.TextBox txbSimPandLStkAssign;
        private System.Windows.Forms.TextBox txbSimPandLCC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbStkTotalPandL;
        private System.Windows.Forms.TextBox txbStkYTDPandL;
        private System.Windows.Forms.Label label10;
    }
}
