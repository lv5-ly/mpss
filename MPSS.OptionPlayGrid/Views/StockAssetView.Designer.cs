﻿
namespace MPSS.Controls.Views
{
    partial class StockAssetView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.StockTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.BoughtStockButton = new System.Windows.Forms.Button();
            this.OptionPlayButton = new System.Windows.Forms.Button();
            this.RemoveStockButton = new System.Windows.Forms.Button();
            this.StockAssetDataGrid = new System.Windows.Forms.DataGridView();
            this.StockTableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StockAssetDataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // StockTableLayoutPanel
            // 
            this.StockTableLayoutPanel.AutoSize = true;
            this.StockTableLayoutPanel.ColumnCount = 1;
            this.StockTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.StockTableLayoutPanel.Controls.Add(this.BoughtStockButton, 0, 2);
            this.StockTableLayoutPanel.Controls.Add(this.OptionPlayButton, 0, 1);
            this.StockTableLayoutPanel.Controls.Add(this.RemoveStockButton, 0, 1);
            this.StockTableLayoutPanel.Controls.Add(this.StockAssetDataGrid, 0, 0);
            this.StockTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.StockTableLayoutPanel.Name = "StockTableLayoutPanel";
            this.StockTableLayoutPanel.RowCount = 3;
            this.StockTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.StockTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.StockTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.StockTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.StockTableLayoutPanel.Size = new System.Drawing.Size(199, 323);
            this.StockTableLayoutPanel.TabIndex = 0;
            // 
            // BoughtStockButton
            // 
            this.BoughtStockButton.BackColor = System.Drawing.SystemColors.Info;
            this.BoughtStockButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BoughtStockButton.Location = new System.Drawing.Point(1, 294);
            this.BoughtStockButton.Margin = new System.Windows.Forms.Padding(1);
            this.BoughtStockButton.Name = "BoughtStockButton";
            this.BoughtStockButton.Size = new System.Drawing.Size(197, 28);
            this.BoughtStockButton.TabIndex = 11;
            this.BoughtStockButton.Text = "Bought Stock";
            this.BoughtStockButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.BoughtStockButton.UseVisualStyleBackColor = false;
            this.BoughtStockButton.Click += new System.EventHandler(this.BoughtStockButton_Click);
            // 
            // OptionPlayButton
            // 
            this.OptionPlayButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptionPlayButton.Location = new System.Drawing.Point(1, 263);
            this.OptionPlayButton.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.OptionPlayButton.Name = "OptionPlayButton";
            this.OptionPlayButton.Size = new System.Drawing.Size(197, 30);
            this.OptionPlayButton.TabIndex = 10;
            this.OptionPlayButton.Text = "Option Play";
            this.OptionPlayButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.OptionPlayButton.UseVisualStyleBackColor = true;
            this.OptionPlayButton.Click += new System.EventHandler(this.OptionPlayButton_Click);
            // 
            // RemoveStockButton
            // 
            this.RemoveStockButton.BackColor = System.Drawing.SystemColors.Info;
            this.RemoveStockButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemoveStockButton.Location = new System.Drawing.Point(1, 234);
            this.RemoveStockButton.Margin = new System.Windows.Forms.Padding(1);
            this.RemoveStockButton.Name = "RemoveStockButton";
            this.RemoveStockButton.Size = new System.Drawing.Size(197, 28);
            this.RemoveStockButton.TabIndex = 6;
            this.RemoveStockButton.Text = "Remove Stock";
            this.RemoveStockButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.RemoveStockButton.UseVisualStyleBackColor = false;
            this.RemoveStockButton.Click += new System.EventHandler(this.RemoveStockButton_Click);
            // 
            // StockAssetDataGrid
            // 
            this.StockAssetDataGrid.AllowUserToAddRows = false;
            this.StockAssetDataGrid.AllowUserToDeleteRows = false;
            this.StockAssetDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StockAssetDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.StockAssetDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.StockAssetDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.StockAssetDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockAssetDataGrid.Location = new System.Drawing.Point(1, 1);
            this.StockAssetDataGrid.Margin = new System.Windows.Forms.Padding(1);
            this.StockAssetDataGrid.MultiSelect = false;
            this.StockAssetDataGrid.Name = "StockAssetDataGrid";
            this.StockAssetDataGrid.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.StockAssetDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.StockAssetDataGrid.RowHeadersVisible = false;
            this.StockAssetDataGrid.RowHeadersWidth = 72;
            this.StockAssetDataGrid.RowTemplate.Height = 25;
            this.StockAssetDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.StockAssetDataGrid.Size = new System.Drawing.Size(197, 231);
            this.StockAssetDataGrid.TabIndex = 1;
            this.StockAssetDataGrid.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.StockAssetDataGrid_CellToolTipTextNeeded);
            this.StockAssetDataGrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.StockAssetDataGrid_ColumnHeaderMouseClick);
            this.StockAssetDataGrid.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.StockAssetDataGrid_RowStateChanged);
            // 
            // StockAssetView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.StockTableLayoutPanel);
            this.Name = "StockAssetView";
            this.Size = new System.Drawing.Size(199, 323);
            this.StockTableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StockAssetDataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel StockTableLayoutPanel;
        private System.Windows.Forms.DataGridView StockAssetDataGrid;
        private System.Windows.Forms.Button RemoveStockButton;
        private System.Windows.Forms.Button OptionPlayButton;
        private System.Windows.Forms.Button BoughtStockButton;
    }
}
