﻿using LiveCharts;
using LiveCharts.Wpf;
using MPSS.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    public partial class SimulatedChart : UserControl
    {
        private StockAssetViewModel ViewModel;

        public SimulatedChart()
        {
            InitializeComponent();

            // Let's initialize the charts
            CartesianChart.Series = new SeriesCollection();
        }

        public void SetViewModel(StockAssetViewModel applicationViewModel)
        {
            ViewModel = applicationViewModel;
        }

        public override void Refresh()
        {
            base.Refresh();

            if (ViewModel != null)
            {
                /// Loop thru all the stock assets and add up the unrealized CC premium and Realized P&L
                /// Get the current Month and Year in integer form
                int currentMonth = DateTime.Now.Month;
                int currentYear = DateTime.Now.Year;
                decimal expiredCCProfit = 0;

                CartesianChart.Series.Clear();

                foreach (var s in ViewModel.StockAssetBindingList)
                {
                    expiredCCProfit += s.GetExpiredPandL() + s.GetMonthlyPandL(currentMonth, currentYear);
                }

                CartesianChart.Series.Add(
                    new RowSeries
                    {
                        Title = "Expired CC",
                        Values = new ChartValues<decimal>() { expiredCCProfit }
                    });

                /// Loop thru all the stocks and add up stock assignment P&Ls
                decimal assignedStkPandL = 0;
                foreach (var s in ViewModel.StockAssetBindingList)
                {
                    assignedStkPandL += s.GetStockAssignedPandL();
                }
                CartesianChart.Series.Add(
                    new RowSeries
                    {
                        Title = "Assigned Strike",
                        Values = new ChartValues<decimal>() { assignedStkPandL + expiredCCProfit }
                    });

                CartesianChart.AxisX.Clear();
                CartesianChart.AxisX.Add(new Axis
                {
                    Title = "Total",
                    LabelFormatter = value => value.ToString("C")

                });

                CartesianChart.AxisY.Clear();
                CartesianChart.AxisY.Add(new Axis
                {
                    Title = "Simulated",
                    Labels = new List<string> { "P&L" }
                });

            }
        }
    }
}
