﻿using MPSS.Common.DataProvider;
using MPSS.Common.Model;
using MPSS.Common.Model.Tradier;
using MPSS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    /// <summary>
    /// A custom control, to provide the background task activities and control
    /// the background task
    /// </summary>
    public partial class WatchdogView : UserControl
    {
        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked Option Call Changed")]
        public event EventHandler<ScanResultEventArgs> ScanResultChanged;
        #endregion Public Properties

        #region Private Properties        
        private StockAssetViewModel ViewModel;
        private readonly TradierStockOptionDataService OptionDataService = new(new OptionChainCache());
        private int NumberOfPossiblePlays = 0;
        private readonly int MAX_SLEEP_TIME = 300;      // Maximum sleep time in sec.
        private readonly int MAX_EXPIRATION_DATES = 3;  // Maximum expiration dates in scanning ahead
        private bool ClearLogSet { get; set; }
        #endregion Private Properties

        #region Constructor
        public WatchdogView()
        {
            InitializeComponent();

            // Initial condition
            OptionDataWorker.CancelAsync();
            WorkerStatusLabel.Text = "Watchdog is ready, awaiting for your command...";
            WorkerProgressBar.Value = 0;

            WatchdogTakeABreak.Enabled = false;
            WatchdogGoToWork.Enabled = true;
        }
        #endregion Constructor

        #region Event Handler
        /// <summary>
        /// This is the entry for the worker to ... work!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptionDataWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            BackgroundWorker worker = sender as BackgroundWorker;

            // Quick check and return if Cancellation is pending
            if (worker.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            worker.ReportProgress(-1);
            WatchdogTakeABreak.Enabled = true;
            WatchdogGoToWork.Enabled = false;

            var expirationDates = GetExpirationDates(MAX_EXPIRATION_DATES);
            var transactions = ViewModel.GetAllActiveCoveredCallTransactions();
            var stocks = ViewModel.GetAllActiveNonCoveredCallTransactions();

            // This is total of work items, basically each time we call Tradier
            // to retrieve data, it's considered a work-item.
            //  - For each active CC, we need to query for option-chain, to find possible buy-back
            //  - For each active stock, we need to query for all the options, to find possible CC
            //  - For each active stock, we also need to find the stock quote, to help determine
            //      whether the CC is worth to be considered
            var total = transactions.Count() + (stocks.Count() * expirationDates.Count()) + stocks.Count();
            var item = 1;

            worker.ReportProgress(0,
                new WorkerProgressReport($"Scanning for potential buy-back (25% of SOLD-CALL price) active CC transactions..."));

            NumberOfPossiblePlays = 0;      // Reset number of possible plays
            foreach (var t in transactions)
            {
                t.CallOptions.Clear();

                // Get the the current call option, for this strike price and
                //  expiration date.
                Option o = GetCallOption(t.AssetSymbol, t.StrikePrice, t.ExpiredDate);

                // Add this call option to the transaction's internal
                //  call-option list. Since the strike price is the same
                //  we can use expiration date as key, and the
                //  call-option as value
                t.CallOptions[t.ExpiredDate.Date] = o;
                NumberOfPossiblePlays += t.IsPlayPossible ? 1 : 0;

                // Report progress, make sure to create new instance of 
                //  CallOption
                worker.ReportProgress((int)(item++ * 100 / total), new WorkerProgressReport(t));

                // This allows the UI thread to run, and show the result
                System.Threading.Thread.Sleep(3000);
            }

            worker.ReportProgress(0,
                new WorkerProgressReport($"Scanning for potential CC (premium is 5% of adj cost or current market)..."));
            foreach (var s in stocks)
            {
                var quote = GetQuote(s.Symbol);
                s.LatestQuote = quote ?? s.LatestQuote;
                worker.ReportProgress((int)(item++ * 100 / total),
                    new WorkerProgressReport(s, quote));
                System.Threading.Thread.Sleep(1500);
                foreach (var d in expirationDates)
                {
                    s.Options.Clear();

                    // Get the current option chain, which are all expires
                    //  on the specified date
                    var options = GetOptionChain(s.Symbol, d);

                    // Add this option chain to the stock
                    s.Options[d.Date] = options;

                    NumberOfPossiblePlays += s.IsPlayPossible ? 1 : 0;


                    // Report the progress
                    worker.ReportProgress((int)(item++ * 100 / total),
                        new WorkerProgressReport(s, d));

                    // This allows the UI thread to run, and show the result
                    System.Threading.Thread.Sleep(1500);
                }
            }

            worker.ReportProgress(-2);
            var i = MAX_SLEEP_TIME;
            while (!worker.CancellationPending && i-- > 0)
            {
                System.Threading.Thread.Sleep(1000); // Sleep for 1 sec
                worker.ReportProgress((int)(i * 100 / MAX_SLEEP_TIME));
            }
            e.Cancel = worker.CancellationPending;
        }

        private static IEnumerable<DateTime> GetExpirationDates(int expirationDates)
        {
            List<DateTime> dates = new();
            for (var n = 0; n < expirationDates; n++)
            {
                var start = DateTime.Today.Date.AddDays(n * 7);
                dates.Add(GetNextWeekday(start, DayOfWeek.Friday));
            }
            return dates;
        }

        private void OptionDataWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if (e.ProgressPercentage == -1)
            {
                if (ClearLogSet)
                {
                    WorkersLog.Clear();
                    ClearLogSet = false;
                    ClearLog.Enabled = true;
                }

                WorkersLog.AppendText($"{DateTime.Now.ToShortTimeString()} Start scanning");
                WorkersLog.AppendText(Environment.NewLine);


                WorkerStatusLabel.Text = "Watchdog is awake!";
                WorkerProgressBar.Value = 0;
                // LastScanningDateTime.Text = $"Last scan at {DateTime.Now.ToShortTimeString()}";
            }
            else if (e.ProgressPercentage == -2)
            {
                // Raise the ProgressChangedChanged
                RaiseEvent(ScanResultChanged, sender, new ScanResultEventArgs(NumberOfPossiblePlays));

                WorkerStatusLabel.Text = $"Watchdog sleeps for {MAX_SLEEP_TIME / 60}min.";
                WorkersLog.AppendText(Environment.NewLine);
            }
            else if (e.UserState is not WorkerProgressReport progress)
            {
                WorkerProgressBar.Value = e.ProgressPercentage;
            }
            else
            {
                WorkersLog.AppendText(progress.Message);
                WorkersLog.AppendText(Environment.NewLine);

                if (worker.CancellationPending)
                {
                    WorkerStatusLabel.Text = "Calling the dog...";
                }
                else
                {
                    WorkerStatusLabel.Text = "Watchdog is scanning...";
                }

                WorkerProgressBar.Value = e.ProgressPercentage;



            }
        }

        private void OptionDataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                WorkerStatusLabel.Text = "Watchdog is taking a break";
                WorkerProgressBar.Value = 0;
            }
            else
            {
                OptionDataWorker.RunWorkerAsync();
            }
        }

        private void WatchdogTakeABreak_Click(object sender, EventArgs e)
        {
            OptionDataWorker.CancelAsync();
            WatchdogTakeABreak.Enabled = false;
            WatchdogGoToWork.Enabled = true;
        }

        private void WatchdogGoToWork_Click(object sender, EventArgs e)
        {
            OptionDataWorker.RunWorkerAsync();
            WatchdogGoToWork.Enabled = false;
            WatchdogTakeABreak.Enabled = true;
        }

        private void ClearLog_Click(object sender, EventArgs e)
        {
            ClearLogSet = true;
            ClearLog.Enabled = false;
        }
        #endregion Event Handler

        #region Private Methods
        private IEnumerable<Option> GetOptionChain(string symbol, DateTime expiredDate)
        {
            var chain = OptionDataService.GetOptionChain(symbol, expiredDate);
            return chain?.Options?.Option;
        }
        private StockQuote GetQuote(string symbol)
        {
            var qr = OptionDataService.GetQuote(symbol);

            return qr?.Quotes?.Quote;
        }


        /// <summary>
        /// Retrieve a list of call-options from Tradier and return
        /// the call option with matching strike price
        /// </summary>
        /// <param name="strikePrice">Strike price</param>
        /// <param name="expiredDate">Expiration date</param>
        /// <returns>Highest Ask premium call option</returns>
        private Option GetCallOption(string symbol, decimal strikePrice, DateTime expiredDate)
        {
            // TODO
            //  Call Tradier service to retrieve option chain, and return
            //  the call-option with matching strike price
            var options = GetOptionChain(symbol, expiredDate);
            return options?.FirstOrDefault(o => o.Strike == strikePrice && o.OptionType == "call");
        }

        private static void RaiseEvent(
            EventHandler<ScanResultEventArgs> eventHandler,
            object sender, ScanResultEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }

        /// <summary>
        /// An utility to provide the DateTime object of the next weekday
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="day">The weekday</param>
        /// <returns>The DateTime object for the coming weekday</returns>
        private static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int)day - (int)start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }

        public void SetViewModel(StockAssetViewModel vm)
        {
            ViewModel = vm;
        }
        #endregion Private Methods


    }
}
