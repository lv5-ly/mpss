﻿
namespace MPSS.Controls.Views
{
    partial class StockTransactionView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.EditTransactionButton = new System.Windows.Forms.Button();
            this.DeleteTransactionButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.TransactionDataGrid = new System.Windows.Forms.DataGridView();
            this.HideInactiveTransactionCheckBox = new System.Windows.Forms.CheckBox();
            this.TransactionLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionDataGrid)).BeginInit();
            this.TransactionLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // EditTransactionButton
            // 
            this.EditTransactionButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.EditTransactionButton.Enabled = false;
            this.EditTransactionButton.Location = new System.Drawing.Point(391, 270);
            this.EditTransactionButton.Margin = new System.Windows.Forms.Padding(1);
            this.EditTransactionButton.Name = "EditTransactionButton";
            this.EditTransactionButton.Size = new System.Drawing.Size(63, 28);
            this.EditTransactionButton.TabIndex = 9;
            this.EditTransactionButton.Text = "Edit Transaction";
            this.EditTransactionButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.EditTransactionButton.UseVisualStyleBackColor = true;
            this.EditTransactionButton.Click += new System.EventHandler(this.EditTransactionButton_Click);
            // 
            // DeleteTransactionButton
            // 
            this.DeleteTransactionButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DeleteTransactionButton.AutoSize = true;
            this.DeleteTransactionButton.Enabled = false;
            this.DeleteTransactionButton.Location = new System.Drawing.Point(196, 270);
            this.DeleteTransactionButton.Margin = new System.Windows.Forms.Padding(1);
            this.DeleteTransactionButton.Name = "DeleteTransactionButton";
            this.DeleteTransactionButton.Size = new System.Drawing.Size(114, 28);
            this.DeleteTransactionButton.TabIndex = 10;
            this.DeleteTransactionButton.Text = "Delete Transaction";
            this.DeleteTransactionButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.DeleteTransactionButton.UseVisualStyleBackColor = true;
            this.DeleteTransactionButton.Click += new System.EventHandler(this.DeleteTransactionButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CloseButton.AutoSize = true;
            this.CloseButton.Enabled = false;
            this.CloseButton.Location = new System.Drawing.Point(34, 269);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.CloseButton.MaximumSize = new System.Drawing.Size(100, 30);
            this.CloseButton.MinimumSize = new System.Drawing.Size(100, 30);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(100, 30);
            this.CloseButton.TabIndex = 8;
            this.CloseButton.Text = "Close";
            this.CloseButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // TransactionDataGrid
            // 
            this.TransactionDataGrid.AllowUserToAddRows = false;
            this.TransactionDataGrid.AllowUserToDeleteRows = false;
            this.TransactionDataGrid.AllowUserToOrderColumns = true;
            this.TransactionDataGrid.AllowUserToResizeRows = false;
            this.TransactionDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TransactionDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TransactionDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.TransactionDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TransactionDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TransactionLayoutPanel.SetColumnSpan(this.TransactionDataGrid, 3);
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionDataGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.TransactionDataGrid.Location = new System.Drawing.Point(1, 31);
            this.TransactionDataGrid.Margin = new System.Windows.Forms.Padding(1);
            this.TransactionDataGrid.MultiSelect = false;
            this.TransactionDataGrid.Name = "TransactionDataGrid";
            this.TransactionDataGrid.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionDataGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.TransactionDataGrid.RowHeadersVisible = false;
            this.TransactionDataGrid.RowHeadersWidth = 72;
            this.TransactionDataGrid.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionDataGrid.RowTemplate.Height = 25;
            this.TransactionDataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.TransactionDataGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TransactionDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TransactionDataGrid.Size = new System.Drawing.Size(505, 237);
            this.TransactionDataGrid.TabIndex = 0;
            this.TransactionDataGrid.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.TransactionDataGrid_ColumnHeaderMouseClick);
            this.TransactionDataGrid.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.TransactionDataGridView_RowStateChanged);
            // 
            // HideInactiveTransactionCheckBox
            // 
            this.HideInactiveTransactionCheckBox.AutoSize = true;
            this.HideInactiveTransactionCheckBox.Checked = true;
            this.HideInactiveTransactionCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TransactionLayoutPanel.SetColumnSpan(this.HideInactiveTransactionCheckBox, 3);
            this.HideInactiveTransactionCheckBox.Location = new System.Drawing.Point(3, 3);
            this.HideInactiveTransactionCheckBox.Name = "HideInactiveTransactionCheckBox";
            this.HideInactiveTransactionCheckBox.Size = new System.Drawing.Size(158, 19);
            this.HideInactiveTransactionCheckBox.TabIndex = 1;
            this.HideInactiveTransactionCheckBox.Text = "Hide Inactive Transaction";
            this.HideInactiveTransactionCheckBox.UseVisualStyleBackColor = true;
            this.HideInactiveTransactionCheckBox.CheckedChanged += new System.EventHandler(this.HideInactiveTransactionCheckBox_CheckedChanged);
            // 
            // TransactionLayoutPanel
            // 
            this.TransactionLayoutPanel.ColumnCount = 3;
            this.TransactionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TransactionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TransactionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.TransactionLayoutPanel.Controls.Add(this.EditTransactionButton, 2, 2);
            this.TransactionLayoutPanel.Controls.Add(this.DeleteTransactionButton, 1, 2);
            this.TransactionLayoutPanel.Controls.Add(this.HideInactiveTransactionCheckBox, 0, 0);
            this.TransactionLayoutPanel.Controls.Add(this.TransactionDataGrid, 0, 1);
            this.TransactionLayoutPanel.Controls.Add(this.CloseButton, 0, 2);
            this.TransactionLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.TransactionLayoutPanel.Name = "TransactionLayoutPanel";
            this.TransactionLayoutPanel.RowCount = 3;
            this.TransactionLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.TransactionLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TransactionLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.TransactionLayoutPanel.Size = new System.Drawing.Size(507, 299);
            this.TransactionLayoutPanel.TabIndex = 11;
            // 
            // StockTransactionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.TransactionLayoutPanel);
            this.Name = "StockTransactionView";
            this.Size = new System.Drawing.Size(507, 299);
            this.Load += new System.EventHandler(this.StockTransactionView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TransactionDataGrid)).EndInit();
            this.TransactionLayoutPanel.ResumeLayout(false);
            this.TransactionLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView TransactionDataGrid;
        private System.Windows.Forms.CheckBox HideInactiveTransactionCheckBox;
        private System.Windows.Forms.Button DeleteTransactionButton;
        private System.Windows.Forms.Button EditTransactionButton;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.TableLayoutPanel TransactionLayoutPanel;
    }
}
