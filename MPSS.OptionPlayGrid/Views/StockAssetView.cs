﻿using MPSS.Common.Model;
using MPSS.OptionPlayControl;
using MPSS.ViewModel;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    /// <summary>
    /// This is an user-control, which contains
    /// - Stock Asset Data Grid
    /// - Bought Stock, Option Play, Remove Stock buttons
    /// </summary>
    public partial class StockAssetView : UserControl
    {
        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked stock asset list is changed")]
        public event EventHandler<StockAssetEventArgs> StockAssetListChanged;
        #endregion Public Properties

        #region Private Properties
        private StockAssetViewModel viewModel;
        private SortOrder currentSortOrder;
        private readonly BindingSource StockAssetBindingSource = new();
        private readonly string REMOVE_STOCK_CONFIRMATION_MESSAGE = "Are you sure you want to remove this stock?";
        private readonly string REMOVE_STOCK_CAPTION = "Remove Stock";
        #endregion Private Properties

        #region Constructor
        public StockAssetView()
        {
            InitializeComponent();
        }
        #endregion Constructor

        #region Public Methods
        public void SetViewModel(StockAssetViewModel vm)
        {
            viewModel = vm;

            if (viewModel != null)
            {
                viewModel.Load();
                if (StockAssetBindingSource.DataSource == null)
                {
                    StockAssetBindingSource.DataSource = viewModel.GetStockAssetBindingList();
                    StockAssetDataGrid.DataSource = StockAssetBindingSource;

                    FormatStockAssetGrid();
                }

            }
        }

        #endregion Public Methods

        #region Event Handlers
        private void StockAssetDataGrid_RowStateChanged(
            object sender,
            DataGridViewRowStateChangedEventArgs e)
        {
            if (StockAssetDataGrid.SelectedRows.Count == 1)
            {
                StockAsset sa = StockAssetDataGrid.SelectedRows[0].DataBoundItem as StockAsset;
                viewModel.SelectStockAsset(sa);
                RaiseEvent(StockAssetListChanged,
                    sa,
                    new StockAssetEventArgs(sa));

                SetButtonState(sa);
            }
        }


        private void RemoveStockButton_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show(
                REMOVE_STOCK_CONFIRMATION_MESSAGE,
                REMOVE_STOCK_CAPTION,
                MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                var sa = viewModel.GetCurrentlySelectedStock();
                if (sa != null)
                {
                    viewModel.RemoveStock();
                    viewModel.Load();
                    FormatStockAssetGrid();

                    RaiseEvent(StockAssetListChanged,
                        sa,
                        new StockAssetEventArgs(sa));
                }
            }
        }

        private void BoughtStockButton_Click(object sender, EventArgs e)
        {
            var dialog = new BuyShareDialog(
                viewModel,
                viewModel.GetCurrentlySelectedStock(),
                AssetTransactionAction.Bought,
                AssetTransactionType.Share);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // TODO
                //  Move the code inside the BuyShareDialog to here
                //  because we want to keep the CRUD in this
                //  class, and not in the dialog control

                // Tell the view model to reload the data, since
                //  we just add a new stock transaction
                ReloadData();

                RaiseEvent(StockAssetListChanged,
                    viewModel.GetCurrentlySelectedStock(),
                    new StockAssetEventArgs(viewModel.GetCurrentlySelectedStock()));
            }
        }

        private void OptionPlayButton_Click(object sender, EventArgs e)
        {
            var sa = viewModel.GetCurrentlySelectedStock();
            var dialog = new OptionPlayDialog(sa, viewModel.CurrentSelectedTransaction);
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                viewModel.AddTransaction(dialog.transaction);
                ReloadData();

                RaiseEvent(StockAssetListChanged,
                    sa,
                    new StockAssetEventArgs(sa));
            }
            dialog.Close();
        }

        /// <summary>
        /// Whenever the data grid wants to display the tool-tip, it raise this event; 
        /// thus we want to handle this event by inspecting the row/column on which
        /// the mouse is hovering, and construct the correct tool-tip to display
        /// </summary>
        /// <param name="sender">The object raising this event</param>
        /// <param name="e">The parameter for this event</param>
        private void StockAssetDataGrid_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
        {
            if (e.ColumnIndex == 1
                && e.RowIndex >= 0
                && viewModel != null)
            {
                DataGridViewRow r = StockAssetDataGrid.Rows[e.RowIndex];
                StockAsset s = viewModel.GetStockBySymbol(r.Cells[1].Value.ToString());
                if (s != null)
                {
                    e.ToolTipText = GetToolTipByStock(s);
                }
            }
        }

        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// The Tool Tip consists of
        /// - YTD's P&L, this year only
        /// - All time P&L, as in spanning multiple years
        /// </summary>
        /// <param name="stock">The stock object</param>
        /// <returns>Tool Tip string</returns>
        private static string GetToolTipByStock(StockAsset stock)
        {
            var ytdpl = stock.GetYTDPandL();
            var atpl = stock.GetTotalPandL();
            return $"YTD P&L: {ytdpl:C}, All time P&L: {atpl:C}";
        }

        private void FormatStockAssetGrid()
        {
            this.StockAssetDataGrid.Columns["StockAssetId"].Visible = false;

            this.StockAssetDataGrid.Columns["Symbol"].HeaderText = "Symbol";
            this.StockAssetDataGrid.Columns["Symbol"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.StockAssetDataGrid.Columns["Symbol"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            this.StockAssetDataGrid.Columns["Shares"].HeaderText = "Shares";
            this.StockAssetDataGrid.Columns["Shares"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.StockAssetDataGrid.Columns["Shares"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.StockAssetDataGrid.Columns["Shares"].DefaultCellStyle.Format = "N0";
            this.StockAssetDataGrid.Columns["Shares"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            this.StockAssetDataGrid.Columns["Shares"].Width = 80;


            this.StockAssetDataGrid.Columns["AdjustedCost"].HeaderText = "Orig/Adj Cost";
            this.StockAssetDataGrid.Columns["AdjustedCost"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.StockAssetDataGrid.Columns["AdjustedCost"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.StockAssetDataGrid.Columns["AdjustedCost"].DefaultCellStyle.Format = "C";

            StockAssetDataGrid.AutoResizeColumns();
        }

        private static void RaiseEvent(EventHandler<StockAssetEventArgs> eventHandler, object sender, StockAssetEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }
        private void SetButtonState(StockAsset sa)
        {
            BoughtStockButton.Enabled = true;
            RemoveStockButton.Enabled = OptionPlayButton.Enabled = sa != null;
        }
        private void StockAssetDataGrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // We support only sorting on column 0, which is the TransactionDate; 
            //  and also when we have more than one transactions in the grid
            if (e.ColumnIndex == 1 &&
                StockAssetDataGrid.Rows.Count > 1)
            {
                ReloadData();
                StockAssetDataGrid.Columns[1].HeaderCell.SortGlyphDirection = currentSortOrder;
            }
        }

        private void ReloadData()
        {
            // Set the next sort-order
            currentSortOrder = currentSortOrder == SortOrder.None ?
                SortOrder.Ascending :
                currentSortOrder == SortOrder.Ascending ?
                SortOrder.Descending : SortOrder.None;

            int so = currentSortOrder == SortOrder.None ?
                0 : currentSortOrder == SortOrder.Ascending ?
                1 : -1;

            viewModel.Load(so);

            Refresh();
        }
        #endregion Private Methods
    }
}
