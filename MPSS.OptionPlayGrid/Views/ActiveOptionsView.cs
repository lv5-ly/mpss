﻿using MPSS.Common.Model;
using MPSS.Controls.Dialogs;
using MPSS.OptionPlayControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    public partial class ActiveOptionsView : UserControl
    {
        [Browsable(true)]
        [Category("Action")]
        [Description("Transaction closed")]
        public event EventHandler<StockTransactionEventArgs> TransactionClosed;

        private readonly SortableBindingList<ActiveOptionTransaction> ActiveOptionList = new();
        public ActiveOptionsView()
        {
            InitializeComponent();

            MCMRGrid.Title = "Active Options";
            MCMRGrid.SetBindingData(ActiveOptionList);
        }

        public void Refresh(IEnumerable<AssetTransaction> transactions)
        {
            ActiveOptionList.Clear();
            foreach (var t in transactions)
            {
                ActiveOptionList.Add(new ActiveOptionTransaction(t));
            }
            MCMRGrid.Refresh();
        }

        private void CloseTransaction(AssetTransaction parentTransaction)
        {
            CloseTransactionDialog dialog = parentTransaction.IsShareTransaction() ?
                new CloseShareDialog(parentTransaction) :
                new CloseOptionPlayDialog(parentTransaction);

            AssetTransaction childTransaction = dialog.ShowDialog() == DialogResult.OK ?
                dialog.Transaction : null;
            dialog.Close();

            if (childTransaction != null)
            {
                var eventArgs = new StockTransactionEventArgs(parentTransaction, childTransaction);
                RaiseEvent(TransactionClosed, this, eventArgs);

                // Tell the viewModel to close out the currently selected
                //  transaction, with this child transaction
                //viewModel.CloseTransaction(parentTransaction, childTransaction);

                //// viewModel.AddTransaction(dialog.transaction);

                //// RefreshAllPanels(viewModel);
                //LoadDataIntoTransactionBindingList();
                //Refresh();

                //// Raise TransactionListChanged event
                //RaiseEvent(TransactionListChanged, sender,
                //    new StockTransactionEventArgs(childTransaction));
            }
        }

        private static void RaiseEvent(EventHandler<StockTransactionEventArgs> eventHandler, object sender, StockTransactionEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }

        private void MCMRGrid_RowDoubleClick(object sender, StockTransactionEventArgs e)
        {
            var eventArgs = e as StockTransactionEventArgs;
            CloseTransaction(eventArgs.CurrentAssetTransaction);
        }

        private static void Scenario(AssetTransaction currentAssetTransaction)
        {
            var dialog = new ScenarioDialog(currentAssetTransaction);
            if (dialog.ShowDialog() == DialogResult.OK)
            {

            }
            dialog.Close();
        }

        private void MCMRGrid_ContextMenuItemClicked(object sender, StockTransactionEventArgs e)
        {
            if (e.SelectedContextMenuItemName == "ScenarioMenuItem")
            {
                Scenario(e.CurrentAssetTransaction);
            }
            else
            {
                CloseTransaction(e.CurrentAssetTransaction);
            }
        }
    }
}
