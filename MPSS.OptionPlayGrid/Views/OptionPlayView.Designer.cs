﻿
namespace MPSS.OptionPlayControl
{
    partial class OptionPlayView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBoxOptionPlays = new System.Windows.Forms.GroupBox();
            this.dataGridViewOptionPlays = new System.Windows.Forms.DataGridView();
            this.groupBoxOptionPlays.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOptionPlays)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxOptionPlays
            // 
            this.groupBoxOptionPlays.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxOptionPlays.Controls.Add(this.dataGridViewOptionPlays);
            this.groupBoxOptionPlays.Location = new System.Drawing.Point(0, 0);
            this.groupBoxOptionPlays.Name = "groupBoxOptionPlays";
            this.groupBoxOptionPlays.Size = new System.Drawing.Size(468, 194);
            this.groupBoxOptionPlays.TabIndex = 0;
            this.groupBoxOptionPlays.TabStop = false;
            this.groupBoxOptionPlays.Text = "Option Plays";
            // 
            // dataGridViewOptionPlays
            // 
            this.dataGridViewOptionPlays.AllowUserToAddRows = false;
            this.dataGridViewOptionPlays.AllowUserToDeleteRows = false;
            this.dataGridViewOptionPlays.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewOptionPlays.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOptionPlays.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewOptionPlays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOptionPlays.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewOptionPlays.Location = new System.Drawing.Point(3, 19);
            this.dataGridViewOptionPlays.Margin = new System.Windows.Forms.Padding(0);
            this.dataGridViewOptionPlays.MultiSelect = false;
            this.dataGridViewOptionPlays.Name = "dataGridViewOptionPlays";
            this.dataGridViewOptionPlays.ReadOnly = true;
            this.dataGridViewOptionPlays.RowHeadersVisible = false;
            this.dataGridViewOptionPlays.RowTemplate.Height = 25;
            this.dataGridViewOptionPlays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewOptionPlays.Size = new System.Drawing.Size(462, 172);
            this.dataGridViewOptionPlays.TabIndex = 0;
            // 
            // OptionPlayView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxOptionPlays);
            this.Name = "OptionPlayView";
            this.Size = new System.Drawing.Size(468, 194);
            this.groupBoxOptionPlays.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOptionPlays)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOptionPlays;
        private System.Windows.Forms.DataGridView dataGridViewOptionPlays;
    }
}
