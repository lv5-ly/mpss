﻿
namespace MPSS.Controls.Views
{
    partial class StockTransactionChart
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TransactionDailyChart = new LiveCharts.WinForms.CartesianChart();
            this.StockTransactionChartTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.StockTransactionChartTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TransactionDailyChart
            // 
            this.TransactionDailyChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TransactionDailyChart.Location = new System.Drawing.Point(3, 3);
            this.TransactionDailyChart.Name = "TransactionDailyChart";
            this.TransactionDailyChart.Size = new System.Drawing.Size(731, 466);
            this.TransactionDailyChart.TabIndex = 0;
            this.TransactionDailyChart.Text = "cartesianChart1";
            // 
            // StockTransactionChartTableLayoutPanel
            // 
            this.StockTransactionChartTableLayoutPanel.ColumnCount = 1;
            this.StockTransactionChartTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.StockTransactionChartTableLayoutPanel.Controls.Add(this.TransactionDailyChart, 0, 0);
            this.StockTransactionChartTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StockTransactionChartTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.StockTransactionChartTableLayoutPanel.Name = "StockTransactionChartTableLayoutPanel";
            this.StockTransactionChartTableLayoutPanel.RowCount = 2;
            this.StockTransactionChartTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.StockTransactionChartTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.StockTransactionChartTableLayoutPanel.Size = new System.Drawing.Size(737, 522);
            this.StockTransactionChartTableLayoutPanel.TabIndex = 1;
            // 
            // StockTransactionChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.StockTransactionChartTableLayoutPanel);
            this.Name = "StockTransactionChart";
            this.Size = new System.Drawing.Size(737, 522);
            this.StockTransactionChartTableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private LiveCharts.WinForms.CartesianChart TransactionDailyChart;
        private System.Windows.Forms.TableLayoutPanel StockTransactionChartTableLayoutPanel;
    }
}
