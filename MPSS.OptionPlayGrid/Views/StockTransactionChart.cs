﻿using LiveCharts;
using LiveCharts.Wpf;
using MPSS.Common.Model;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    public partial class StockTransactionChart : UserControl
    {
        private IEnumerable<AssetTransaction> Transactions;

        public StockTransactionChart()
        {
            InitializeComponent();

            TransactionDailyChart.Series = new SeriesCollection();


            //TransactionDailyChart.Series = new SeriesCollection
            //{
            //    new ColumnSeries
            //    {
            //        Title = "2015",
            //        Values = new ChartValues<double> { 10, 50, 39, 50 }
            //    }
            //};

            ////adding series will update and animate the chart automatically
            //TransactionDailyChart.Series.Add(new ColumnSeries
            //{
            //    Title = "2016",
            //    Values = new ChartValues<double> { 11, 56, 42 }
            //});

            ////also adding values updates and animates the chart automatically
            //TransactionDailyChart.Series[1].Values.Add(48d);

            //TransactionDailyChart.AxisX.Add(new Axis
            //{
            //    Title = "Sales Man",
            //    Labels = new[] { "Maria", "Susan", "Charles", "Frida" }
            //});

            //TransactionDailyChart.AxisY.Add(new Axis
            //{
            //    Title = "Sold Apps",
            //    LabelFormatter = value => value.ToString("N")
            //});

        }

        public void SetViewModel(IEnumerable<AssetTransaction> transactions)
        {
            Transactions = transactions;

            if (Transactions != null)
            {



                var inactivePairs = Transactions
                    .Where(t => !t.Active && t.IsOptionPlay())
                    .Select(t => new KeyValuePair<AssetTransaction, AssetTransaction>(t, GetParent(t.ParentTransactionId)));

                TransactionDailyChart.Series.Clear();
                var childPrices = new ChartValues<decimal>(
                    inactivePairs
                    .Select(p => p.Key.Price));
                var parentPrices = new ChartValues<decimal>(
                    inactivePairs
                    .Select(p => p.Value != null ? p.Value.Price : 0));
                TransactionDailyChart.Series.Add(
                    new ColumnSeries
                    {
                        Title = "Child",
                        Values = childPrices
                    });
                TransactionDailyChart.Series.Add(
                    new ColumnSeries
                    {
                        Title = "Parent",
                        Values = parentPrices
                    });

                var optionPlayLabels = inactivePairs.Select(p => p.Key.TransactionDisplayType).ToArray();
                TransactionDailyChart.AxisX.Clear();
                TransactionDailyChart.AxisX.Add(new Axis
                {
                    Title = "Option Plays",
                    Labels = optionPlayLabels
                });

                TransactionDailyChart.AxisY.Clear();
                TransactionDailyChart.AxisY.Add(new Axis
                {
                    Title = "Price",
                    LabelFormatter = value => value.ToString("C")
                });
            }
        }

        private AssetTransaction GetParent(int parentTransactionId)
        {
            return Transactions.FirstOrDefault(t => t.TransactionId == parentTransactionId);
        }
    }
}
