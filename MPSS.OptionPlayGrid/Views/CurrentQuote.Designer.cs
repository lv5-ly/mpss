﻿
namespace MPSS.Controls.Views
{
    partial class CurrentQuote
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CurrentQuotePanel = new System.Windows.Forms.Panel();
            this.CurrentQuoteTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.SymbolButton = new System.Windows.Forms.Button();
            this.CurrentPriceButton = new System.Windows.Forms.Button();
            this.HighPriceButton = new System.Windows.Forms.Button();
            this.LowPriceButton = new System.Windows.Forms.Button();
            this.PriceChangeButton = new System.Windows.Forms.Button();
            this.CurrentQuotePanel.SuspendLayout();
            this.CurrentQuoteTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // CurrentQuotePanel
            // 
            this.CurrentQuotePanel.AutoSize = true;
            this.CurrentQuotePanel.Controls.Add(this.CurrentQuoteTableLayoutPanel);
            this.CurrentQuotePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CurrentQuotePanel.Location = new System.Drawing.Point(0, 0);
            this.CurrentQuotePanel.Name = "CurrentQuotePanel";
            this.CurrentQuotePanel.Size = new System.Drawing.Size(249, 119);
            this.CurrentQuotePanel.TabIndex = 0;
            // 
            // CurrentQuoteTableLayoutPanel
            // 
            this.CurrentQuoteTableLayoutPanel.AutoSize = true;
            this.CurrentQuoteTableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.CurrentQuoteTableLayoutPanel.ColumnCount = 3;
            this.CurrentQuoteTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.48688F));
            this.CurrentQuoteTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.92784F));
            this.CurrentQuoteTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.80069F));
            this.CurrentQuoteTableLayoutPanel.Controls.Add(this.SymbolButton, 0, 0);
            this.CurrentQuoteTableLayoutPanel.Controls.Add(this.CurrentPriceButton, 1, 0);
            this.CurrentQuoteTableLayoutPanel.Controls.Add(this.HighPriceButton, 2, 1);
            this.CurrentQuoteTableLayoutPanel.Controls.Add(this.LowPriceButton, 2, 2);
            this.CurrentQuoteTableLayoutPanel.Controls.Add(this.PriceChangeButton, 1, 1);
            this.CurrentQuoteTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CurrentQuoteTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.CurrentQuoteTableLayoutPanel.Name = "CurrentQuoteTableLayoutPanel";
            this.CurrentQuoteTableLayoutPanel.RowCount = 3;
            this.CurrentQuoteTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.96552F));
            this.CurrentQuoteTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 29.31034F));
            this.CurrentQuoteTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.58621F));
            this.CurrentQuoteTableLayoutPanel.Size = new System.Drawing.Size(249, 119);
            this.CurrentQuoteTableLayoutPanel.TabIndex = 0;
            // 
            // SymbolButton
            // 
            this.SymbolButton.AutoSize = true;
            this.SymbolButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SymbolButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SymbolButton.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.SymbolButton.Location = new System.Drawing.Point(6, 6);
            this.SymbolButton.Name = "SymbolButton";
            this.CurrentQuoteTableLayoutPanel.SetRowSpan(this.SymbolButton, 3);
            this.SymbolButton.Size = new System.Drawing.Size(68, 107);
            this.SymbolButton.TabIndex = 2;
            this.SymbolButton.Text = "UAL";
            this.SymbolButton.UseVisualStyleBackColor = true;
            // 
            // CurrentPriceButton
            // 
            this.CurrentPriceButton.AutoSize = true;
            this.CurrentPriceButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CurrentPriceButton.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.CurrentQuoteTableLayoutPanel.SetColumnSpan(this.CurrentPriceButton, 2);
            this.CurrentPriceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CurrentPriceButton.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.CurrentPriceButton.Location = new System.Drawing.Point(83, 6);
            this.CurrentPriceButton.Name = "CurrentPriceButton";
            this.CurrentPriceButton.Size = new System.Drawing.Size(160, 40);
            this.CurrentPriceButton.TabIndex = 7;
            this.CurrentPriceButton.Text = "$123.45";
            this.CurrentPriceButton.UseVisualStyleBackColor = false;
            // 
            // HighPriceButton
            // 
            this.HighPriceButton.AutoSize = true;
            this.HighPriceButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.HighPriceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HighPriceButton.Location = new System.Drawing.Point(159, 55);
            this.HighPriceButton.Name = "HighPriceButton";
            this.HighPriceButton.Size = new System.Drawing.Size(84, 25);
            this.HighPriceButton.TabIndex = 4;
            this.HighPriceButton.Text = "$150.11";
            this.HighPriceButton.UseVisualStyleBackColor = true;
            // 
            // LowPriceButton
            // 
            this.LowPriceButton.AutoSize = true;
            this.LowPriceButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LowPriceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LowPriceButton.Location = new System.Drawing.Point(159, 89);
            this.LowPriceButton.Name = "LowPriceButton";
            this.LowPriceButton.Size = new System.Drawing.Size(84, 24);
            this.LowPriceButton.TabIndex = 5;
            this.LowPriceButton.Text = "$150.11";
            this.LowPriceButton.UseVisualStyleBackColor = true;
            // 
            // PriceChangeButton
            // 
            this.PriceChangeButton.AutoSize = true;
            this.PriceChangeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PriceChangeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PriceChangeButton.ForeColor = System.Drawing.Color.Firebrick;
            this.PriceChangeButton.Location = new System.Drawing.Point(83, 55);
            this.PriceChangeButton.Name = "PriceChangeButton";
            this.CurrentQuoteTableLayoutPanel.SetRowSpan(this.PriceChangeButton, 2);
            this.PriceChangeButton.Size = new System.Drawing.Size(67, 58);
            this.PriceChangeButton.TabIndex = 6;
            this.PriceChangeButton.Text = "+$12.34";
            this.PriceChangeButton.UseVisualStyleBackColor = true;
            // 
            // CurrentQuote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CurrentQuotePanel);
            this.Name = "CurrentQuote";
            this.Size = new System.Drawing.Size(249, 119);
            this.CurrentQuotePanel.ResumeLayout(false);
            this.CurrentQuotePanel.PerformLayout();
            this.CurrentQuoteTableLayoutPanel.ResumeLayout(false);
            this.CurrentQuoteTableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel CurrentQuotePanel;
        private System.Windows.Forms.TableLayoutPanel CurrentQuoteTableLayoutPanel;
        private System.Windows.Forms.Button SymbolButton;
        private System.Windows.Forms.Button HighPriceButton;
        private System.Windows.Forms.Button LowPriceButton;
        private System.Windows.Forms.Button CurrentPriceButton;
        private System.Windows.Forms.Button PriceChangeButton;
    }
}
