﻿using MPSS.Common.Model;
using MPSS.Controls.Dialogs;
using MPSS.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace MPSS.OptionPlayControl
{
    public partial class CurrentMonthView : UserControl
    {
        private static readonly DateTimeFormatInfo DTFI = new();
        private StockAssetViewModel ViewModel;
        private readonly SortableBindingList<AnnualSummary> AnnualSummaryList = new();
        private readonly AnnualSummaryYear SelectableYear = new();

        public CurrentMonthView()
        {
            InitializeComponent();

            AnnualSummaryList.Add(new AnnualSummary());

            // Bind the grid
            DGV_AnnualSummary.DataSource = AnnualSummaryList;

            // Bind data with numeric up-down
            // NUD_YearSelector.DataBindings.Add("Value", SelectableYear, "Year");
            NUD_YearSelector.Maximum = DateTime.Now.Year;
            NUD_YearSelector.Minimum = 0;
            NUD_YearSelector.Value = DateTime.Now.Year;


        }

        public void Refresh(StockAssetViewModel vModel)
        {
            // Keep a copy
            ViewModel = vModel;

            /// Get the current Month and Year in integer form
            int currentMonth = DateTime.Now.Month;
            int currentYear = DateTime.Now.Year;

            /// Display the current date and montn/year label
            DateTime now = DateTime.Now;
            labelMMMYYY.Text = now.ToString("MMMM yyyy");

            /// Loop thru all the stock assets and add up too the P&L for the current month and year
            decimal profit = 0;
            foreach (var s in vModel.AllStocks)
            {
                profit += s.GetMonthlyPandL(currentMonth, currentYear);
            }
            txbMonthlyPandL.Text = profit.ToString("C");

            // Loop thru all the months calculate YTD and monhtly P&L for all assets
            decimal YTDprofit = 0;
            decimal[] monthlyProfit;
            monthlyProfit = new decimal[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            for (int i = 1; i <= currentMonth; i++)
            {
                foreach (var s in vModel.AllStocks)
                {
                    YTDprofit += s.GetMonthlyPandL(i, currentYear);
                    monthlyProfit[i - 1] = monthlyProfit[i - 1] + s.GetMonthlyPandL(i, currentYear);
                }
            }
            txbYTDPandL.Text = YTDprofit.ToString("C");

            if (vModel.AllStocks.Count > 0)
            {
                // Setup the year selector
                var minYear = vModel.AllStocks
                    .SelectMany(s => s.Transactions)
                    .OrderBy(t => t.TransactionDate)
                    .Select(t => t.TransactionDate.Year)
                    .First();
                NUD_YearSelector.Minimum = minYear;
                NUD_YearSelector.Value = DateTime.Now.Year;
                CalculateAnnualSummary(Convert.ToInt32(NUD_YearSelector.Value), ViewModel.AllStocks);
                DGV_AnnualSummary.Refresh();
            }
        }


        #region Event Handlers
        /// <summary>
        /// This is a handler when user clicks on one of the monthly P&L 
        ///     text box. 
        /// </summary>
        /// <param name="sender">The text box, on which user clicks</param>
        /// <param name="e">Mouse position</param>
        private void MonthlyPL_Click(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            var monthIndex = DTFI.AbbreviatedMonthNames
                .Select((s, i) => new { s, i })
                .FirstOrDefault(m => textBox.Name.Contains(m.s));
            var dialog = new RealizedPLReportDialog(monthIndex.i, ViewModel);
            dialog.ShowDialog();
            dialog.Close();
        }
        #endregion Event Handlers

        #region Private Methods

        #endregion Private Methods

        private void gbCurrentMonth_Enter(object sender, EventArgs e)
        {

        }

        private void multiColumnsMultiRowsGrid1_Load(object sender, EventArgs e)
        {

        }

        private void DGV_AnnualSummary_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void LB_AnnualSummary_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void NUD_YearSelector_ValueChanged(object sender, EventArgs e)
        {
            CalculateAnnualSummary(Convert.ToInt32(NUD_YearSelector.Value), ViewModel?.AllStocks);
            DGV_AnnualSummary.Refresh();
        }

        private void CalculateAnnualSummary(int y, List<StockAsset> stocks)
        {
            var asl = AnnualSummaryList.First();
            for (var m = 0; m < 12; m++)
            {
                asl.Clear(m);
                if (stocks != null)
                {
                    foreach (var s in stocks)
                    {
                        asl.Add(m, s.GetMonthlyPandL(m + 1, y));
                    }
                }
            }

        }

        private void DGV_AnnualSummary_Click(object sender, EventArgs e)
        {
            var grid = sender as DataGridView;
            int ci = grid.CurrentCell.ColumnIndex;
            int y = Convert.ToInt32(NUD_YearSelector.Value);
            var dialog = new RealizedPLReportDialog(ci, ViewModel, y);
            dialog.ShowDialog();
            dialog.Close();
        }
    }

}
