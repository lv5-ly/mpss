﻿using MPSS.Common.DataService;
using MPSS.Common.Model.FinnHub;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    public partial class CurrentQuote : UserControl
    {
        private string StockSymbol;
        private Quote StockQuote;
        private IStockDataService DataService;

        public CurrentQuote()
        {
            InitializeComponent();
        }

        public void SetStockSymbol(string symbol)
        {
            StockSymbol = symbol;

            RetrieveQuoteDataAsync();
            DisplayQuoteData();
        }

        private void DisplayQuoteData()
        {
            SymbolButton.Text = StockSymbol;

            if (StockQuote != null)
            {
                CurrentPriceButton.Text = StockQuote.CurrentPrice.ToString("C");
                PriceChangeButton.Text = StockQuote.PriceChange.ToString("+$0.00;-$0.00;$0.00");
                HighPriceButton.Text = $"{StockQuote.HighPrice:C} (High)";
                LowPriceButton.Text = $"{StockQuote.LowPrice:C} (Low)"; ;

                // Let's also change the color
                CurrentPriceButton.BackColor = StockQuote.CurrentPrice > StockQuote.PreviousClosePrice ?
                    Color.DarkSeaGreen : Color.Salmon;

                PriceChangeButton.ForeColor = StockQuote.CurrentPrice > StockQuote.PreviousClosePrice ?
                    Color.DarkGreen : Color.Firebrick;
            }
        }

        private async void RetrieveQuoteDataAsync()
        {
            if (DataService != null)
            {
                try
                {
                    var newQuote = await DataService.GetStockPriceAsync(StockSymbol);
                    StockQuote = newQuote;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Unexpected exception {ex.Message}");
                }
            }
        }

        public void SetDataService(IStockDataService service)
        {
            DataService = service;
        }
    }
}
