﻿using MPSS.Common.Model;
using System;
using System.Windows.Forms;

namespace MPSS.StatisticControl
{
    public partial class StockMonthlyView : UserControl
    {
        private StockAsset stockAsset = null;

        public StockMonthlyView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set the currently select stock asset
        /// </summary>
        /// <param name="currentSelectedAsset">Selected stock asset</param>
        public void SetStockAsset(StockAsset currentSelectedAsset)
        {
            stockAsset = currentSelectedAsset;

            // Set the option play control's stock object, so it can 
            optionPlayControl.CurrentStock = stockAsset;
            // Then tell it to refresh, basically re-calculate all
            //  numbers
            optionPlayControl.Refresh();
        }

        /// <summary>
        /// Calculate and upate the values in all fields in the panel
        /// </summary>
        public void RefreshStockStatistics()
        {
            int currentMonth = DateTime.Now.Month;
            int currentYear = DateTime.Now.Year;
            if (stockAsset != null &&
                stockAsset.Transactions != null &&
                stockAsset.Transactions.Count > 0)
            {
                // For the case of the user enters only naked calls/puts, the stock does not 
                //  have a bought-share transaction, that means, average share price cannot
                //  be calculated. Also, it's best for the StockAsset object to do this 
                //  type of calculation, because it has a list of transactions.
                tbxAverageStockPrice.Text = stockAsset.GetAverageStockPrice().ToString("C");

                txbTicker.Text = stockAsset.Symbol;

                int s = stockAsset.GetNumberOfShares();
                txbShares.Text = s.ToString("D");

                /// Populate P&L text box
                decimal PandL = stockAsset.GetMonthlyPandL(currentMonth, currentYear);
                txbMonthlyPL.Text = PandL.ToString("C");

                /// Populate P&L %
                /// Note: 
                ///     If the average is 0, that means the stock does not have a
                ///     transaction, of which it bought the shares. The stock may
                ///     only have option play transactions.
                decimal PandLRatio = 0M;
                if (stockAsset.GetAverageStockPrice() != 0 && s > 0)
                {
                    PandLRatio = PandL / (stockAsset.GetAverageStockPrice() * s);
                }
                txbMonthlyPLRatio.Text = PandLRatio.ToString("P");

                /// Calculate Simulated  P&L for selected stock CC, including realized P&L
                txbSimPandLCC.Text = (stockAsset.GetExpiredPandL() +
                                      stockAsset.GetMonthlyPandL(currentMonth, currentYear)).ToString("C");

                /// Calcualted SImualted P&L for selected stock assigment, including realized P&L
                txbSimPandLStkAssign.Text = (stockAsset.GetExpiredPandL() +
                                             stockAsset.GetStockAssignedPandL() +
                                             stockAsset.GetMonthlyPandL(currentMonth, currentYear)).ToString("C");

                /// Calculate Stock YTD P&L
                txbStkYTDPandL.Text = stockAsset.GetYTDPandL().ToString("C");

                /// Calculate Stock Total P&L
                txbStkTotalPandL.Text = stockAsset.GetTotalPandL().ToString("C");
            }
        }
    }
}
