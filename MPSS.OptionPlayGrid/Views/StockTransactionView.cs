﻿using MPSS.Common.Model;
using MPSS.Common.ViewModel;
using MPSS.OptionPlayControl;
using MPSS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    /// <summary>
    /// This is an user-control, which contains
    /// - Transaction Data Grid
    /// - Hide Inactive transaction check-box
    /// - Close, Edit, and Delete buttons
    /// </summary>
    public partial class StockTransactionView : UserControl
    {
        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked transaction list is changed")]
        public event EventHandler<StockTransactionEventArgs> TransactionListChanged;

        [Browsable(true)]
        [Category("Action")]
        [Description("Transaction closed")]
        public event EventHandler<StockTransactionEventArgs> TransactionClosed;
        #endregion Public Properties

        #region Private Properties
        private readonly string DELETE_TRANSACTION_CONFIRMATION_MESSAGE = "Are you sure you want to remove this transaction";
        private readonly string DELETE_TRANSACTION_CAPTION = "Remove Transaction";
        private StockAssetViewModel viewModel;
        private StockAsset CurrentStockAsset;
        private readonly BindingSource transactionBindingSource = new();

        private readonly BindingList<TransactionViewModel> transactionBindingList = new();
        private bool HideInactiveTransaction
        {
            get
            {
                return HideInactiveTransactionCheckBox.Checked;
            }
        }

        public void SetViewModel(StockAsset sa)
        {
            CurrentStockAsset = sa;
            if (CurrentStockAsset != null)
            {

            }
        }

        private SortOrder currentSortOrder = SortOrder.None;

        private int selectedTransactionId = 0;

        private TransactionViewModel selectedTransaction = null;
        #endregion Private Properties

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public StockTransactionView()
        {
            InitializeComponent();
        }
        #endregion Constructor

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Refresh()
        {
            base.Refresh();

            TransactionDataGrid.Columns[0].HeaderCell.SortGlyphDirection = currentSortOrder;

            SetBackgroundColorForInactiveRows(Color.LightGray);

            // Put back the originally selected transaction
            foreach (DataGridViewRow r in TransactionDataGrid.Rows)
            {
                var transaction = r.DataBoundItem as TransactionViewModel;
                if (transaction.GetTransactionId() == selectedTransactionId)
                {
                    r.Selected = true;
                }
            }

            SetButtonState();
        }


        /// <summary>
        /// Save the viewModel object, to use in this control.
        /// This will be called right after the data is loaded
        /// into the viewModel
        /// </summary>
        /// <param name="vm">view model object</param>
        public void SetViewModel(StockAssetViewModel vm)
        {
            viewModel = vm;

            if (viewModel != null)
            {
                LoadDataIntoTransactionBindingList();

                if (transactionBindingSource.DataSource == null)
                {
                    transactionBindingSource.DataSource = transactionBindingList;
                    TransactionDataGrid.DataSource = transactionBindingSource;

                    FormatTransactionGrid(TransactionDataGrid);
                }
            }
        }

        public void LoadStockTransactions()
        {
            LoadDataIntoTransactionBindingList();
            Refresh();
        }


        /// <summary>
        /// Load selected stock's transaction from the viewModel, into the 
        /// transaction grid's binding list.
        /// </summary>
        public int LoadDataIntoTransactionBindingList()
        {
            // int selectedTransactionId = 0;
            transactionBindingList.Clear();
            if (viewModel != null && viewModel.HasStockAsset)
            {
                foreach (var t in GetSelectedStockTransaction())
                {
                    if (t.IsActive() || !HideInactiveTransaction)
                    {
                        transactionBindingList.Add(new TransactionViewModel(t));
                    }
                }

                // Pick the first transaction as the default currently
                //  selected transaction. If the list is empty, set
                //  the transactionId as 0
                selectedTransactionId = selectedTransactionId != 0 ?
                    selectedTransactionId :
                    transactionBindingList.Count > 0 ?
                    transactionBindingList.First().GetTransactionId() :
                    0;
            }
            return selectedTransactionId;
        }
        #endregion Public Methods

        #region Event Handlers
        private void EditTransactionButton_Click(object sender, EventArgs e)
        {
            var t = viewModel.CurrentSelectedTransaction;
            if (t.IsSpreadPlay)
            {
                EditSpreadPlay(t);
            }
            else
            {
                var buyDialog = new BuyShareDialog(viewModel, AssetTransactionAction.Edit);

                if (buyDialog.ShowDialog() == DialogResult.OK)
                {
                    // Tell everyone to refresh
                    LoadDataIntoTransactionBindingList();
                    Refresh();

                    // Raise TransactionListChanged event
                    RaiseEvent(TransactionListChanged, sender,
                        new StockTransactionEventArgs(null));
                }
            }
        }

        private void DeleteTransactionButton_Click(object sender, EventArgs e)
        {
            if (selectedTransactionId != 0)
            {
                var result = MessageBox.Show(
                    DELETE_TRANSACTION_CONFIRMATION_MESSAGE,
                    DELETE_TRANSACTION_CAPTION,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    // Tell the viewModel to remove the transaction, which basically
                    //  tell the database to remove the transaction record
                    viewModel.DeleteTransaction();

                    // Remove transaction from the transaction grid's data binding list
                    transactionBindingList.Remove(selectedTransaction);

                    // Tell everyone to refresh
                    LoadDataIntoTransactionBindingList();
                    Refresh();

                    // Raise TransactionListChanged event
                    RaiseEvent(TransactionListChanged,
                        sender,
                        new StockTransactionEventArgs(selectedTransaction.GetTransaction()));
                }
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            AssetTransaction parentTransaction = selectedTransaction.GetTransaction();
            CloseTransactionDialog dialog = (parentTransaction.IsShareTransaction()) ?
                new CloseShareDialog(parentTransaction) :
                new CloseOptionPlayDialog(parentTransaction);

            AssetTransaction childTransaction = dialog.ShowDialog() == DialogResult.OK ?
                dialog.Transaction : null;
            dialog.Close();

            if (childTransaction != null)
            {
                var eventArgs = new StockTransactionEventArgs(parentTransaction, childTransaction);
                RaiseEvent(TransactionClosed, this, eventArgs);
            }
        }

        private void TransactionDataGridView_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Selected &&
                TransactionDataGrid.SelectedRows.Count > 0)
            {
                selectedTransaction = TransactionDataGrid.SelectedRows[0].DataBoundItem as TransactionViewModel;
                selectedTransactionId = selectedTransaction.GetTransactionId();

                // Tell the viewModel about currently selected transaction
                viewModel.SelectTransaction(selectedTransaction.GetTransaction());

                SetButtonState();
            }
        }

        /// <summary>
        /// Whenever checked value is chanaged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HideInactiveTransactionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            LoadDataIntoTransactionBindingList();
            Refresh();
        }

        private void StockTransactionView_Load(object sender, EventArgs e)
        {
            if (TransactionDataGrid.Columns.Count > 0)
            {
                var column = TransactionDataGrid.Columns[0];

                // Set the sort order glyph
                column.SortMode = DataGridViewColumnSortMode.Programmatic;

                // Ask the DataGridView to indicate the sort glyph
                column.HeaderCell.SortGlyphDirection = SortOrder.None;

                SetBackgroundColorForInactiveRows(Color.LightGray);
            }
        }

        /// <summary>
        /// User just clicks on the header column, requests sorting 
        /// </summary>
        /// <param name="sender">Component generating this event</param>
        /// <param name="e">Event argument</param>
        private void TransactionDataGrid_ColumnHeaderMouseClick(
            object sender,
            DataGridViewCellMouseEventArgs e)
        {
            // We support only sorting on column 0, which is the TransactionDate; 
            //  and also when we have more than one transactions in the grid
            if (e.ColumnIndex == 0 &&
                TransactionDataGrid.Rows.Count > 1)
            {
                // Set the next sort-order
                currentSortOrder = currentSortOrder == SortOrder.None ?
                    SortOrder.Ascending :
                    currentSortOrder == SortOrder.Ascending ?
                    SortOrder.Descending : SortOrder.None;

                LoadDataIntoTransactionBindingList();
                Refresh();
            }
        }
        #endregion Event Handlers

        #region Private Methods
        /// <summary>
        /// Edit a spread option play
        /// </summary>
        /// <param name="t">Spread Option transaction</param>
        private void EditSpreadPlay(AssetTransaction t)
        {
            var dialog = new OptionPlayDialog(t);

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                viewModel.UpdateTransaction(dialog.transaction);

                // Tell everyone to refresh
                LoadDataIntoTransactionBindingList();
                Refresh();

                // Raise TransactionListChanged event
                RaiseEvent(TransactionListChanged, this,
                    new StockTransactionEventArgs(null));
            }
        }



        private void SetButtonState()
        {
            // If user selects an active transaction, he can edit or delete that transaction
            EditTransactionButton.Enabled = DeleteTransactionButton.Enabled = selectedTransactionId != 0;

            CloseButton.Enabled = selectedTransactionId != 0 && selectedTransaction.IsActive();
        }
        private static void RaiseEvent(EventHandler<StockTransactionEventArgs> eventHandler, object sender, StockTransactionEventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }


        private IEnumerable<AssetTransaction> GetSelectedStockTransaction()
        {
            return currentSortOrder == SortOrder.None ?
                    viewModel.GetCurrentSelectedStockTransactions() :
                    currentSortOrder == SortOrder.Ascending ?
                    viewModel.GetCurrentSelectedStockTransactions().OrderBy(r => r.TransactionDate) :
                    viewModel.GetCurrentSelectedStockTransactions().OrderByDescending(r => r.TransactionDate);

        }

        private static void FormatTransactionGrid(DataGridView dgv)
        {
            dgv.Columns["TransactionDate"].HeaderText = "On";
            dgv.Columns["TransactionDate"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["TransactionDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["TransactionDate"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgv.Columns["TransactionDate"].Width = 80;

            dgv.Columns["TransactionDisplayType"].HeaderText = "Transaction Type";
            dgv.Columns["TransactionDisplayType"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["TransactionDisplayType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgv.Columns["DisplayQuantity"].HeaderText = "Shares";
            dgv.Columns["DisplayQuantity"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["DisplayQuantity"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["DisplayQuantity"].DefaultCellStyle.Format = "N0";
            dgv.Columns["DisplayQuantity"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgv.Columns["DisplayQuantity"].Width = 80;

            dgv.Columns["Price"].HeaderText = "Price";
            dgv.Columns["Price"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["Price"].DefaultCellStyle.Format = "c";

            dgv.Columns["Notes"].HeaderText = "Notes";
            dgv.Columns["Notes"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["Notes"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }


        private void SetBackgroundColorForInactiveRows(Color backgroundColor)
        {
            foreach (var row in GetInactiveRows())
            {
                row.DefaultCellStyle.BackColor = backgroundColor;
            }
        }

        private IEnumerable<DataGridViewRow> GetInactiveRows()
        {
            return TransactionDataGrid.Rows
                .OfType<DataGridViewRow>()
                .Where(r => (r.DataBoundItem as TransactionViewModel).IsInactive)
                .Select(r => r);
        }
        #endregion Private Methods
    }
}
