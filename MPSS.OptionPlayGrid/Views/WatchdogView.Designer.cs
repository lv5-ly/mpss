﻿
namespace MPSS.Controls.Views
{
    partial class WatchdogView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WatchdogView));
            this.OptionDataWorker = new System.ComponentModel.BackgroundWorker();
            this.WorkerLogGroupBox = new System.Windows.Forms.GroupBox();
            this.WorkersLog = new System.Windows.Forms.TextBox();
            this.WorkerStatusStatusStrip = new System.Windows.Forms.StatusStrip();
            this.WatchdogSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.WatchdogTakeABreak = new System.Windows.Forms.ToolStripMenuItem();
            this.WatchdogGoToWork = new System.Windows.Forms.ToolStripMenuItem();
            this.ClearLog = new System.Windows.Forms.ToolStripMenuItem();
            this.WorkerProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.WorkerStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.WorkerLogGroupBox.SuspendLayout();
            this.WorkerStatusStatusStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OptionDataWorker
            // 
            this.OptionDataWorker.WorkerReportsProgress = true;
            this.OptionDataWorker.WorkerSupportsCancellation = true;
            this.OptionDataWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.OptionDataWorker_DoWork);
            this.OptionDataWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.OptionDataWorker_ProgressChanged);
            this.OptionDataWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.OptionDataWorker_RunWorkerCompleted);
            // 
            // WorkerLogGroupBox
            // 
            this.WorkerLogGroupBox.Controls.Add(this.WorkersLog);
            this.WorkerLogGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkerLogGroupBox.Location = new System.Drawing.Point(3, 3);
            this.WorkerLogGroupBox.Name = "WorkerLogGroupBox";
            this.WorkerLogGroupBox.Size = new System.Drawing.Size(657, 250);
            this.WorkerLogGroupBox.TabIndex = 0;
            this.WorkerLogGroupBox.TabStop = false;
            this.WorkerLogGroupBox.Text = "Watchdog\'s log";
            // 
            // WorkersLog
            // 
            this.WorkersLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.WorkersLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WorkersLog.Location = new System.Drawing.Point(3, 19);
            this.WorkersLog.Multiline = true;
            this.WorkersLog.Name = "WorkersLog";
            this.WorkersLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.WorkersLog.Size = new System.Drawing.Size(651, 228);
            this.WorkersLog.TabIndex = 0;
            // 
            // WorkerStatusStatusStrip
            // 
            this.WorkerStatusStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WatchdogSplitButton,
            this.WorkerStatusLabel,
            this.WorkerProgressBar});
            this.WorkerStatusStatusStrip.Location = new System.Drawing.Point(0, 259);
            this.WorkerStatusStatusStrip.Name = "WorkerStatusStatusStrip";
            this.WorkerStatusStatusStrip.ShowItemToolTips = true;
            this.WorkerStatusStatusStrip.Size = new System.Drawing.Size(663, 22);
            this.WorkerStatusStatusStrip.TabIndex = 1;
            this.WorkerStatusStatusStrip.Text = "Watchdog Status";
            // 
            // WatchdogSplitButton
            // 
            this.WatchdogSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.WatchdogSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WatchdogTakeABreak,
            this.WatchdogGoToWork,
            this.ClearLog});
            this.WatchdogSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("WatchdogSplitButton.Image")));
            this.WatchdogSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.WatchdogSplitButton.Name = "WatchdogSplitButton";
            this.WatchdogSplitButton.Size = new System.Drawing.Size(32, 20);
            this.WatchdogSplitButton.Text = "WatchdogSplitButton";
            this.WatchdogSplitButton.ToolTipText = "Command the dog to take a break or continue working";
            // 
            // WatchdogTakeABreak
            // 
            this.WatchdogTakeABreak.Name = "WatchdogTakeABreak";
            this.WatchdogTakeABreak.Size = new System.Drawing.Size(142, 22);
            this.WatchdogTakeABreak.Text = "Take a break";
            this.WatchdogTakeABreak.Click += new System.EventHandler(this.WatchdogTakeABreak_Click);
            // 
            // WatchdogGoToWork
            // 
            this.WatchdogGoToWork.Name = "WatchdogGoToWork";
            this.WatchdogGoToWork.Size = new System.Drawing.Size(142, 22);
            this.WatchdogGoToWork.Text = "Back to work";
            this.WatchdogGoToWork.ToolTipText = "Tell the dog to continue scanning";
            this.WatchdogGoToWork.Click += new System.EventHandler(this.WatchdogGoToWork_Click);
            // 
            // ClearLog
            // 
            this.ClearLog.AutoToolTip = true;
            this.ClearLog.Name = "ClearLog";
            this.ClearLog.Size = new System.Drawing.Size(142, 22);
            this.ClearLog.Text = "Clear log";
            this.ClearLog.ToolTipText = "Clear log on next scan";
            this.ClearLog.Click += new System.EventHandler(this.ClearLog_Click);
            // 
            // WorkerProgressBar
            // 
            this.WorkerProgressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.WorkerProgressBar.AutoToolTip = true;
            this.WorkerProgressBar.Name = "WorkerProgressBar";
            this.WorkerProgressBar.Size = new System.Drawing.Size(200, 16);
            this.WorkerProgressBar.Step = 1;
            this.WorkerProgressBar.ToolTipText = "Watchdog Progress";
            // 
            // WorkerStatusLabel
            // 
            this.WorkerStatusLabel.Name = "WorkerStatusLabel";
            this.WorkerStatusLabel.Size = new System.Drawing.Size(96, 17);
            this.WorkerStatusLabel.Text = "Watchdog status";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.WorkerStatusStatusStrip, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.WorkerLogGroupBox, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(663, 281);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // WatchdogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "WatchdogView";
            this.Size = new System.Drawing.Size(663, 281);
            this.WorkerLogGroupBox.ResumeLayout(false);
            this.WorkerLogGroupBox.PerformLayout();
            this.WorkerStatusStatusStrip.ResumeLayout(false);
            this.WorkerStatusStatusStrip.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker OptionDataWorker;
        private System.Windows.Forms.GroupBox WorkerLogGroupBox;
        private System.Windows.Forms.TextBox WorkersLog;
        //private System.Windows.Forms.StatusStrip WorkerStatus;
        private System.Windows.Forms.ToolStripStatusLabel WorkerStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar WorkerProgressBar;
        private System.Windows.Forms.StatusStrip WorkerStatusStatusStrip;
        private System.Windows.Forms.ToolStripSplitButton WatchdogSplitButton;
        //private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        //private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem WatchdogTakeABreak;
        private System.Windows.Forms.ToolStripMenuItem WatchdogGoToWork;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStripMenuItem ClearLog;
    }
}
