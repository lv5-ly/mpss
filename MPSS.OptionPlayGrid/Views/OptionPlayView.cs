﻿using MPSS.Common.Model;
using System.ComponentModel;
using System.Windows.Forms;

namespace MPSS.OptionPlayControl
{
    /// <summary>
    /// This is the control to display option plays
    /// </summary>
    public partial class OptionPlayView : UserControl
    {
        public StockAsset CurrentStock = null;

        //private readonly List<AssetTransactionAction> TransactionActions = new()
        //{
        //    AssetTransactionAction.Bought,
        //    AssetTransactionAction.Sold
        //};
        //private readonly List<AssetTransactionType> TransactionTypes = new()
        //{
        //    AssetTransactionType.CoveredCall,
        //    AssetTransactionType.CoveredPut
        //};
        private readonly BindingList<OptionPlayMetric> OptionPlays = new();

        public OptionPlayView()
        {
            InitializeComponent();

            // Have the grid view to bind with a data object
            OptionPlays = new BindingList<OptionPlayMetric>();
            dataGridViewOptionPlays.DataSource = OptionPlays;

            // Let's format the grid columns, so that it can display
            //  the values correctly/nicely            
            dataGridViewOptionPlays.Columns["Contracts"].DefaultCellStyle.Format = "N0";
            dataGridViewOptionPlays.Columns["Contracts"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridViewOptionPlays.Columns["ContractType"].HeaderText = "Transaction Type";
            dataGridViewOptionPlays.Columns["ContractType"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewOptionPlays.Columns["ContractType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dataGridViewOptionPlays.Columns["Premium"].DefaultCellStyle.Format = "c";
            dataGridViewOptionPlays.Columns["Premium"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewOptionPlays.Columns["UnrealizedValue"].DefaultCellStyle.Format = "c";
            dataGridViewOptionPlays.Columns["UnrealizedValue"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dataGridViewOptionPlays.Columns["RealizedValue"].DefaultCellStyle.Format = "c";
            dataGridViewOptionPlays.Columns["RealizedValue"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }

        /// <summary>
        /// Override the refresh method, to trigger the recalculation of
        /// all metrics
        /// </summary>
        public override void Refresh()
        {
            base.Refresh();

            OptionPlays.Clear();
            if (CurrentStock != null)
            {
                /// Calculate type Covered-call option play P&Ls
                if (CurrentStock.HasThisPlay(AssetTransactionAction.Sold, AssetTransactionType.CoveredCall))
                {
                    var CCcredit = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Sold, AssetTransactionType.CoveredCall);
                    var CCdebit = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Bought, AssetTransactionType.CoveredCall);
                    OptionPlays.Add(new OptionPlayMetric()
                    {
                        Contracts = CCcredit.Contracts,
                        ContractType = CCcredit.ContractType,
                        Premium = CCcredit.Premium - CCdebit.Premium,
                        UnrealizedValue = CCcredit.UnrealizedValue + CCdebit.UnrealizedValue,
                        RealizedValue = CCcredit.RealizedValue + CCdebit.RealizedValue,
                    });
                }

                /// Calculate covered Put option play P&Ls
                if (CurrentStock.HasThisPlay(AssetTransactionAction.Sold, AssetTransactionType.CoveredPut))
                {
                    var CPcredit = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Sold, AssetTransactionType.CoveredPut);
                    var CPdebit = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Bought, AssetTransactionType.CoveredPut);
                    OptionPlays.Add(new OptionPlayMetric()
                    {
                        Contracts = CPcredit.Contracts,
                        ContractType = CPcredit.ContractType,
                        Premium = CPcredit.Premium - CPdebit.Premium,
                        UnrealizedValue = CPcredit.UnrealizedValue + CPdebit.UnrealizedValue,
                        RealizedValue = CPcredit.RealizedValue + CPdebit.RealizedValue,
                    });
                }

                /// Calculate credit Spread play P&Ls
                if (CurrentStock.HasThisPlay(AssetTransactionAction.Sold, AssetTransactionType.CreditSpread))
                {
                    var SpreadCreditOpen = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Sold, AssetTransactionType.CreditSpread);
                    var SpreadCreditClose = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Bought, AssetTransactionType.CreditSpread);
                    OptionPlays.Add(new OptionPlayMetric()
                    {
                        Contracts = SpreadCreditOpen.Contracts,
                        ContractType = SpreadCreditOpen.ContractType,
                        Premium = SpreadCreditOpen.Premium - SpreadCreditClose.Premium,
                        UnrealizedValue = SpreadCreditOpen.UnrealizedValue + SpreadCreditClose.UnrealizedValue,
                        RealizedValue = SpreadCreditOpen.RealizedValue + SpreadCreditClose.RealizedValue,
                    });
                }

                /// Calculate Open call option play P&L
                if (CurrentStock.HasThisPlay(AssetTransactionAction.Bought, AssetTransactionType.CoveredCall))
                {
                    var CallClose = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Sold, AssetTransactionType.CoveredCall);
                    var CallOpen = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Bought, AssetTransactionType.CoveredCall);
                    OptionPlays.Add(new OptionPlayMetric()
                    {
                        Contracts = CallOpen.Contracts,
                        ContractType = CallOpen.ContractType,
                        Premium = CallOpen.Premium - CallClose.Premium,
                        UnrealizedValue = CallOpen.UnrealizedValue + CallClose.UnrealizedValue,
                        RealizedValue = CallOpen.RealizedValue + CallClose.RealizedValue,
                    });
                }

                /// Calculate Open Put option play P&L
                if (CurrentStock.HasThisPlay(AssetTransactionAction.Bought, AssetTransactionType.CoveredPut))
                {
                    var PutClose = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Sold, AssetTransactionType.CoveredPut);
                    var PutOpen = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Bought, AssetTransactionType.CoveredPut);
                    OptionPlays.Add(new OptionPlayMetric()
                    {
                        Contracts = PutOpen.Contracts,
                        ContractType = PutOpen.ContractType,
                        Premium = PutOpen.Premium - PutClose.Premium,
                        UnrealizedValue = PutOpen.UnrealizedValue + PutClose.UnrealizedValue,
                        RealizedValue = PutOpen.RealizedValue + PutClose.RealizedValue,
                    });
                }

                /// Calculate debit Spread play P&Ls
                if (CurrentStock.HasThisPlay(AssetTransactionAction.Bought, AssetTransactionType.DebitSpread))
                {
                    var SpreadDebitOpen = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Bought, AssetTransactionType.DebitSpread);
                    var SpreadDebitClose = new OptionPlayMetric(CurrentStock, AssetTransactionAction.Sold, AssetTransactionType.DebitSpread);
                    OptionPlays.Add(new OptionPlayMetric()
                    {
                        Contracts = SpreadDebitOpen.Contracts,
                        ContractType = SpreadDebitOpen.ContractType,
                        Premium = SpreadDebitOpen.Premium - SpreadDebitClose.Premium,
                        UnrealizedValue = SpreadDebitOpen.UnrealizedValue + SpreadDebitClose.UnrealizedValue,
                        RealizedValue = SpreadDebitOpen.RealizedValue + SpreadDebitClose.RealizedValue,
                    });
                }
            }
        }
    }
}
