﻿
namespace MPSS.Controls.Views
{
    partial class ActiveOptionsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MCMRGrid = new MPSS.Controls.Components.MultiColumnsMultiRowsGrid();
            this.SuspendLayout();
            // 
            // MCMRGrid
            // 
            this.MCMRGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MCMRGrid.Location = new System.Drawing.Point(0, 0);
            this.MCMRGrid.Name = "MCMRGrid";
            this.MCMRGrid.Size = new System.Drawing.Size(674, 374);
            this.MCMRGrid.TabIndex = 0;
            this.MCMRGrid.Title = "Multi Columns Multi Rows Grid";
            this.MCMRGrid.RowDoubleClick += new System.EventHandler<MPSS.Common.Model.StockTransactionEventArgs>(this.MCMRGrid_RowDoubleClick);
            this.MCMRGrid.ContextMenuItemClicked += new System.EventHandler<MPSS.Common.Model.StockTransactionEventArgs>(this.MCMRGrid_ContextMenuItemClicked);
            // 
            // ActiveOptionsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MCMRGrid);
            this.Name = "ActiveOptionsView";
            this.Size = new System.Drawing.Size(674, 374);
            this.ResumeLayout(false);

        }

        #endregion

        private Components.MultiColumnsMultiRowsGrid MCMRGrid;
    }
}
