﻿
namespace MPSS.OptionPlayControl
{
    partial class CurrentMonthView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbCurrentMonth = new System.Windows.Forms.GroupBox();
            this.DGV_AnnualSummary = new System.Windows.Forms.DataGridView();
            this.NUD_YearSelector = new System.Windows.Forms.NumericUpDown();
            this.txbYTDPandL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelMMMYYY = new System.Windows.Forms.Label();
            this.txbMonthlyPandL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbCurrentMonth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_AnnualSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_YearSelector)).BeginInit();
            this.SuspendLayout();
            // 
            // gbCurrentMonth
            // 
            this.gbCurrentMonth.AutoSize = true;
            this.gbCurrentMonth.Controls.Add(this.DGV_AnnualSummary);
            this.gbCurrentMonth.Controls.Add(this.NUD_YearSelector);
            this.gbCurrentMonth.Controls.Add(this.txbYTDPandL);
            this.gbCurrentMonth.Controls.Add(this.label2);
            this.gbCurrentMonth.Controls.Add(this.labelMMMYYY);
            this.gbCurrentMonth.Controls.Add(this.txbMonthlyPandL);
            this.gbCurrentMonth.Controls.Add(this.label1);
            this.gbCurrentMonth.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbCurrentMonth.Location = new System.Drawing.Point(0, 0);
            this.gbCurrentMonth.Name = "gbCurrentMonth";
            this.gbCurrentMonth.Size = new System.Drawing.Size(833, 114);
            this.gbCurrentMonth.TabIndex = 0;
            this.gbCurrentMonth.TabStop = false;
            this.gbCurrentMonth.Text = "Current Month and Annual Realized P&&L";
            // 
            // DGV_AnnualSummary
            // 
            this.DGV_AnnualSummary.AllowUserToAddRows = false;
            this.DGV_AnnualSummary.AllowUserToDeleteRows = false;
            this.DGV_AnnualSummary.AllowUserToResizeRows = false;
            this.DGV_AnnualSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV_AnnualSummary.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV_AnnualSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV_AnnualSummary.Location = new System.Drawing.Point(62, 17);
            this.DGV_AnnualSummary.MultiSelect = false;
            this.DGV_AnnualSummary.Name = "DGV_AnnualSummary";
            this.DGV_AnnualSummary.ReadOnly = true;
            this.DGV_AnnualSummary.RowHeadersVisible = false;
            this.DGV_AnnualSummary.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.DGV_AnnualSummary.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV_AnnualSummary.RowTemplate.Height = 25;
            this.DGV_AnnualSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DGV_AnnualSummary.ShowEditingIcon = false;
            this.DGV_AnnualSummary.Size = new System.Drawing.Size(768, 47);
            this.DGV_AnnualSummary.TabIndex = 35;
            this.DGV_AnnualSummary.Click += new System.EventHandler(this.DGV_AnnualSummary_Click);
            // 
            // NUD_YearSelector
            // 
            this.NUD_YearSelector.Location = new System.Drawing.Point(3, 17);
            this.NUD_YearSelector.Name = "NUD_YearSelector";
            this.NUD_YearSelector.Size = new System.Drawing.Size(53, 23);
            this.NUD_YearSelector.TabIndex = 34;
            this.NUD_YearSelector.ValueChanged += new System.EventHandler(this.NUD_YearSelector_ValueChanged);
            // 
            // txbYTDPandL
            // 
            this.txbYTDPandL.BackColor = System.Drawing.SystemColors.Control;
            this.txbYTDPandL.Location = new System.Drawing.Point(62, 69);
            this.txbYTDPandL.Margin = new System.Windows.Forms.Padding(2);
            this.txbYTDPandL.Name = "txbYTDPandL";
            this.txbYTDPandL.ReadOnly = true;
            this.txbYTDPandL.Size = new System.Drawing.Size(70, 23);
            this.txbYTDPandL.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 70);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "YTD P&&L";
            // 
            // labelMMMYYY
            // 
            this.labelMMMYYY.AutoSize = true;
            this.labelMMMYYY.Location = new System.Drawing.Point(185, 70);
            this.labelMMMYYY.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMMMYYY.Name = "labelMMMYYY";
            this.labelMMMYYY.Size = new System.Drawing.Size(65, 15);
            this.labelMMMYYY.TabIndex = 2;
            this.labelMMMYYY.Text = "MonthYear";
            // 
            // txbMonthlyPandL
            // 
            this.txbMonthlyPandL.Location = new System.Drawing.Point(297, 69);
            this.txbMonthlyPandL.Name = "txbMonthlyPandL";
            this.txbMonthlyPandL.ReadOnly = true;
            this.txbMonthlyPandL.Size = new System.Drawing.Size(70, 23);
            this.txbMonthlyPandL.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "P && L";
            // 
            // CurrentMonthView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbCurrentMonth);
            this.Name = "CurrentMonthView";
            this.Size = new System.Drawing.Size(833, 122);
            this.gbCurrentMonth.ResumeLayout(false);
            this.gbCurrentMonth.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_AnnualSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_YearSelector)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCurrentMonth;
        private System.Windows.Forms.TextBox txbMonthlyPandL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelMMMYYY;
        private System.Windows.Forms.TextBox txbYTDPandL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NUD_YearSelector;
        private System.Windows.Forms.DataGridView DGV_AnnualSummary;
    }
}
