﻿using MPSS.Common.Model;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MPSS.Controls.Views
{
    public partial class SelectDBFileView : UserControl
    {
        #region Public Properties
        [Browsable(true)]
        [Category("Action")]
        [Description("Invoked DBFile selection is changed")]
        public event EventHandler<EventArgs> DBFileChanged;
        #endregion Public Properties

        #region Private Properties
        private readonly OpenFileDialog ofd;    // OpenFileDialog instance
        private int currentDBFileIdx = 0;       // Currently selected DBFile index
        private string defaultDBFilename = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\MPSS.db";
        #endregion Private Properties

        #region Constructors
        public SelectDBFileView()
        {
            InitializeComponent();

            // Let's initialize the DBFileView
            SelectDBFileComboBox.Items.Clear();
            SelectDBFileComboBox.Items.Add("Select file...");

            //var defaultDBFilename = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\MPSS.db";
            currentDBFileIdx = SelectDBFileComboBox.Items.Add(new DBFile(defaultDBFilename));

            ofd = new OpenFileDialog()
            {
                Title = "Select MPSS DB File",
                Filter = "MPSS DB files (*.db)|*.db"
            };

            // Set the initial selection
            SelectDBFileComboBox.SelectedIndex = currentDBFileIdx;
            txbDBFileName.Text = defaultDBFilename;
        }

        public void SetDBFile(DBFile currentDatabaseFile)
        {
            defaultDBFilename = currentDatabaseFile.GetFileName();

            SelectDBFileComboBox.Items.Clear();
            SelectDBFileComboBox.Items.Add("Select file...");

            //var defaultDBFilename = $@"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\MPSS.db";
            currentDBFileIdx = SelectDBFileComboBox.Items.Add(new DBFile(defaultDBFilename));

            // Set the initial selection
            SelectDBFileComboBox.SelectedIndex = currentDBFileIdx;

            txbDBFileName.Text = defaultDBFilename;
        }
        #endregion Constructors

        #region Public Methods
        public DBFile GetDBFile()
        {
            return (DBFile)SelectDBFileComboBox.SelectedItem;
        }
        #endregion Public Methods

        #region Private Methods
        private static void RaiseEvent(EventHandler<EventArgs> eventHandler, object sender, EventArgs e)
        {
            eventHandler?.Invoke(sender, e);
        }

        private void SelectDBFileComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            if (cb.SelectedItem.ToString() == "Select file...")
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    currentDBFileIdx = cb.Items.Add(new DBFile(ofd.FileName));
                    cb.SelectedIndex = currentDBFileIdx;
                }
                else
                {
                    cb.SelectedIndex = currentDBFileIdx;
                }
            }
            else
            {
                currentDBFileIdx = cb.SelectedIndex;
                RaiseEvent(DBFileChanged, cb.SelectedItem, e);
            }
            txbDBFileName.Text = (cb.SelectedItem as DBFile).GetFileName();
        }
        #endregion Private Methods
    }
}
