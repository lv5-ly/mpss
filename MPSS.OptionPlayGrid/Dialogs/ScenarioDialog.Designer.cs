﻿
namespace MPSS.Controls.Dialogs
{
    partial class ScenarioDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScenarioTabControl = new System.Windows.Forms.TabControl();
            this.CCRollTabPage = new System.Windows.Forms.TabPage();
            this.CCRollScenarioPanel = new MPSS.Controls.Components.CCRollScenario();
            this.CCRollGrid = new MPSS.Controls.Components.KeyAndValueGrid();
            this.ClosingOutToSellStockTabPage = new System.Windows.Forms.TabPage();
            this.BBCCToSellScenarioPanel = new MPSS.Controls.Components.BuyBackCCToSellScenario();
            this.BBAndSellGrid = new MPSS.Controls.Components.KeyAndValueGrid();
            this.SellStockTabPage = new System.Windows.Forms.TabPage();
            this.SellStockScenarioPanel = new MPSS.Controls.Components.SellStockScenario();
            this.SellStockGrid = new MPSS.Controls.Components.KeyAndValueGrid();
            this.TInfo = new MPSS.Controls.Components.TransactionInfo();
            this.ScenarioTabControl.SuspendLayout();
            this.CCRollTabPage.SuspendLayout();
            this.ClosingOutToSellStockTabPage.SuspendLayout();
            this.SellStockTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // ScenarioTabControl
            // 
            this.ScenarioTabControl.Controls.Add(this.CCRollTabPage);
            this.ScenarioTabControl.Controls.Add(this.ClosingOutToSellStockTabPage);
            this.ScenarioTabControl.Controls.Add(this.SellStockTabPage);
            this.ScenarioTabControl.Location = new System.Drawing.Point(14, 10);
            this.ScenarioTabControl.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.ScenarioTabControl.Name = "ScenarioTabControl";
            this.ScenarioTabControl.SelectedIndex = 0;
            this.ScenarioTabControl.Size = new System.Drawing.Size(919, 794);
            this.ScenarioTabControl.TabIndex = 0;
            // 
            // CCRollTabPage
            // 
            this.CCRollTabPage.Controls.Add(this.CCRollScenarioPanel);
            this.CCRollTabPage.Controls.Add(this.CCRollGrid);
            this.CCRollTabPage.Location = new System.Drawing.Point(4, 39);
            this.CCRollTabPage.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.CCRollTabPage.Name = "CCRollTabPage";
            this.CCRollTabPage.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.CCRollTabPage.Size = new System.Drawing.Size(911, 751);
            this.CCRollTabPage.TabIndex = 0;
            this.CCRollTabPage.Text = "CC Roll";
            this.CCRollTabPage.UseVisualStyleBackColor = true;
            // 
            // CCRollScenarioPanel
            // 
            this.CCRollScenarioPanel.Location = new System.Drawing.Point(7, 0);
            this.CCRollScenarioPanel.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.CCRollScenarioPanel.Name = "CCRollScenarioPanel";
            this.CCRollScenarioPanel.Size = new System.Drawing.Size(897, 200);
            this.CCRollScenarioPanel.TabIndex = 2;
            this.CCRollScenarioPanel.ScenarioResultReady += new System.EventHandler<MPSS.Common.Model.CCRollScenarioResultEventArgs>(this.CCRollScenarioPanel_ScenarioResultReady);
            // 
            // CCRollGrid
            // 
            this.CCRollGrid.Location = new System.Drawing.Point(5, 212);
            this.CCRollGrid.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.CCRollGrid.Name = "CCRollGrid";
            this.CCRollGrid.Size = new System.Drawing.Size(895, 520);
            this.CCRollGrid.TabIndex = 1;
            this.CCRollGrid.Title = "Key && Value Grid";
            // 
            // ClosingOutToSellStockTabPage
            // 
            this.ClosingOutToSellStockTabPage.Controls.Add(this.BBCCToSellScenarioPanel);
            this.ClosingOutToSellStockTabPage.Controls.Add(this.BBAndSellGrid);
            this.ClosingOutToSellStockTabPage.Location = new System.Drawing.Point(4, 39);
            this.ClosingOutToSellStockTabPage.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.ClosingOutToSellStockTabPage.Name = "ClosingOutToSellStockTabPage";
            this.ClosingOutToSellStockTabPage.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.ClosingOutToSellStockTabPage.Size = new System.Drawing.Size(911, 751);
            this.ClosingOutToSellStockTabPage.TabIndex = 1;
            this.ClosingOutToSellStockTabPage.Text = "+CC && Sell Stock";
            this.ClosingOutToSellStockTabPage.UseVisualStyleBackColor = true;
            // 
            // BBCCToSellScenarioPanel
            // 
            this.BBCCToSellScenarioPanel.Location = new System.Drawing.Point(7, 0);
            this.BBCCToSellScenarioPanel.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.BBCCToSellScenarioPanel.Name = "BBCCToSellScenarioPanel";
            this.BBCCToSellScenarioPanel.Size = new System.Drawing.Size(900, 202);
            this.BBCCToSellScenarioPanel.TabIndex = 3;
            this.BBCCToSellScenarioPanel.ScenarioResultReady += new System.EventHandler<MPSS.Common.Model.BBCCToSellScenarioResultEventArgs>(this.BuyBackccToSellScenario_ScenarioResultReady);
            // 
            // BBAndSellGrid
            // 
            this.BBAndSellGrid.Location = new System.Drawing.Point(5, 212);
            this.BBAndSellGrid.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.BBAndSellGrid.Name = "BBAndSellGrid";
            this.BBAndSellGrid.Size = new System.Drawing.Size(895, 520);
            this.BBAndSellGrid.TabIndex = 2;
            this.BBAndSellGrid.Title = "Key && Value Grid";
            // 
            // SellStockTabPage
            // 
            this.SellStockTabPage.Controls.Add(this.SellStockScenarioPanel);
            this.SellStockTabPage.Controls.Add(this.SellStockGrid);
            this.SellStockTabPage.Location = new System.Drawing.Point(4, 39);
            this.SellStockTabPage.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.SellStockTabPage.Name = "SellStockTabPage";
            this.SellStockTabPage.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.SellStockTabPage.Size = new System.Drawing.Size(911, 751);
            this.SellStockTabPage.TabIndex = 2;
            this.SellStockTabPage.Text = "Sell Stock";
            this.SellStockTabPage.UseVisualStyleBackColor = true;
            // 
            // SellStockScenarioPanel
            // 
            this.SellStockScenarioPanel.Location = new System.Drawing.Point(7, 0);
            this.SellStockScenarioPanel.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.SellStockScenarioPanel.Name = "SellStockScenarioPanel";
            this.SellStockScenarioPanel.Size = new System.Drawing.Size(897, 208);
            this.SellStockScenarioPanel.TabIndex = 5;
            this.SellStockScenarioPanel.ScenarioResultReady += new System.EventHandler<MPSS.Common.Model.SellStockScenarioResultEventArgs>(this.SellStockScenario_ScenarioResultReady);
            // 
            // SellStockGrid
            // 
            this.SellStockGrid.Location = new System.Drawing.Point(5, 212);
            this.SellStockGrid.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.SellStockGrid.Name = "SellStockGrid";
            this.SellStockGrid.Size = new System.Drawing.Size(895, 520);
            this.SellStockGrid.TabIndex = 4;
            this.SellStockGrid.Title = "Key && Value Grid";
            // 
            // TInfo
            // 
            this.TInfo.Location = new System.Drawing.Point(19, 808);
            this.TInfo.Margin = new System.Windows.Forms.Padding(9, 12, 9, 12);
            this.TInfo.Name = "TInfo";
            this.TInfo.Size = new System.Drawing.Size(917, 210);
            this.TInfo.TabIndex = 1;
            this.TInfo.CloseButtonClick += new System.EventHandler<MPSS.Common.Model.StockTransactionEventArgs>(this.TInfo_CloseButtonClick);
            this.TInfo.Load += new System.EventHandler(this.TInfo_Load);
            // 
            // ScenarioDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(927, 1030);
            this.Controls.Add(this.TInfo);
            this.Controls.Add(this.ScenarioTabControl);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "ScenarioDialog";
            this.Text = "Scenarios";
            this.Load += new System.EventHandler(this.ScenarioDialog_Load);
            this.ScenarioTabControl.ResumeLayout(false);
            this.CCRollTabPage.ResumeLayout(false);
            this.ClosingOutToSellStockTabPage.ResumeLayout(false);
            this.SellStockTabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl ScenarioTabControl;
        private System.Windows.Forms.TabPage CCRollTabPage;
        private System.Windows.Forms.TabPage ClosingOutToSellStockTabPage;
        private System.Windows.Forms.TabPage SellStockTabPage;
        //private System.Windows.Forms.Button u;
        //private System.Windows.Forms.Button Calculate;
        private Components.KeyAndValueGrid CCRollGrid;
        private Components.KeyAndValueGrid BBAndSellGrid;
        private Components.KeyAndValueGrid SellStockGrid;
        private Components.TransactionInfo TInfo;
        private Components.CCRollScenario CCRollScenarioPanel;
        private Components.BuyBackCCToSellScenario BBCCToSellScenarioPanel;
        private Components.SellStockScenario SellStockScenarioPanel;
    }
}
