﻿
namespace MPSS.OptionPlayControl
{
    partial class CloseOptionPlayDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CloseOptionPlayDialog));
            this.panel2 = new System.Windows.Forms.Panel();
            this.OKButton = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblPrice = new System.Windows.Forms.Label();
            this.Price = new System.Windows.Forms.TextBox();
            this.CloseDialogLabel = new System.Windows.Forms.Label();
            this.CloseOutRadioButton = new System.Windows.Forms.RadioButton();
            this.ExpiredRadioButton = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AssignedRadioButton = new System.Windows.Forms.RadioButton();
            this.NumberOfContracts = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.ParentTransactionDate = new System.Windows.Forms.Label();
            this.lblFee = new System.Windows.Forms.Label();
            this.Fee = new System.Windows.Forms.TextBox();
            this.lblContracts = new System.Windows.Forms.Label();
            this.TransactionDatePickerLabel = new System.Windows.Forms.Label();
            this.TransactionDatePicker = new System.Windows.Forms.DateTimePicker();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfContracts)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.OKButton);
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Location = new System.Drawing.Point(12, 164);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(543, 65);
            this.panel2.TabIndex = 7;
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.BackColor = System.Drawing.SystemColors.Control;
            this.OKButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OKButton.Location = new System.Drawing.Point(425, 9);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(106, 42);
            this.OKButton.TabIndex = 13;
            this.OKButton.Text = "Confirm";
            this.OKButton.UseVisualStyleBackColor = false;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(14, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(106, 42);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BTNCancel_Click);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(97, 68);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(92, 15);
            this.lblPrice.TabIndex = 2;
            this.lblPrice.Text = "Price (per share)";
            // 
            // Price
            // 
            this.Price.Location = new System.Drawing.Point(97, 86);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(92, 23);
            this.Price.TabIndex = 8;
            this.Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // CloseDialogLabel
            // 
            this.CloseDialogLabel.AutoSize = true;
            this.CloseDialogLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CloseDialogLabel.Location = new System.Drawing.Point(13, 10);
            this.CloseDialogLabel.Name = "CloseDialogLabel";
            this.CloseDialogLabel.Size = new System.Drawing.Size(156, 21);
            this.CloseDialogLabel.TabIndex = 10;
            this.CloseDialogLabel.Text = "Call/Put Close Dialog";
            // 
            // CloseOutRadioButton
            // 
            this.CloseOutRadioButton.AutoSize = true;
            this.CloseOutRadioButton.Location = new System.Drawing.Point(97, 43);
            this.CloseOutRadioButton.Name = "CloseOutRadioButton";
            this.CloseOutRadioButton.Size = new System.Drawing.Size(79, 19);
            this.CloseOutRadioButton.TabIndex = 13;
            this.CloseOutRadioButton.TabStop = true;
            this.CloseOutRadioButton.Text = "Close-Out";
            this.CloseOutRadioButton.UseVisualStyleBackColor = true;
            this.CloseOutRadioButton.CheckedChanged += new System.EventHandler(this.RBClosedOut_CheckedChanged);
            // 
            // ExpiredRadioButton
            // 
            this.ExpiredRadioButton.AutoSize = true;
            this.ExpiredRadioButton.Location = new System.Drawing.Point(14, 43);
            this.ExpiredRadioButton.Name = "ExpiredRadioButton";
            this.ExpiredRadioButton.Size = new System.Drawing.Size(64, 19);
            this.ExpiredRadioButton.TabIndex = 14;
            this.ExpiredRadioButton.TabStop = true;
            this.ExpiredRadioButton.Text = "Expired";
            this.ExpiredRadioButton.UseVisualStyleBackColor = true;
            this.ExpiredRadioButton.CheckedChanged += new System.EventHandler(this.RBExpired_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.AssignedRadioButton);
            this.panel1.Controls.Add(this.NumberOfContracts);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.ParentTransactionDate);
            this.panel1.Controls.Add(this.lblFee);
            this.panel1.Controls.Add(this.Fee);
            this.panel1.Controls.Add(this.lblContracts);
            this.panel1.Controls.Add(this.TransactionDatePickerLabel);
            this.panel1.Controls.Add(this.TransactionDatePicker);
            this.panel1.Controls.Add(this.ExpiredRadioButton);
            this.panel1.Controls.Add(this.CloseOutRadioButton);
            this.panel1.Controls.Add(this.CloseDialogLabel);
            this.panel1.Controls.Add(this.Price);
            this.panel1.Controls.Add(this.lblPrice);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(543, 146);
            this.panel1.TabIndex = 6;
            // 
            // AssignedRadioButton
            // 
            this.AssignedRadioButton.AutoSize = true;
            this.AssignedRadioButton.Location = new System.Drawing.Point(195, 43);
            this.AssignedRadioButton.Name = "AssignedRadioButton";
            this.AssignedRadioButton.Size = new System.Drawing.Size(73, 19);
            this.AssignedRadioButton.TabIndex = 24;
            this.AssignedRadioButton.TabStop = true;
            this.AssignedRadioButton.Text = "Assigned";
            this.AssignedRadioButton.UseVisualStyleBackColor = true;
            this.AssignedRadioButton.CheckedChanged += new System.EventHandler(this.RBAssigned_CheckedChanged);
            // 
            // NumberOfContracts
            // 
            this.NumberOfContracts.Location = new System.Drawing.Point(14, 87);
            this.NumberOfContracts.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumberOfContracts.Name = "NumberOfContracts";
            this.NumberOfContracts.Size = new System.Drawing.Size(77, 23);
            this.NumberOfContracts.TabIndex = 23;
            this.NumberOfContracts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumberOfContracts.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(320, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Transaction Date:";
            // 
            // ParentTransactionDate
            // 
            this.ParentTransactionDate.AutoSize = true;
            this.ParentTransactionDate.Cursor = System.Windows.Forms.Cursors.Default;
            this.ParentTransactionDate.Location = new System.Drawing.Point(425, 10);
            this.ParentTransactionDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ParentTransactionDate.Name = "ParentTransactionDate";
            this.ParentTransactionDate.Size = new System.Drawing.Size(83, 15);
            this.ParentTransactionDate.TabIndex = 21;
            this.ParentTransactionDate.Text = "MM/DD/YYYY";
            // 
            // lblFee
            // 
            this.lblFee.AutoSize = true;
            this.lblFee.Location = new System.Drawing.Point(195, 68);
            this.lblFee.Name = "lblFee";
            this.lblFee.Size = new System.Drawing.Size(25, 15);
            this.lblFee.TabIndex = 20;
            this.lblFee.Text = "Fee";
            // 
            // Fee
            // 
            this.Fee.Location = new System.Drawing.Point(195, 86);
            this.Fee.Name = "Fee";
            this.Fee.Size = new System.Drawing.Size(77, 23);
            this.Fee.TabIndex = 19;
            this.Fee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblContracts
            // 
            this.lblContracts.AutoSize = true;
            this.lblContracts.Location = new System.Drawing.Point(14, 68);
            this.lblContracts.Name = "lblContracts";
            this.lblContracts.Size = new System.Drawing.Size(58, 15);
            this.lblContracts.TabIndex = 17;
            this.lblContracts.Text = "Contracts";
            // 
            // TransactionDatePickerLabel
            // 
            this.TransactionDatePickerLabel.AutoSize = true;
            this.TransactionDatePickerLabel.Location = new System.Drawing.Point(323, 65);
            this.TransactionDatePickerLabel.Name = "TransactionDatePickerLabel";
            this.TransactionDatePickerLabel.Size = new System.Drawing.Size(173, 15);
            this.TransactionDatePickerLabel.TabIndex = 16;
            this.TransactionDatePickerLabel.Text = "Enter new closed date if desired";
            // 
            // TransactionDatePicker
            // 
            this.TransactionDatePicker.Location = new System.Drawing.Point(320, 83);
            this.TransactionDatePicker.Name = "TransactionDatePicker";
            this.TransactionDatePicker.Size = new System.Drawing.Size(211, 23);
            this.TransactionDatePicker.TabIndex = 15;
            // 
            // CloseOptionPlayDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 239);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CloseOptionPlayDialog";
            this.Text = "Close Option Play Dialog";
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfContracts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox Price;
        private System.Windows.Forms.Label CloseDialogLabel;
        private System.Windows.Forms.RadioButton CloseOutRadioButton;
        private System.Windows.Forms.RadioButton ExpiredRadioButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label TransactionDatePickerLabel;
        private System.Windows.Forms.DateTimePicker TransactionDatePicker;
        private System.Windows.Forms.Label lblFee;
        private System.Windows.Forms.TextBox Fee;
        private System.Windows.Forms.Label lblContracts;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ParentTransactionDate;
        private System.Windows.Forms.NumericUpDown NumberOfContracts;
        private System.Windows.Forms.RadioButton AssignedRadioButton;
    }
}