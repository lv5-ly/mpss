﻿using MPSS.Common.ViewModel;
using MPSS.ViewModel;
using System;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace MPSS.Controls.Dialogs
{
    /// <summary>
    /// This control shows a list of stock transaction with the Expiration Date in the 
    /// specified month.
    /// </summary>
    public partial class RealizedPLReportDialog : Form
    {
        #region Private Properties
        private readonly int MonthIndex = 0;
        private readonly BindingSource MonthlyTransactionBindingSource = new();
        private readonly BindingList<MonthlyTransactionViewModel> MonthlyTransactionBindingList = new();
        private readonly int SelectedYear = DateTime.Now.Year;
        //private readonly List<string> HideColumns = new()
        //{
        //    "TransactionId",
        //    "ExpiredDate",
        //    "TransactionAction",
        //    "Quantity",
        //    "StrikePrice",
        //    "Fee",
        //    "Active",
        //    "Hide",
        //    "Status",
        //    "ParentTransactionId",
        //    "IsActiveCoveredCall",
        //    "Stock",
        //    "TransactionType"
        //};
        //private readonly List<KeyValuePair<string, string>> ColumnHeaders = new()
        //{
        //    { new KeyValuePair<string, string>("AssetSymbol", "Holding") },
        //    { new KeyValuePair<string, string>("TransactionDate", "On") },
        //    { new KeyValuePair<string, string>("DisplayQuantity", "Shares") },
        //    { new KeyValuePair<string, string>("Message", "Notes") },
        //    { new KeyValuePair<string, string>("TransactionDisplayType", "Transaction Type") }
        //};
        private readonly StockAssetViewModel ViewModel;
        private readonly DateTimeFormatInfo DTFI = new();
        #endregion

        #region Constructors
        public RealizedPLReportDialog()
        {
            InitializeComponent();

            // Prepare the transaction data grid
            BindDataSource();
            FormatTransactionGrid(TransactionDataGridView);
        }

        /// <summary>
        /// This is to specify the month index (0 - 11) to invoke
        ///     this control as a pop-up dialog, and to list
        ///     transactions in that month
        /// </summary>
        /// <param name="i">Month index, 0 - 11</param>        
        public RealizedPLReportDialog(int i) : this()
        {
            MonthIndex = i;
        }

        public RealizedPLReportDialog(int i, StockAssetViewModel viewModel) : this(i)
        {
            ViewModel = viewModel;
        }

        public RealizedPLReportDialog(int i, StockAssetViewModel viewModel, int year) : this(i)
        {
            ViewModel = viewModel;
            SelectedYear = year;
        }
        #endregion

        #region Private Methods
        //private void SetColumnHeaderName()
        //{
        //    foreach (var ch in ColumnHeaders)
        //    {
        //        TransactionDataGridView.Columns[ch.Key].HeaderText = ch.Value;
        //    }
        //}

        //private void FormatTransactionDataGridView()
        //{
        //    foreach (var columnName in HideColumns)
        //    {
        //        TransactionDataGridView.Columns[columnName].Visible = false;
        //    }
        //}

        private void BindDataSource()
        {
            MonthlyTransactionBindingSource.DataSource = MonthlyTransactionBindingList;
            TransactionDataGridView.DataSource = MonthlyTransactionBindingSource;
        }
        private static void FormatTransactionGrid(DataGridView dgv)
        {
            dgv.Columns["Symbol"].Visible = true;
            dgv.Columns["Symbol"].HeaderText = "Symbol";
            dgv.Columns["Symbol"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["Symbol"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["Symbol"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgv.Columns["Symbol"].Width = 60;

            dgv.Columns["TransactionDate"].HeaderText = "On";
            dgv.Columns["TransactionDate"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["TransactionDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["TransactionDate"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgv.Columns["TransactionDate"].Width = 80;

            dgv.Columns["TransactionDisplayType"].HeaderText = "Transaction Type";
            dgv.Columns["TransactionDisplayType"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["TransactionDisplayType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            dgv.Columns["DisplayQuantity"].HeaderText = "Shares";
            dgv.Columns["DisplayQuantity"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["DisplayQuantity"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["DisplayQuantity"].DefaultCellStyle.Format = "N0";
            dgv.Columns["DisplayQuantity"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            dgv.Columns["DisplayQuantity"].Width = 80;

            dgv.Columns["Price"].HeaderText = "Price";
            dgv.Columns["Price"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgv.Columns["Price"].DefaultCellStyle.Format = "c";

            dgv.Columns["Notes"].HeaderText = "Notes";
            dgv.Columns["Notes"].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv.Columns["Notes"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
        #endregion

        #region Event Handlers
        private void Button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void MonthlyStocksTransactionList_Load(object sender, EventArgs e)
        {
            var monthNumber = MonthIndex + 1;
            //this.Text = $"{DTFI.GetMonthName(monthNumber)} {DateTime.Today.Year} - Realized P&L Report";
            this.Text = $"{DTFI.GetMonthName(monthNumber)} {SelectedYear} - Realized P&L Report";
            if (ViewModel != null)
            {
                var allTransactions = ViewModel
                    .AllStocks
                    .SelectMany(s => s.Transactions)
                    .Where(t => t.ExpiredDate.Month == monthNumber && t.ExpiredDate.Year == SelectedYear && t.Inactive)
                    .Select(t => new MonthlyTransactionViewModel(t));
                foreach (var t in allTransactions)
                {
                    MonthlyTransactionBindingList.Add(t);
                }

                StatusLabel.Text = $"{MonthlyTransactionBindingList.Count} closed/inactive transaction(s) for the month of {DTFI.GetMonthName(monthNumber)}, {SelectedYear}";
            }
            else
            {
                StatusLabel.Text = $"No data";
            }
        }
        #endregion


    }
}
