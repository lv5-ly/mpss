﻿using MPSS.Common.Model;
using System.Windows.Forms;

namespace MPSS.OptionPlayControl
{
    /// <summary>
    /// This class is base class, from which the CloseShareDialog and 
    /// CloseOptionPlayDialog are derived
    /// </summary>
    public class CloseTransactionDialog : Form
    {
        public AssetTransaction Transaction { get; set; }
    }
}
