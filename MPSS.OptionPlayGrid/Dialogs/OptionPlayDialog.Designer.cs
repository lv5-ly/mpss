﻿
namespace MPSS.OptionPlayControl
{
    partial class OptionPlayDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionPlayDialog));
            this.panel2 = new System.Windows.Forms.Panel();
            this.OkButton = new System.Windows.Forms.Button();
            this.DialogCancelButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SpreadTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.SpreadOptionPut = new System.Windows.Forms.RadioButton();
            this.SpreadOptionCall = new System.Windows.Forms.RadioButton();
            this.SecondaryStrikePriceTextBox = new System.Windows.Forms.TextBox();
            this.SecondaryStrikePriceLabel = new System.Windows.Forms.Label();
            this.ContractsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.TransactionTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.DebitSpreadRadioButton = new System.Windows.Forms.RadioButton();
            this.CreditSpreadRadioButton = new System.Windows.Forms.RadioButton();
            this.PutRadioButton = new System.Windows.Forms.RadioButton();
            this.CallRadioButton = new System.Windows.Forms.RadioButton();
            this.TransactionActionGroupBox = new System.Windows.Forms.GroupBox();
            this.SoldRadioButton = new System.Windows.Forms.RadioButton();
            this.BoughtRadioButton = new System.Windows.Forms.RadioButton();
            this.ExpirationDateLabel = new System.Windows.Forms.Label();
            this.ExpiredDatePicker = new System.Windows.Forms.DateTimePicker();
            this.StrikePriceTextBox = new System.Windows.Forms.TextBox();
            this.PrimaryStrikePriceLabel = new System.Windows.Forms.Label();
            this.FeeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TransactionDateLabel = new System.Windows.Forms.Label();
            this.TransactionDatePicker = new System.Windows.Forms.DateTimePicker();
            this.PriceTextBox = new System.Windows.Forms.TextBox();
            this.SymbolTextBox = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.ContractsLabel = new System.Windows.Forms.Label();
            this.SymbolLabel = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SpreadTypeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContractsNumericUpDown)).BeginInit();
            this.TransactionTypeGroupBox.SuspendLayout();
            this.TransactionActionGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.OkButton);
            this.panel2.Controls.Add(this.DialogCancelButton);
            this.panel2.Location = new System.Drawing.Point(12, 252);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(641, 65);
            this.panel2.TabIndex = 3;
            // 
            // OkButton
            // 
            this.OkButton.BackColor = System.Drawing.SystemColors.Control;
            this.OkButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.OkButton.Location = new System.Drawing.Point(516, 9);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(106, 42);
            this.OkButton.TabIndex = 13;
            this.OkButton.Text = "OK";
            this.OkButton.UseVisualStyleBackColor = false;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // DialogCancelButton
            // 
            this.DialogCancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.DialogCancelButton.Location = new System.Drawing.Point(14, 9);
            this.DialogCancelButton.Name = "DialogCancelButton";
            this.DialogCancelButton.Size = new System.Drawing.Size(106, 42);
            this.DialogCancelButton.TabIndex = 12;
            this.DialogCancelButton.Text = "Cancel";
            this.DialogCancelButton.UseVisualStyleBackColor = true;
            this.DialogCancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.SpreadTypeGroupBox);
            this.panel1.Controls.Add(this.SecondaryStrikePriceTextBox);
            this.panel1.Controls.Add(this.SecondaryStrikePriceLabel);
            this.panel1.Controls.Add(this.ContractsNumericUpDown);
            this.panel1.Controls.Add(this.TransactionTypeGroupBox);
            this.panel1.Controls.Add(this.TransactionActionGroupBox);
            this.panel1.Controls.Add(this.ExpirationDateLabel);
            this.panel1.Controls.Add(this.ExpiredDatePicker);
            this.panel1.Controls.Add(this.StrikePriceTextBox);
            this.panel1.Controls.Add(this.PrimaryStrikePriceLabel);
            this.panel1.Controls.Add(this.FeeTextBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.TransactionDateLabel);
            this.panel1.Controls.Add(this.TransactionDatePicker);
            this.panel1.Controls.Add(this.PriceTextBox);
            this.panel1.Controls.Add(this.SymbolTextBox);
            this.panel1.Controls.Add(this.lblPrice);
            this.panel1.Controls.Add(this.ContractsLabel);
            this.panel1.Controls.Add(this.SymbolLabel);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(641, 234);
            this.panel1.TabIndex = 2;
            // 
            // SpreadTypeGroupBox
            // 
            this.SpreadTypeGroupBox.Controls.Add(this.SpreadOptionPut);
            this.SpreadTypeGroupBox.Controls.Add(this.SpreadOptionCall);
            this.SpreadTypeGroupBox.Location = new System.Drawing.Point(184, 14);
            this.SpreadTypeGroupBox.Name = "SpreadTypeGroupBox";
            this.SpreadTypeGroupBox.Size = new System.Drawing.Size(101, 89);
            this.SpreadTypeGroupBox.TabIndex = 21;
            this.SpreadTypeGroupBox.TabStop = false;
            this.SpreadTypeGroupBox.Text = "Spread Type";
            // 
            // SpreadOptionPut
            // 
            this.SpreadOptionPut.AutoSize = true;
            this.SpreadOptionPut.Location = new System.Drawing.Point(11, 58);
            this.SpreadOptionPut.Name = "SpreadOptionPut";
            this.SpreadOptionPut.Size = new System.Drawing.Size(82, 19);
            this.SpreadOptionPut.TabIndex = 2;
            this.SpreadOptionPut.Text = "Spread Put";
            this.SpreadOptionPut.UseVisualStyleBackColor = true;
            // 
            // SpreadOptionCall
            // 
            this.SpreadOptionCall.AutoSize = true;
            this.SpreadOptionCall.Checked = true;
            this.SpreadOptionCall.Location = new System.Drawing.Point(11, 21);
            this.SpreadOptionCall.Name = "SpreadOptionCall";
            this.SpreadOptionCall.Size = new System.Drawing.Size(84, 19);
            this.SpreadOptionCall.TabIndex = 1;
            this.SpreadOptionCall.TabStop = true;
            this.SpreadOptionCall.Text = "Spread Call";
            this.SpreadOptionCall.UseVisualStyleBackColor = true;
            // 
            // SecondaryStrikePriceTextBox
            // 
            this.SecondaryStrikePriceTextBox.Location = new System.Drawing.Point(184, 189);
            this.SecondaryStrikePriceTextBox.Name = "SecondaryStrikePriceTextBox";
            this.SecondaryStrikePriceTextBox.Size = new System.Drawing.Size(77, 23);
            this.SecondaryStrikePriceTextBox.TabIndex = 19;
            this.SecondaryStrikePriceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.SecondaryStrikePriceTextBox.Visible = false;
            // 
            // SecondaryStrikePriceLabel
            // 
            this.SecondaryStrikePriceLabel.AutoSize = true;
            this.SecondaryStrikePriceLabel.Location = new System.Drawing.Point(184, 171);
            this.SecondaryStrikePriceLabel.Name = "SecondaryStrikePriceLabel";
            this.SecondaryStrikePriceLabel.Size = new System.Drawing.Size(131, 15);
            this.SecondaryStrikePriceLabel.TabIndex = 20;
            this.SecondaryStrikePriceLabel.Text = "Strike Price (Secondary)";
            this.SecondaryStrikePriceLabel.Visible = false;
            // 
            // ContractsNumericUpDown
            // 
            this.ContractsNumericUpDown.Location = new System.Drawing.Point(112, 140);
            this.ContractsNumericUpDown.Name = "ContractsNumericUpDown";
            this.ContractsNumericUpDown.Size = new System.Drawing.Size(77, 23);
            this.ContractsNumericUpDown.TabIndex = 7;
            this.ContractsNumericUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ContractsNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // TransactionTypeGroupBox
            // 
            this.TransactionTypeGroupBox.Controls.Add(this.DebitSpreadRadioButton);
            this.TransactionTypeGroupBox.Controls.Add(this.CreditSpreadRadioButton);
            this.TransactionTypeGroupBox.Controls.Add(this.PutRadioButton);
            this.TransactionTypeGroupBox.Controls.Add(this.CallRadioButton);
            this.TransactionTypeGroupBox.Location = new System.Drawing.Point(14, 14);
            this.TransactionTypeGroupBox.Name = "TransactionTypeGroupBox";
            this.TransactionTypeGroupBox.Size = new System.Drawing.Size(164, 89);
            this.TransactionTypeGroupBox.TabIndex = 18;
            this.TransactionTypeGroupBox.TabStop = false;
            this.TransactionTypeGroupBox.Text = "Transaction Type";
            // 
            // DebitSpreadRadioButton
            // 
            this.DebitSpreadRadioButton.AutoSize = true;
            this.DebitSpreadRadioButton.Location = new System.Drawing.Point(61, 58);
            this.DebitSpreadRadioButton.Name = "DebitSpreadRadioButton";
            this.DebitSpreadRadioButton.Size = new System.Drawing.Size(92, 19);
            this.DebitSpreadRadioButton.TabIndex = 6;
            this.DebitSpreadRadioButton.Text = "Debit Spread";
            this.DebitSpreadRadioButton.UseVisualStyleBackColor = true;
            this.DebitSpreadRadioButton.CheckedChanged += new System.EventHandler(this.DebitSpreadRadioButton_CheckedChanged);
            // 
            // CreditSpreadRadioButton
            // 
            this.CreditSpreadRadioButton.AutoSize = true;
            this.CreditSpreadRadioButton.Location = new System.Drawing.Point(61, 21);
            this.CreditSpreadRadioButton.Name = "CreditSpreadRadioButton";
            this.CreditSpreadRadioButton.Size = new System.Drawing.Size(96, 19);
            this.CreditSpreadRadioButton.TabIndex = 5;
            this.CreditSpreadRadioButton.Text = "Credit Spread";
            this.CreditSpreadRadioButton.UseVisualStyleBackColor = true;
            this.CreditSpreadRadioButton.CheckedChanged += new System.EventHandler(this.CreditSpreadRadioButton_CheckedChanged);
            // 
            // PutRadioButton
            // 
            this.PutRadioButton.AutoSize = true;
            this.PutRadioButton.Location = new System.Drawing.Point(6, 58);
            this.PutRadioButton.Name = "PutRadioButton";
            this.PutRadioButton.Size = new System.Drawing.Size(43, 19);
            this.PutRadioButton.TabIndex = 4;
            this.PutRadioButton.Text = "Put";
            this.PutRadioButton.UseVisualStyleBackColor = true;
            this.PutRadioButton.CheckedChanged += new System.EventHandler(this.PutRadioButton_CheckedChanged);
            // 
            // CallRadioButton
            // 
            this.CallRadioButton.AutoSize = true;
            this.CallRadioButton.Checked = true;
            this.CallRadioButton.Location = new System.Drawing.Point(6, 22);
            this.CallRadioButton.Name = "CallRadioButton";
            this.CallRadioButton.Size = new System.Drawing.Size(45, 19);
            this.CallRadioButton.TabIndex = 3;
            this.CallRadioButton.TabStop = true;
            this.CallRadioButton.Text = "Call";
            this.CallRadioButton.UseVisualStyleBackColor = true;
            this.CallRadioButton.CheckedChanged += new System.EventHandler(this.CallRadioButton_CheckedChanged);
            // 
            // TransactionActionGroupBox
            // 
            this.TransactionActionGroupBox.Controls.Add(this.SoldRadioButton);
            this.TransactionActionGroupBox.Controls.Add(this.BoughtRadioButton);
            this.TransactionActionGroupBox.Location = new System.Drawing.Point(305, 14);
            this.TransactionActionGroupBox.Name = "TransactionActionGroupBox";
            this.TransactionActionGroupBox.Size = new System.Drawing.Size(87, 89);
            this.TransactionActionGroupBox.TabIndex = 17;
            this.TransactionActionGroupBox.TabStop = false;
            this.TransactionActionGroupBox.Text = "Action";
            // 
            // SoldRadioButton
            // 
            this.SoldRadioButton.AutoSize = true;
            this.SoldRadioButton.Checked = true;
            this.SoldRadioButton.Location = new System.Drawing.Point(11, 58);
            this.SoldRadioButton.Name = "SoldRadioButton";
            this.SoldRadioButton.Size = new System.Drawing.Size(48, 19);
            this.SoldRadioButton.TabIndex = 2;
            this.SoldRadioButton.TabStop = true;
            this.SoldRadioButton.Text = "Sold";
            this.SoldRadioButton.UseVisualStyleBackColor = true;
            // 
            // BoughtRadioButton
            // 
            this.BoughtRadioButton.AutoSize = true;
            this.BoughtRadioButton.Location = new System.Drawing.Point(11, 21);
            this.BoughtRadioButton.Name = "BoughtRadioButton";
            this.BoughtRadioButton.Size = new System.Drawing.Size(64, 19);
            this.BoughtRadioButton.TabIndex = 1;
            this.BoughtRadioButton.Text = "Bought";
            this.BoughtRadioButton.UseVisualStyleBackColor = true;
            // 
            // ExpirationDateLabel
            // 
            this.ExpirationDateLabel.AutoSize = true;
            this.ExpirationDateLabel.Location = new System.Drawing.Point(398, 122);
            this.ExpirationDateLabel.Name = "ExpirationDateLabel";
            this.ExpirationDateLabel.Size = new System.Drawing.Size(86, 15);
            this.ExpirationDateLabel.TabIndex = 15;
            this.ExpirationDateLabel.Text = "Expiration Date";
            // 
            // ExpiredDatePicker
            // 
            this.ExpiredDatePicker.Location = new System.Drawing.Point(398, 140);
            this.ExpiredDatePicker.Name = "ExpiredDatePicker";
            this.ExpiredDatePicker.Size = new System.Drawing.Size(224, 23);
            this.ExpiredDatePicker.TabIndex = 11;
            // 
            // StrikePriceTextBox
            // 
            this.StrikePriceTextBox.Location = new System.Drawing.Point(14, 189);
            this.StrikePriceTextBox.Name = "StrikePriceTextBox";
            this.StrikePriceTextBox.Size = new System.Drawing.Size(77, 23);
            this.StrikePriceTextBox.TabIndex = 10;
            this.StrikePriceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PrimaryStrikePriceLabel
            // 
            this.PrimaryStrikePriceLabel.AutoSize = true;
            this.PrimaryStrikePriceLabel.Location = new System.Drawing.Point(14, 171);
            this.PrimaryStrikePriceLabel.Name = "PrimaryStrikePriceLabel";
            this.PrimaryStrikePriceLabel.Size = new System.Drawing.Size(117, 15);
            this.PrimaryStrikePriceLabel.TabIndex = 12;
            this.PrimaryStrikePriceLabel.Text = "Strike Price (Primary)";
            // 
            // FeeTextBox
            // 
            this.FeeTextBox.Location = new System.Drawing.Point(315, 139);
            this.FeeTextBox.Name = "FeeTextBox";
            this.FeeTextBox.Size = new System.Drawing.Size(77, 23);
            this.FeeTextBox.TabIndex = 9;
            this.FeeTextBox.Text = "$0.00";
            this.FeeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(315, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Fee";
            // 
            // TransactionDateLabel
            // 
            this.TransactionDateLabel.AutoSize = true;
            this.TransactionDateLabel.Location = new System.Drawing.Point(398, 14);
            this.TransactionDateLabel.Name = "TransactionDateLabel";
            this.TransactionDateLabel.Size = new System.Drawing.Size(95, 15);
            this.TransactionDateLabel.TabIndex = 7;
            this.TransactionDateLabel.Text = "Transaction Date";
            // 
            // TransactionDatePicker
            // 
            this.TransactionDatePicker.Location = new System.Drawing.Point(398, 32);
            this.TransactionDatePicker.Name = "TransactionDatePicker";
            this.TransactionDatePicker.Size = new System.Drawing.Size(224, 23);
            this.TransactionDatePicker.TabIndex = 5;
            // 
            // PriceTextBox
            // 
            this.PriceTextBox.Location = new System.Drawing.Point(206, 140);
            this.PriceTextBox.Name = "PriceTextBox";
            this.PriceTextBox.Size = new System.Drawing.Size(92, 23);
            this.PriceTextBox.TabIndex = 8;
            this.PriceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // SymbolTextBox
            // 
            this.SymbolTextBox.Location = new System.Drawing.Point(14, 140);
            this.SymbolTextBox.Name = "SymbolTextBox";
            this.SymbolTextBox.Size = new System.Drawing.Size(77, 23);
            this.SymbolTextBox.TabIndex = 6;
            this.SymbolTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(206, 122);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(92, 15);
            this.lblPrice.TabIndex = 2;
            this.lblPrice.Text = "Price (per share)";
            // 
            // ContractsLabel
            // 
            this.ContractsLabel.AutoSize = true;
            this.ContractsLabel.Location = new System.Drawing.Point(112, 122);
            this.ContractsLabel.Name = "ContractsLabel";
            this.ContractsLabel.Size = new System.Drawing.Size(58, 15);
            this.ContractsLabel.TabIndex = 1;
            this.ContractsLabel.Text = "Contracts";
            // 
            // SymbolLabel
            // 
            this.SymbolLabel.AutoSize = true;
            this.SymbolLabel.Location = new System.Drawing.Point(14, 122);
            this.SymbolLabel.Name = "SymbolLabel";
            this.SymbolLabel.Size = new System.Drawing.Size(47, 15);
            this.SymbolLabel.TabIndex = 0;
            this.SymbolLabel.Text = "Symbol";
            // 
            // OptionPlayDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 329);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OptionPlayDialog";
            this.Text = "Option Play";
            this.Load += new System.EventHandler(this.OptionPlayDialog_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.SpreadTypeGroupBox.ResumeLayout(false);
            this.SpreadTypeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContractsNumericUpDown)).EndInit();
            this.TransactionTypeGroupBox.ResumeLayout(false);
            this.TransactionTypeGroupBox.PerformLayout();
            this.TransactionActionGroupBox.ResumeLayout(false);
            this.TransactionActionGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button DialogCancelButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox TransactionActionGroupBox;
        private System.Windows.Forms.RadioButton SoldRadioButton;
        private System.Windows.Forms.RadioButton BoughtRadioButton;
        private System.Windows.Forms.Label ExpirationDateLabel;
        private System.Windows.Forms.DateTimePicker ExpiredDatePicker;
        private System.Windows.Forms.TextBox StrikePriceTextBox;
        private System.Windows.Forms.Label PrimaryStrikePriceLabel;
        private System.Windows.Forms.TextBox FeeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label TransactionDateLabel;
        private System.Windows.Forms.DateTimePicker TransactionDatePicker;
        private System.Windows.Forms.TextBox PriceTextBox;
        private System.Windows.Forms.TextBox SymbolTextBox;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label ContractsLabel;
        private System.Windows.Forms.Label SymbolLabel;
        private System.Windows.Forms.GroupBox TransactionTypeGroupBox;
        private System.Windows.Forms.RadioButton PutRadioButton;
        private System.Windows.Forms.RadioButton CallRadioButton;
        private System.Windows.Forms.NumericUpDown ContractsNumericUpDown;
        private System.Windows.Forms.TextBox SecondaryStrikePriceTextBox;
        private System.Windows.Forms.Label SecondaryStrikePriceLabel;
        private System.Windows.Forms.RadioButton DebitSpreadRadioButton;
        private System.Windows.Forms.RadioButton CreditSpreadRadioButton;
        private System.Windows.Forms.GroupBox SpreadTypeGroupBox;
        private System.Windows.Forms.RadioButton SpreadOptionPut;
        private System.Windows.Forms.RadioButton SpreadOptionCall;
    }
}