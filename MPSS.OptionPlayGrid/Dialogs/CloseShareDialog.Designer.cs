﻿
namespace MPSS.OptionPlayControl
{
    partial class CloseShareDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CloseShareDialog));
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCloseDialogTransDate = new System.Windows.Forms.Label();
            this.lblFee = new System.Windows.Forms.Label();
            this.Fee = new System.Windows.Forms.TextBox();
            this.lblTransactionAction = new System.Windows.Forms.Label();
            this.Shares = new System.Windows.Forms.TextBox();
            this.lblBuyDate = new System.Windows.Forms.Label();
            this.transactionDateTime = new System.Windows.Forms.DateTimePicker();
            this.Price = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblContracts = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.btnOk);
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Location = new System.Drawing.Point(21, 238);
            this.panel2.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(926, 126);
            this.panel2.TabIndex = 5;
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.SystemColors.Control;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOk.Location = new System.Drawing.Point(723, 18);
            this.btnOk.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(182, 84);
            this.btnOk.TabIndex = 13;
            this.btnOk.Text = "Confirm";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.BTNOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Location = new System.Drawing.Point(24, 18);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(182, 84);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BTNCancel_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblCloseDialogTransDate);
            this.panel1.Controls.Add(this.lblFee);
            this.panel1.Controls.Add(this.Fee);
            this.panel1.Controls.Add(this.lblTransactionAction);
            this.panel1.Controls.Add(this.Shares);
            this.panel1.Controls.Add(this.lblBuyDate);
            this.panel1.Controls.Add(this.transactionDateTime);
            this.panel1.Controls.Add(this.Price);
            this.panel1.Controls.Add(this.lblPrice);
            this.panel1.Controls.Add(this.lblContracts);
            this.panel1.Location = new System.Drawing.Point(21, 24);
            this.panel1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(926, 198);
            this.panel1.TabIndex = 4;
            // 
            // lblCloseDialogTransDate
            // 
            this.lblCloseDialogTransDate.AutoSize = true;
            this.lblCloseDialogTransDate.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblCloseDialogTransDate.Location = new System.Drawing.Point(697, 27);
            this.lblCloseDialogTransDate.Name = "lblCloseDialogTransDate";
            this.lblCloseDialogTransDate.Size = new System.Drawing.Size(145, 30);
            this.lblCloseDialogTransDate.TabIndex = 13;
            this.lblCloseDialogTransDate.Text = "MM/DD/YYYY";
            // 
            // lblFee
            // 
            this.lblFee.AutoSize = true;
            this.lblFee.Location = new System.Drawing.Point(334, 84);
            this.lblFee.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFee.Name = "lblFee";
            this.lblFee.Size = new System.Drawing.Size(45, 30);
            this.lblFee.TabIndex = 12;
            this.lblFee.Text = "Fee";
            // 
            // Fee
            // 
            this.Fee.Location = new System.Drawing.Point(334, 120);
            this.Fee.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Fee.Name = "Fee";
            this.Fee.Size = new System.Drawing.Size(129, 35);
            this.Fee.TabIndex = 11;
            this.Fee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTransactionAction
            // 
            this.lblTransactionAction.AutoSize = true;
            this.lblTransactionAction.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblTransactionAction.Location = new System.Drawing.Point(22, 20);
            this.lblTransactionAction.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTransactionAction.Name = "lblTransactionAction";
            this.lblTransactionAction.Size = new System.Drawing.Size(152, 38);
            this.lblTransactionAction.TabIndex = 10;
            this.lblTransactionAction.Text = "Bought on:";
            // 
            // Shares
            // 
            this.Shares.Location = new System.Drawing.Point(24, 120);
            this.Shares.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Shares.Name = "Shares";
            this.Shares.Size = new System.Drawing.Size(129, 35);
            this.Shares.TabIndex = 9;
            this.Shares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBuyDate
            // 
            this.lblBuyDate.AutoSize = true;
            this.lblBuyDate.Location = new System.Drawing.Point(518, 78);
            this.lblBuyDate.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblBuyDate.Name = "lblBuyDate";
            this.lblBuyDate.Size = new System.Drawing.Size(308, 30);
            this.lblBuyDate.TabIndex = 7;
            this.lblBuyDate.Text = "Enter new closed date if desired";
            // 
            // transactionDateTime
            // 
            this.transactionDateTime.Location = new System.Drawing.Point(518, 114);
            this.transactionDateTime.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.transactionDateTime.Name = "transactionDateTime";
            this.transactionDateTime.Size = new System.Drawing.Size(381, 35);
            this.transactionDateTime.TabIndex = 5;
            // 
            // Price
            // 
            this.Price.Location = new System.Drawing.Point(166, 120);
            this.Price.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(155, 35);
            this.Price.TabIndex = 8;
            this.Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(166, 84);
            this.lblPrice.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(162, 30);
            this.lblPrice.TabIndex = 2;
            this.lblPrice.Text = "Price (per share)";
            // 
            // lblContracts
            // 
            this.lblContracts.AutoSize = true;
            this.lblContracts.Location = new System.Drawing.Point(24, 84);
            this.lblContracts.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblContracts.Name = "lblContracts";
            this.lblContracts.Size = new System.Drawing.Size(74, 30);
            this.lblContracts.TabIndex = 1;
            this.lblContracts.Text = "Shares";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(518, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 30);
            this.label1.TabIndex = 14;
            this.label1.Text = "Transaction Date:";
            // 
            // CloseShareDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 392);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "CloseShareDialog";
            this.Text = "Close Share Dialog";
            this.Load += new System.EventHandler(this.CloseShareDialog_Load);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox Shares;
        private System.Windows.Forms.Label lblBuyDate;
        private System.Windows.Forms.DateTimePicker transactionDateTime;
        private System.Windows.Forms.TextBox Price;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblContracts;
        private System.Windows.Forms.Label lblTransactionAction;
        private System.Windows.Forms.Label lblFee;
        private System.Windows.Forms.TextBox Fee;
        private System.Windows.Forms.Label lblCloseDialogTransDate;
        private System.Windows.Forms.Label label1;
    }
}