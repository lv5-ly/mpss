﻿
namespace MPSS
{
    partial class BuyShareDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyShareDialog));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblExpiredDate = new System.Windows.Forms.Label();
            this.expiredDatePicker = new System.Windows.Forms.DateTimePicker();
            this.tbStrikePrice = new System.Windows.Forms.TextBox();
            this.lblStrikePrice = new System.Windows.Forms.Label();
            this.Total = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.Fee = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBuyDate = new System.Windows.Forms.Label();
            this.transactionDateTime = new System.Windows.Forms.DateTimePicker();
            this.Price = new System.Windows.Forms.TextBox();
            this.NumberOfShares = new System.Windows.Forms.TextBox();
            this.Symbol = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblShares = new System.Windows.Forms.Label();
            this.lblSymbol = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblExpiredDate);
            this.panel1.Controls.Add(this.expiredDatePicker);
            this.panel1.Controls.Add(this.tbStrikePrice);
            this.panel1.Controls.Add(this.lblStrikePrice);
            this.panel1.Controls.Add(this.Total);
            this.panel1.Controls.Add(this.lblTotal);
            this.panel1.Controls.Add(this.Fee);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblBuyDate);
            this.panel1.Controls.Add(this.transactionDateTime);
            this.panel1.Controls.Add(this.Price);
            this.panel1.Controls.Add(this.NumberOfShares);
            this.panel1.Controls.Add(this.Symbol);
            this.panel1.Controls.Add(this.lblPrice);
            this.panel1.Controls.Add(this.lblShares);
            this.panel1.Controls.Add(this.lblSymbol);
            this.panel1.Location = new System.Drawing.Point(12, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(557, 214);
            this.panel1.TabIndex = 0;
            // 
            // lblExpiredDate
            // 
            this.lblExpiredDate.AutoSize = true;
            this.lblExpiredDate.Location = new System.Drawing.Point(311, 116);
            this.lblExpiredDate.Name = "lblExpiredDate";
            this.lblExpiredDate.Size = new System.Drawing.Size(72, 15);
            this.lblExpiredDate.TabIndex = 15;
            this.lblExpiredDate.Text = "Expired Date";
            // 
            // expiredDatePicker
            // 
            this.expiredDatePicker.Location = new System.Drawing.Point(311, 134);
            this.expiredDatePicker.Name = "expiredDatePicker";
            this.expiredDatePicker.Size = new System.Drawing.Size(224, 23);
            this.expiredDatePicker.TabIndex = 14;
            // 
            // tbStrikePrice
            // 
            this.tbStrikePrice.Location = new System.Drawing.Point(208, 134);
            this.tbStrikePrice.Name = "tbStrikePrice";
            this.tbStrikePrice.Size = new System.Drawing.Size(90, 23);
            this.tbStrikePrice.TabIndex = 13;
            this.tbStrikePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblStrikePrice
            // 
            this.lblStrikePrice.AutoSize = true;
            this.lblStrikePrice.Location = new System.Drawing.Point(208, 116);
            this.lblStrikePrice.Name = "lblStrikePrice";
            this.lblStrikePrice.Size = new System.Drawing.Size(65, 15);
            this.lblStrikePrice.TabIndex = 12;
            this.lblStrikePrice.Text = "Strike Price";
            // 
            // Total
            // 
            this.Total.Location = new System.Drawing.Point(390, 182);
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Size = new System.Drawing.Size(145, 23);
            this.Total.TabIndex = 11;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(390, 164);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(33, 15);
            this.lblTotal.TabIndex = 10;
            this.lblTotal.Text = "Total";
            // 
            // Fee
            // 
            this.Fee.Location = new System.Drawing.Point(208, 82);
            this.Fee.Name = "Fee";
            this.Fee.Size = new System.Drawing.Size(90, 23);
            this.Fee.TabIndex = 9;
            this.Fee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(208, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Fee";
            // 
            // lblBuyDate
            // 
            this.lblBuyDate.AutoSize = true;
            this.lblBuyDate.Location = new System.Drawing.Point(311, 14);
            this.lblBuyDate.Name = "lblBuyDate";
            this.lblBuyDate.Size = new System.Drawing.Size(95, 15);
            this.lblBuyDate.TabIndex = 7;
            this.lblBuyDate.Text = "Transaction Date";
            // 
            // transactionDateTime
            // 
            this.transactionDateTime.Location = new System.Drawing.Point(311, 32);
            this.transactionDateTime.Name = "transactionDateTime";
            this.transactionDateTime.Size = new System.Drawing.Size(224, 23);
            this.transactionDateTime.TabIndex = 6;
            // 
            // Price
            // 
            this.Price.Location = new System.Drawing.Point(112, 82);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(90, 23);
            this.Price.TabIndex = 5;
            this.Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // NumberOfShares
            // 
            this.NumberOfShares.Location = new System.Drawing.Point(16, 82);
            this.NumberOfShares.Name = "NumberOfShares";
            this.NumberOfShares.Size = new System.Drawing.Size(90, 23);
            this.NumberOfShares.TabIndex = 4;
            this.NumberOfShares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Symbol
            // 
            this.Symbol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Symbol.Location = new System.Drawing.Point(16, 35);
            this.Symbol.Name = "Symbol";
            this.Symbol.Size = new System.Drawing.Size(90, 23);
            this.Symbol.TabIndex = 3;
            this.Symbol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(112, 64);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(92, 15);
            this.lblPrice.TabIndex = 2;
            this.lblPrice.Text = "Price (per share)";
            // 
            // lblShares
            // 
            this.lblShares.AutoSize = true;
            this.lblShares.Location = new System.Drawing.Point(16, 64);
            this.lblShares.Name = "lblShares";
            this.lblShares.Size = new System.Drawing.Size(41, 15);
            this.lblShares.TabIndex = 1;
            this.lblShares.Text = "Shares";
            // 
            // lblSymbol
            // 
            this.lblSymbol.AutoSize = true;
            this.lblSymbol.Location = new System.Drawing.Point(16, 17);
            this.lblSymbol.Name = "lblSymbol";
            this.lblSymbol.Size = new System.Drawing.Size(47, 15);
            this.lblSymbol.TabIndex = 0;
            this.lblSymbol.Text = "Symbol";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnOk);
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Location = new System.Drawing.Point(12, 230);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(557, 63);
            this.panel2.TabIndex = 1;
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.SystemColors.Info;
            this.btnOk.Location = new System.Drawing.Point(429, 9);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(106, 42);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Bought";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(14, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(71, 42);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // BuyDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 304);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BuyDialog";
            this.Text = "Transaction";
            this.Load += new System.EventHandler(this.BuyDialog_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblBuyDate;
        private System.Windows.Forms.DateTimePicker transactionDateTime;
        private System.Windows.Forms.TextBox Price;
        private System.Windows.Forms.TextBox NumberOfShares;
        private System.Windows.Forms.TextBox Symbol;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblShares;
        private System.Windows.Forms.Label lblSymbol;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox Total;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox Fee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbStrikePrice;
        private System.Windows.Forms.Label lblStrikePrice;
        private System.Windows.Forms.Label lblExpiredDate;
        private System.Windows.Forms.DateTimePicker expiredDatePicker;
    }
}