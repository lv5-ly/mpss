﻿using MPSS.Common.Model;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace MPSS.OptionPlayControl
{
    public partial class CloseShareDialog : CloseTransactionDialog
    {
        public CloseShareDialog()
        {
            InitializeComponent();
        }

        public CloseShareDialog(AssetTransaction parentTransaction)
        {
            Transaction = new AssetTransaction(parentTransaction)
            {
                // Override few properties
                ParentTransactionId = parentTransaction.TransactionId,
                Active = false, // We need to set this as inactive
                Hide = false,
                Status = AssetTransactionStatus.Open,

                // Since this is a close dialog, we want this 
                //  transaction to be opposite of the parent
                //  transaction
                TransactionAction = parentTransaction.OppositeAction(),
            };

            InitializeComponent();
            lblCloseDialogTransDate.Text = parentTransaction.TransactionDate.ToString("d");
        }

        private void CloseShareDialog_Load(object sender, EventArgs e)
        {
            lblTransactionAction.Text = $"{Transaction.TransactionAction.Description().ToUpper()} - {Transaction.AssetSymbol}";

            Shares.Text = Transaction.Quantity.ToString();
            Price.Text = Transaction.Price.ToString("c");
            Fee.Text = Transaction.Fee.ToString("c");

        }

        private void BTNOk_Click(object sender, EventArgs e)
        {
            // Let's transfer what user enters into 
            //  the transaction object, so that we can
            //  pass it back to the caller
            Transaction.ExpiredDate = transactionDateTime.Value;
            Transaction.Quantity = int.TryParse(Shares.Text, out int q) ? q : 1;
            Transaction.Price = decimal.TryParse(Price.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal p) ? p : 0M;
            Transaction.Fee = decimal.TryParse(Fee.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal f) ? f : 0M;

            DialogResult = DialogResult.OK;
            Close();
        }

        private void BTNCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
