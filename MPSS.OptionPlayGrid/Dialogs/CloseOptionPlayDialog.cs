﻿using MPSS.Common.Model;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace MPSS.OptionPlayControl
{
    public partial class CloseOptionPlayDialog : CloseTransactionDialog
    {
        public CloseOptionPlayDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The main form uses this constructor, and it passes 
        /// the selected transaction, which this object is going
        /// to use, or based on, to create the correct behavior
        /// </summary>
        /// <param name="parentTransaction">The selected/parent transaction</param>
        public CloseOptionPlayDialog(AssetTransaction parentTransaction)
        {
            Transaction = new AssetTransaction(parentTransaction)
            {
                // Override few properties
                TransactionDate = DateTime.Now,
                ParentTransactionId = parentTransaction.TransactionId,
                Active = false,
                Hide = false,
                Status = AssetTransactionStatus.Expired,

                // Since this is a close dialog, we want this 
                //  transaction to be opposite of the parent
                //  transaction
                TransactionAction = parentTransaction.OppositeAction(),
                ExpiredDate = parentTransaction.ExpiredDate
            };
            InitializeComponent();
            ParentTransactionDate.Text = $"{parentTransaction.TransactionDate:d}";
            CloseDialogLabel.Text = $"Close {Transaction.AssetSymbol} - {parentTransaction.TransactionAction.Description().ToUpper()} - {parentTransaction.TransactionType.Description().ToUpper()}";
            CloseOutRadioButton.Checked = true;
            Price.Text = $"{Transaction.Price:C}";
            TransactionDatePicker.Value = Transaction.TransactionDate;

            NumberOfContracts.Maximum = NumberOfContracts.Value = (Transaction.Quantity / 100);

            Fee.Text = $"{Transaction.Fee:C}";

            // If the transaction is -CC or -CP, then enable the Assigned radio button
            AssignedRadioButton.Enabled = parentTransaction.TransactionAction == AssetTransactionAction.Sold
                && (parentTransaction.TransactionType == AssetTransactionType.CoveredCall ||
                parentTransaction.TransactionType == AssetTransactionType.CoveredPut);
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            if (CloseOutRadioButton.Checked)
            {
                var price = decimal.TryParse(Price.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal p) ? p : 0M;
                var fee = decimal.TryParse(Fee.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal f) ? f : 0M;
                var contracts = (int)(NumberOfContracts.Value * 100);
                Transaction.CloseOut(price, fee, contracts, TransactionDatePicker.Value);
            }
            else if (ExpiredRadioButton.Checked)
            {
                Transaction.Expire();
            }
            else if (AssignedRadioButton.Checked)
            {
                Transaction.Assigned();
            }

            DialogResult = DialogResult.OK;
            Close();
        }

        private void RBExpired_CheckedChanged(object sender, EventArgs e)
        {
            /// Enable the Price and Date Picker only if Expired RadioButton is not checked
            NumberOfContracts.Enabled = TransactionDatePicker.Enabled = Price.Enabled = !ExpiredRadioButton.Checked;
        }

        private void RBClosedOut_CheckedChanged(object sender, EventArgs e)
        {
            /// Enable the Price and Date Picker only if Expired RadioButton is not checked
            NumberOfContracts.Enabled = TransactionDatePicker.Enabled = Price.Enabled = CloseOutRadioButton.Checked;
        }

        private void RBAssigned_CheckedChanged(object sender, EventArgs e)
        {
            /// Enable the Price and Date Picker only if Expired RadioButton is not checked
            NumberOfContracts.Enabled = TransactionDatePicker.Enabled = Price.Enabled = !AssignedRadioButton.Checked;
        }

        private void BTNCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
