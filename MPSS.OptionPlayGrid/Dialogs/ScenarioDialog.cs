﻿using MPSS.Common.Model;
using System;
using System.Linq;
using System.Windows.Forms;

namespace MPSS.Controls.Dialogs
{
    public partial class ScenarioDialog : Form
    {
        #region Private Static Methods
        private static void SetValue(string key, string v, SortableBindingList<KeyAndValue> bindingList)
        {
            var d = bindingList.FirstOrDefault(i => i.Key == key);
            d.Value = v;
        }

        #endregion

        #region Private Properties
        private readonly ScenarioCalculator calculator;

        private readonly SortableBindingList<KeyAndValue> CCRollData = new()
        {
            new KeyAndValue(ScenarioCalculator.C_CCROLL_REPL, string.Empty),
            new KeyAndValue(ScenarioCalculator.C_CCROLL_CSA, string.Empty),
            new KeyAndValue(ScenarioCalculator.C_CCROLL_RPL, string.Empty),
            new KeyAndValue(ScenarioCalculator.N_CCROLL_NECC, string.Empty),
            new KeyAndValue(ScenarioCalculator.N_CCROLL_NSA, string.Empty),
            new KeyAndValue(ScenarioCalculator.N_CCROLL_RPL, string.Empty),
        };

        private readonly SortableBindingList<KeyAndValue> BBCCToSellData = new()
        {
            new KeyAndValue(ScenarioCalculator.BBCCTOSELL_REPL, string.Empty),
            new KeyAndValue(ScenarioCalculator.BBCCTOSELL_CSA, string.Empty),
            new KeyAndValue(ScenarioCalculator.BBCCTOSELL_RPL, string.Empty)
        };

        private readonly SortableBindingList<KeyAndValue> SellStockData = new()
        {
            new KeyAndValue(ScenarioCalculator.SELLSTOCK_OP_RPL, string.Empty),
            new KeyAndValue(ScenarioCalculator.SELLSTOCK_AP_RPL, string.Empty)
        };
        #endregion

        #region Constructors
        public ScenarioDialog()
        {
            InitializeComponent();
        }

        public ScenarioDialog(AssetTransaction currentAssetTransaction)
        {
            //this.currentAssetTransaction = currentAssetTransaction;
            calculator = new ScenarioCalculator(currentAssetTransaction);
            InitializeComponent();

            CCRollScenarioPanel.SetCalculator(calculator);
            CCRollGrid.Title = "Profit && Loss";
            CCRollGrid.SetHeaders("Scenario", "Total");

            BBCCToSellScenarioPanel.SetCalculator(calculator);
            BBAndSellGrid.Title = "Profit && Loss";
            BBAndSellGrid.SetHeaders("Scenario", "Total");

            SellStockScenarioPanel.SetCalculator(calculator);
            SellStockGrid.Title = "Profit && Loss";
            SellStockGrid.SetHeaders("Scenario", "Total");

            TInfo.Transaction = currentAssetTransaction;
            TInfo.Title = "Option Play Transaction";
        }
        #endregion

        #region Event Handlers
        private void TInfo_CloseButtonClick(object sender, StockTransactionEventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void ScenarioDialog_Load(object sender, EventArgs e)
        {
            CCRollGrid.SetBindingData(CCRollData);
            BBAndSellGrid.SetBindingData(BBCCToSellData);
            SellStockGrid.SetBindingData(SellStockData);
        }


        /// <summary>
        /// When the user clicks on the Calculate button in the CC Roll Scenario panel
        /// uses scenario-calculator to calculate the result, and then raise this event, to
        /// indicate that the result is ready. This is the handler of that event,
        /// to extract the result from the event arguments and populate the result 
        /// grid
        /// </summary>
        /// <param name="sender">The object which sends this event</param>
        /// <param name="e">The event arguments</param>
        private void CCRollScenarioPanel_ScenarioResultReady(object sender, CCRollScenarioResultEventArgs e)
        {
            foreach (var r in e.Values)
            {
                SetValue(r.Key, $"{r.Value:C}", CCRollData);
            }
            CCRollGrid.Refresh();
        }

        private void BuyBackccToSellScenario_ScenarioResultReady(object sender, BBCCToSellScenarioResultEventArgs e)
        {
            foreach (var r in e.Values)
            {
                SetValue(r.Key, $"{r.Value:C}", BBCCToSellData);
            }
            BBAndSellGrid.Refresh();
        }

        private void SellStockScenario_ScenarioResultReady(object sender, SellStockScenarioResultEventArgs e)
        {
            foreach (var r in e.Values)
            {
                SetValue(r.Key, $"{r.Value:C}", SellStockData);
            }
            SellStockGrid.Refresh();
        }
        #endregion

        private void TInfo_Load(object sender, EventArgs e)
        {

        }
    }
}
