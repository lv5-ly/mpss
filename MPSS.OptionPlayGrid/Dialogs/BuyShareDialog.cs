﻿using MPSS.Common.Model;
using MPSS.ViewModel;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace MPSS
{
    public partial class BuyShareDialog : Form
    {
        private readonly StockAssetViewModel viewModel;
        private readonly StockAsset selectedAsset;
        private readonly AssetTransactionType transactionType = AssetTransactionType.Share;
        private readonly AssetTransactionAction transactionAction = AssetTransactionAction.Bought;

        public AssetTransaction CurrentSelectedTransaction { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="vm">View Model instance</param>
        /// <param name="selected">Selected item</param>
        public BuyShareDialog(StockAssetViewModel vm, StockAsset selected,
            AssetTransactionAction tAction = AssetTransactionAction.Bought,
            AssetTransactionType tType = AssetTransactionType.Share)
        {
            InitializeComponent();

            viewModel = vm;
            selectedAsset = selected;

            transactionAction = tAction;
            transactionType = tType;
        }

        public BuyShareDialog(StockAssetViewModel vm, AssetTransactionAction tAction)
        {
            InitializeComponent();

            viewModel = vm;

            CurrentSelectedTransaction = vm.CurrentSelectedTransaction;
            transactionAction = tAction;
            transactionType = CurrentSelectedTransaction.TransactionType;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (transactionAction == AssetTransactionAction.Edit)
            {
                CurrentSelectedTransaction.Price =
                    decimal.TryParse(Price.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal p) ?
                    p : CurrentSelectedTransaction.Price;
                CurrentSelectedTransaction.UpdateQuantity(NumberOfShares.Text);
                CurrentSelectedTransaction.TransactionDate = transactionDateTime.Value;
                CurrentSelectedTransaction.ExpiredDate = expiredDatePicker.Value;
                CurrentSelectedTransaction.Fee =
                    decimal.TryParse(Fee.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal f) ?
                    f : CurrentSelectedTransaction.Fee;
                CurrentSelectedTransaction.StrikePrice =
                    decimal.TryParse(tbStrikePrice.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal sp) ?
                    sp : CurrentSelectedTransaction.StrikePrice;

                // We want to clear out the note/message, because this one is an one-legged
                //  transaction, thus it should not have any notes
                if (CurrentSelectedTransaction.Message.StartsWith("Spread play"))
                {
                    CurrentSelectedTransaction.Message = string.Empty;
                }

                viewModel.UpdateTransaction(CurrentSelectedTransaction);
            }
            else if (transactionAction == AssetTransactionAction.Bought ||
                transactionAction == AssetTransactionAction.Sold)
            {
                var t = new AssetTransaction()
                {
                    AssetSymbol = Symbol.Text,
                    TransactionAction = transactionAction,
                    Price = decimal.TryParse(Price.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal p) ?
                    p : 0M,
                    Quantity = int.TryParse(
                        NumberOfShares.Text, out int q) ? q : 1,
                    TransactionDate = transactionDateTime.Value,
                    TransactionType = transactionType,
                    Active = true,
                    ExpiredDate = expiredDatePicker.Value,
                    Fee = decimal.TryParse(Fee.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal f) ?
                    f : 0M,
                    Hide = false,
                    Message = string.Empty,
                    StrikePrice = decimal.TryParse(tbStrikePrice.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal sp) ?
                    sp : 0M,
                    Status = AssetTransactionStatus.Open
                };
                viewModel.AddTransaction(t);
            }
            else if (transactionAction == AssetTransactionAction.Assigned &&
                CurrentSelectedTransaction != null &&
                CurrentSelectedTransaction.TransactionType == AssetTransactionType.CoveredCall)
            {
                CurrentSelectedTransaction.Status = AssetTransactionStatus.Assigned;
                viewModel.UpdateTransaction(CurrentSelectedTransaction);

                var t = new AssetTransaction()
                {
                    AssetSymbol = Symbol.Text,
                    TransactionAction = AssetTransactionAction.Sold,
                    Price = decimal.TryParse(Price.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal p) ?
                    p : CurrentSelectedTransaction.Price,
                    Quantity = int.TryParse(
                        NumberOfShares.Text, out int q) ? q : 1,
                    TransactionDate = transactionDateTime.Value,
                    TransactionType = AssetTransactionType.Share,
                    Active = true,
                    ExpiredDate = expiredDatePicker.Value,
                    Fee = decimal.TryParse(Fee.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal f) ?
                    f : CurrentSelectedTransaction.Fee,
                    Hide = false,
                    Message = string.Empty,
                    StrikePrice = 0M,
                    Status = AssetTransactionStatus.Open,

                    // We want this new transaction to reference/point back
                    //  to the original/parent transaction.
                    ParentTransactionId = CurrentSelectedTransaction.TransactionId
                };
                viewModel.AddTransaction(t);
            }


            DialogResult = DialogResult.OK;
            Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void BuyDialog_Load(object sender, EventArgs e)
        {
            if (selectedAsset != null)
            {
                this.Symbol.Text = selectedAsset.Symbol;
            }

            if (transactionAction == AssetTransactionAction.Bought ||
                transactionAction == AssetTransactionAction.Sold)
            {
                if (transactionType == AssetTransactionType.Share)
                {
                    lblExpiredDate.Hide();
                    expiredDatePicker.Hide();

                    lblStrikePrice.Hide();
                    tbStrikePrice.Hide();

                    btnOk.Text = (transactionAction == AssetTransactionAction.Bought) ? "Bought" : "Sold";
                }
                else if (transactionType == AssetTransactionType.CoveredCall ||
                    transactionType == AssetTransactionType.CoveredPut)
                {
                    lblExpiredDate.Show();
                    expiredDatePicker.Show();

                    lblStrikePrice.Show();
                    tbStrikePrice.Show();

                    Symbol.ReadOnly = true;

                    btnOk.Text = "Sold";
                }
            }
            else if (transactionAction == AssetTransactionAction.Edit)
            {
                Symbol.Text = CurrentSelectedTransaction.AssetSymbol.ToUpper();


                Price.Text = CurrentSelectedTransaction.Price.ToString("C");
                NumberOfShares.Text = CurrentSelectedTransaction.Quantity.ToString();
                transactionDateTime.Value = CurrentSelectedTransaction.TransactionDate;
                Fee.Text = CurrentSelectedTransaction.Fee.ToString("C");

                if (CurrentSelectedTransaction.TransactionType == AssetTransactionType.CoveredCall ||
                    CurrentSelectedTransaction.TransactionType == AssetTransactionType.CoveredPut)
                {
                    expiredDatePicker.Value = CurrentSelectedTransaction.ExpiredDate;
                    tbStrikePrice.Text = CurrentSelectedTransaction.StrikePrice.ToString("C");

                    lblExpiredDate.Show();
                    expiredDatePicker.Show();

                    lblStrikePrice.Show();
                    tbStrikePrice.Show();
                }
                else
                {
                    lblExpiredDate.Hide();
                    expiredDatePicker.Hide();

                    lblStrikePrice.Hide();
                    tbStrikePrice.Hide();
                }


                Symbol.ReadOnly = true;

                btnOk.Text = "Update";
            }
            else if (transactionAction == AssetTransactionAction.Assigned &&
                CurrentSelectedTransaction.TransactionType == AssetTransactionType.CoveredCall)
            {
                Symbol.Text = CurrentSelectedTransaction.AssetSymbol.ToUpper();


                Price.Text = CurrentSelectedTransaction.StrikePrice.ToString("C");

                NumberOfShares.Text = CurrentSelectedTransaction.Quantity.ToString();

                transactionDateTime.Value = DateTime.Now;
                Fee.Text = CurrentSelectedTransaction.Fee.ToString("C");

                lblExpiredDate.Hide();
                expiredDatePicker.Hide();

                lblStrikePrice.Hide();
                tbStrikePrice.Hide();

                NumberOfShares.ReadOnly = true;
                Price.ReadOnly = true;
                Symbol.ReadOnly = true;

                btnOk.Text = "Assigned";
            }
        }
    }
}
