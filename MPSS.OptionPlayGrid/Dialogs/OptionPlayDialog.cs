﻿using MPSS.Common.Model;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace MPSS.OptionPlayControl
{
    /// <summary>
    /// This is the Option Play Dialog form
    /// </summary>
    public partial class OptionPlayDialog : Form
    {
        #region Properties
        public AssetTransaction transaction;
        public bool IsEditDialog { get; set; } = false;
        #endregion Properties

        #region Constructors
        /// <summary>
        /// This is an empty constructor, basically VS Designer needs it
        /// </summary>
        public OptionPlayDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// This is the constructor, which the application invokes when user clicks
        /// on the Option Play button
        /// </summary>
        /// <param name="currentSelectedAsset">Currently selected stock object</param>
        /// <param name="currentSelectedTransaction">Currently selected transaction</param>
        public OptionPlayDialog(StockAsset currentSelectedAsset, AssetTransaction currentSelectedTransaction)
        {
            // Let's create a transaction to preset all value from the
            //  selecting rows
            transaction = new AssetTransaction(currentSelectedTransaction, currentSelectedAsset)
            {
                // Override few properties
                ParentTransactionId = 0,
                Active = true,
                Hide = false,
                Status = AssetTransactionStatus.Open,
                Message = string.Empty,

                // Override the constructor
                Quantity = (currentSelectedAsset != null) ? currentSelectedAsset.Shares : 0
            };

            InitializeComponent();

            ContractsNumericUpDown.Minimum = 1;
        }

        /// <summary>
        /// Constructor to edit an existing transaction
        /// </summary>
        /// <param name="t">Transaction to edit</param>
        public OptionPlayDialog(AssetTransaction t)
        {
            transaction = t;
            IsEditDialog = true;
            InitializeComponent();
        }
        #endregion Constructors

        #region Event Hanlders
        private void OkButton_Click(object sender, EventArgs e)
        {
            // Let's transfer what user enters/changes into 
            //  the transaction object, so that we can
            //  pass it back to the caller to persist in the
            //  database
            if (!IsEditDialog)
            {
                transaction.TransactionType = GetTransactionType();
                transaction.TransactionAction = BoughtRadioButton.Checked ? AssetTransactionAction.Bought : AssetTransactionAction.Sold;
                transaction.AssetSymbol = SymbolTextBox.Text.ToUpper();
            }
            transaction.TransactionDate = TransactionDatePicker.Value;
            transaction.Quantity = (int)(ContractsNumericUpDown.Value * 100);
            transaction.Price = decimal.TryParse(PriceTextBox.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal p) ? p : 0;
            transaction.Fee = decimal.TryParse(FeeTextBox.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal f) ? f : 0;
            transaction.StrikePrice = decimal.TryParse(StrikePriceTextBox.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal sp) ? sp : 0;
            transaction.ExpiredDate = ExpiredDatePicker.Value;

            if (transaction.TransactionType == AssetTransactionType.CreditSpread ||
                transaction.TransactionType == AssetTransactionType.DebitSpread)
            {
                // update transaction properties
                UpdateSpreadPlayProperties();

                // var secondaryStrikePrice = decimal.TryParse(SecondaryStrikePriceTextBox.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out decimal ssp) ? ssp : 0;
                transaction.Message = $"Spread play {transaction.OptionSpreadType.ToString().ToUpper()}, strike at {transaction.LowerStrikePrice:C} and {transaction.UpperStrikePrice:C}.";
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void UpdateSpreadPlayProperties()
        {
            var primaryStrikePrice = Convert.ToDecimal(StrikePriceTextBox.Text);
            var secondaryStrikePrice = Convert.ToDecimal(SecondaryStrikePriceTextBox.Text);
            transaction.UpdateUpperStrikePrice(Math.Max(primaryStrikePrice, secondaryStrikePrice));
            transaction.UpdateLowerStrikePrice(Math.Min(primaryStrikePrice, secondaryStrikePrice));

            transaction.UpdateSpreadOptionType(SpreadOptionCall.Checked
                ? SpreadOptionType.Call
                : SpreadOptionType.Put);
        }

        private void OptionPlayDialog_Load(object sender, EventArgs e)
        {
            if (IsEditDialog)
            {
                PrepareDialogForEdit();
            }
            else
            {
                PrepareDialogForCreate();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void CreditSpreadRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            // Set the SOLD action
            SoldRadioButton.Checked = true;

            // Disable the transaction action buttons, so user cannot select
            BoughtRadioButton.Enabled = false;
            SoldRadioButton.Enabled = false;

            // Make the secondary strike price visible
            SecondaryStrikePriceLabel.Visible = true;
            SecondaryStrikePriceTextBox.Visible = true;

            // Enable Spread Type
            SpreadTypeGroupBox.Enabled = true;
            SpreadOptionCall.Enabled = true;
            SpreadOptionPut.Enabled = true;
        }

        private void DebitSpreadRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            // Set the BOUGHT action
            BoughtRadioButton.Checked = true;

            // Disable the transaction action buttons, so user cannot select
            BoughtRadioButton.Enabled = false;
            SoldRadioButton.Enabled = false;

            // Make the secondary strike price visible
            SecondaryStrikePriceLabel.Visible = true;
            SecondaryStrikePriceTextBox.Visible = true;

            // Enable Spread Type
            SpreadTypeGroupBox.Enabled = true;
            SpreadOptionCall.Enabled = true;
            SpreadOptionPut.Enabled = true;
        }

        private void CallRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (CallRadioButton.Checked)
            {
                // Disable the transaction action buttons, so user cannot select
                BoughtRadioButton.Enabled = true;
                SoldRadioButton.Enabled = true;

                // Hide the secondary strike price 
                SecondaryStrikePriceLabel.Visible = false;
                SecondaryStrikePriceTextBox.Visible = false;

                // Disable Spread Type
                SpreadTypeGroupBox.Enabled = false;
                SpreadOptionCall.Enabled = false;
                SpreadOptionPut.Enabled = false;
            }
        }

        private void PutRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (PutRadioButton.Checked)
            {
                // Disable the transaction action buttons, so user cannot select
                BoughtRadioButton.Enabled = true;
                SoldRadioButton.Enabled = true;

                // Hide the secondary strike price 
                SecondaryStrikePriceLabel.Visible = false;
                SecondaryStrikePriceTextBox.Visible = false;

                // Disable Spread Type
                SpreadTypeGroupBox.Enabled = false;
                SpreadOptionCall.Enabled = false;
                SpreadOptionPut.Enabled = false;
            }
        }
        #endregion

        #region Private Methods
        private void PrepareDialogForEdit()
        {
            SetupTransactionType();
            CallRadioButton.Enabled =
                PutRadioButton.Enabled =
                DebitSpreadRadioButton.Enabled =
                CreditSpreadRadioButton.Enabled = false;

            if (transaction.IsSpreadPlay)
            {
                SetupSpreadOptionType();
            }

            SetupTransactionAction();

            TransactionDatePicker.Value = transaction.TransactionDate;

            SymbolTextBox.Text = transaction.AssetSymbol;
            SymbolTextBox.ReadOnly = true;

            ContractsNumericUpDown.Value = transaction.Quantity / 100;

            PriceTextBox.Text = $"{transaction.Price}";

            FeeTextBox.Text = $"{transaction.Fee}";

            ExpiredDatePicker.Value = transaction.ExpiredDate;

            StrikePriceTextBox.Text = $"{transaction.LowerStrikePrice}";

            SecondaryStrikePriceTextBox.Text = $"{transaction.UpperStrikePrice}";
        }

        private void SetupSpreadOptionType()
        {
            SpreadOptionCall.Checked =
                SpreadOptionPut.Checked = false;

            SpreadOptionCall.Checked = transaction.OptionSpreadType == SpreadOptionType.Call;
            SpreadOptionPut.Checked = transaction.OptionSpreadType == SpreadOptionType.Put;
        }

        private void SetupTransactionType()
        {
            CallRadioButton.Checked =
                PutRadioButton.Checked =
                DebitSpreadRadioButton.Checked =
                CreditSpreadRadioButton.Checked = false;
            switch (transaction.TransactionType)
            {
                case AssetTransactionType.CoveredCall:
                    CallRadioButton.Checked = true;
                    break;
                case AssetTransactionType.CoveredPut:
                    PutRadioButton.Checked = true;
                    break;
                case AssetTransactionType.DebitSpread:
                    DebitSpreadRadioButton.Checked = true;
                    SetupSpreadType();
                    break;
                case AssetTransactionType.CreditSpread:
                    CreditSpreadRadioButton.Checked = true;
                    SetupSpreadType();
                    break;
            }
        }

        private void SetupSpreadType()
        {
            SpreadOptionCall.Checked =
                transaction.OptionSpreadType == SpreadOptionType.Call;
            SpreadOptionPut.Checked =
                transaction.OptionSpreadType == SpreadOptionType.Put;
        }

        private void SetupTransactionAction()
        {
            BoughtRadioButton.Checked =
                transaction.TransactionAction == AssetTransactionAction.Bought;
            SoldRadioButton.Checked =
                transaction.TransactionAction == AssetTransactionAction.Sold;
        }

        private void PrepareDialogForCreate()
        {
            // Let's set some default data 
            SoldRadioButton.Checked = true;
            CallRadioButton.Checked = true;

            //  If the stock is available, use it
            //  Else use the transaction
            SymbolTextBox.Text = transaction.AssetSymbol;

            // Set the contract number based on the available
            //  shares
            ContractsNumericUpDown.Value = Math.Max(transaction.Quantity / 100, 1);

            FeeTextBox.Text = $"{0M:C}";

            // Disable Spread Type
            SpreadTypeGroupBox.Enabled = false;
            SpreadOptionCall.Enabled = false;
            SpreadOptionPut.Enabled = false;
        }

        private AssetTransactionType GetTransactionType()
        {
            return CallRadioButton.Checked ?
                AssetTransactionType.CoveredCall :
                PutRadioButton.Checked ?
                AssetTransactionType.CoveredPut :
                CreditSpreadRadioButton.Checked ?
                AssetTransactionType.CreditSpread :
                AssetTransactionType.DebitSpread;
        }
        #endregion
    }
}
