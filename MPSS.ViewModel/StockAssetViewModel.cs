﻿using MPSS.Common.DataProvider;
using MPSS.Common.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace MPSS.ViewModel
{
    /// <summary>
    /// Representing a stock asset View Model
    /// </summary>
    public class StockAssetViewModel
    {
        private readonly IStockAssetDataProvider dataProvider;
        public StockAsset currentSelectedAsset { get; private set; }
        public BindingList<StockAsset> StockAssets = new();

        public AssetTransaction currentSelectedTransaction { get; private set; }

        public StockAssetViewModel(IStockAssetDataProvider provider)
        {
            this.dataProvider = provider;
        }

        /// <summary>
        /// Use the data provider to load the stock assets into the StockAssets list
        /// </summary>
        public void Load()
        {
            foreach(var stockAsset in dataProvider.LoadStockAssets())
            {
                this.StockAssets.Add(stockAsset);

                // Scan & auto-expire an active option play transaction in this
                //  stock
                foreach (var t in stockAsset.GetExpiringOptionPlayTransactions())
                {
                    Expiring(t);
                }
            }

            currentSelectedAsset = StockAssets.FirstOrDefault();
            currentSelectedTransaction = currentSelectedAsset?.Transactions.FirstOrDefault();
        }

        /// <summary>
        /// Expiring an active option play transaction
        /// </summary>
        /// <param name="parent">Expiring option play transaction</param>
        private void Expiring(AssetTransaction parent)
        {
            var child = new AssetTransaction(parent);
            child.Expire();
            CloseTransaction(parent, child);
        }

        public AssetTransaction AddTransaction(AssetTransaction t)
        {
            currentSelectedAsset = StockAssets.FirstOrDefault(s => s.Symbol == t.AssetSymbol);
            if(currentSelectedAsset == null)
            {
                currentSelectedAsset = dataProvider.AddNewStockAsset(t);
                this.StockAssets.Add(currentSelectedAsset);                
            }
            else
            {
                t.Stock = currentSelectedAsset;
                currentSelectedTransaction = dataProvider.AddAssetTransaction(t);

                currentSelectedAsset.Transactions.Add(currentSelectedTransaction);

                if(t.TransactionType == AssetTransactionType.Share)
                {
                    currentSelectedAsset.Shares += (t.TransactionAction == AssetTransactionAction.Bought) ?
                        t.Quantity : (-1 * t.Quantity);
                }                       
            }
            return currentSelectedTransaction;
        }

        public void UpdateTransaction(AssetTransaction currentSelectedTransaction)
        {
            dataProvider.UpdateTransaction(currentSelectedTransaction);
        }

        public StockAsset GetSelectedAsset()
        {
            return currentSelectedAsset;
        }

        public void SelectStockAsset(StockAsset selectedAsset)
        {
            currentSelectedAsset = StockAssets.FirstOrDefault(a => a.StockAssetId == selectedAsset.StockAssetId);
        }

        public void SelectTransaction(AssetTransaction selectedTransaction)
        {
            currentSelectedTransaction = currentSelectedAsset.Transactions.FirstOrDefault(t => t.TransactionId == selectedTransaction.TransactionId);
        }

        /// <summary>
        /// Remove the currently select transaction
        /// </summary>
        public void DeleteTransaction()
        {
            if (TransactionHasPartner(out AssetTransaction partner))
            {
                partner.Active = true;
                UpdateTransaction(partner);
            }
            
            // Let's find and remove it from the current stock's transaction list
            currentSelectedAsset.Transactions.Remove(currentSelectedTransaction);

            // Now, we can tell the database to remove it from the transaction table
            dataProvider.DeleteTransaction(currentSelectedTransaction.TransactionId);

            // We need to recalculate the number of shares, for this stock; basically
            //  we re-iterate through all the transaction, and if it's a Bought, we add
            //  if it's a Sold, we subtract
            currentSelectedAsset.Shares = UpdateStockShare(currentSelectedAsset);

            // Since the currentSelectedTransaction was removed, we need to pick a new one, so
            // let's pick the first one
            currentSelectedTransaction = currentSelectedAsset.Transactions.FirstOrDefault();
        }

        /// <summary>
        /// Check to see if this transaction is part of pair
        /// </summary>
        /// <param name="partner">The partner transaction</param>
        /// <returns>True if this transactin is part of a pair, false otherwise</returns>
        private bool TransactionHasPartner(out AssetTransaction partner)
        {
            partner = null;
            if (currentSelectedTransaction.HasParentTransaction())
            {
                partner = currentSelectedAsset
                    .Transactions
                    .FirstOrDefault(t => t.TransactionId == currentSelectedTransaction.ParentTransactionId);
            }
            else if (currentSelectedTransaction.IsParent(currentSelectedAsset.Transactions))
            {
                partner = currentSelectedAsset
                    .Transactions
                    .FirstOrDefault(t => t.ParentTransactionId == currentSelectedTransaction.TransactionId);
            }
            return partner != null;
        }

        public bool IsCurrentSelect(AssetTransactionType transactionType)
        {
            return currentSelectedTransaction.TransactionType == transactionType;
        }

        /// <summary>
        /// Create a Sold Stock transaction, based on the strike price
        /// </summary>
        public int AssignCoveredCall()
        {
            if(currentSelectedTransaction.TransactionType == AssetTransactionType.CoveredCall)
            {
                var t = dataProvider.AddAssetTransaction(
                    new AssetTransaction()
                    {
                        TransactionType = AssetTransactionType.Share,
                        TransactionAction = AssetTransactionAction.Sold,

                        Price = currentSelectedTransaction.StrikePrice,
                        Quantity = currentSelectedTransaction.Quantity,
                        Active = currentSelectedTransaction.Active,
                        AssetSymbol = currentSelectedTransaction.AssetSymbol,
                        ExpiredDate = DateTime.Now,
                        TransactionDate = DateTime.Now,
                        Fee = currentSelectedTransaction.Fee,
                        Hide = currentSelectedTransaction.Hide,
                        Message = currentSelectedTransaction.Message,
                        Status = currentSelectedTransaction.Status,
                        Stock = currentSelectedTransaction.Stock,
                        StrikePrice = 0D
                    });

                currentSelectedAsset = dataProvider.ReloadStockAsset(t.Stock.StockAssetId);
                var stockAsset = StockAssets.FirstOrDefault(s => s.StockAssetId == t.Stock.StockAssetId);
                if(stockAsset != null && currentSelectedAsset.StockAssetId == stockAsset.StockAssetId)
                {
                    stockAsset.Shares = currentSelectedAsset.Shares;
                }
                
                currentSelectedTransaction = currentSelectedAsset.Transactions.FirstOrDefault(r => r.TransactionId == t.TransactionId);
            }
            return currentSelectedTransaction.TransactionId;
        }

        /// <summary>
        /// Remove the stock from the data binding list, and also use the 
        /// dataprovider to remove it from the database
        /// </summary>
        public void RemoveStock()
        {
            // Let's remove it from the database first
            dataProvider.RemoveStockAsset(currentSelectedAsset.StockAssetId);

            // Now we can update the data binding list, which will change 
            //  the data grid in the UI
            StockAssets.Remove(currentSelectedAsset);

            // We need to select a current select asset, if any
            currentSelectedAsset = StockAssets.FirstOrDefault();
        }

        /// <summary>
        /// Creating an option play transaction
        /// </summary>
        /// <param name="optionPlay">Option Play transaction</param>
        public void AddOptionPlay(AssetTransaction optionPlay)
        {
            // First, let's check to see if this option play
            //  belongs to a stock, which already exist
            if(StockAssets.Any(s => s.Symbol == optionPlay.AssetSymbol))
            {
                currentSelectedAsset = dataProvider.AddNewStockAsset(optionPlay.AssetSymbol);
                StockAssets.Add(currentSelectedAsset);
                optionPlay.Stock = currentSelectedAsset;
                currentSelectedTransaction = dataProvider.AddAssetTransaction(optionPlay);
            }
        }

        /// <summary>
        /// Close out parent and child transactions
        /// </summary>
        /// <param name="parent">Parent transaction</param>
        /// <param name="child">Child transaction</param>
        public void CloseTransaction(AssetTransaction parent, AssetTransaction child)
        {
            // TODO
            //  When this is a SOLD-SHARE child transaction
            //  If the parent has more shares than the child transaction, then we need
            //  to auto-generate a split parent transaction, to keep the original parent
            //  transaction active, and adjust the shares correctly.
            if(parent.HasMoreShare(child))
            {
                // First, we need the change the quantity of the original
                //  transaction
                parent.Quantity -= child.Quantity;
                parent.Active = true;
                UpdateTransaction(parent);

                // Create a proxy BOUGHT-SHARE transaction, to serve
                //  as the parent for the child SOLD-SHARE transaction,
                //  so both can be Inactive for P&L calculation
                //  to be correct
                var proxyParent = AddTransaction(
                    new AssetTransaction(parent)
                    {
                        Quantity = child.Quantity,
                        Active = false,
                        TransactionDate = child.TransactionDate
                    });
                // We update the child's parent transaction Id ,to
                //  point to the proxy parent
                child.ParentTransactionId = proxyParent.TransactionId;
            }
            else if(parent.HasSameShare(child))
            {
                // Close out the parent trasaction
                parent.Active = false;
                parent.TransactionDate = child.TransactionDate;
                UpdateTransaction(parent);
            }

            // Add the child transaction
            var childTransaction = AddTransaction(child);

            // Update stock's share quantity
            currentSelectedAsset.Shares = UpdateStockShare(childTransaction.Stock);
        }

        /// <summary>
        /// A function to update stock's share quantity
        /// </summary>
        /// <param name="stock">Stock object</param>
        private int UpdateStockShare(StockAsset stock)
        {
            // We need to recalculate the number of shares, for this stock; basically
            //  we re-iterate through all the transaction, and if it's a Bought, we add
            //  if it's a Sold, we subtract
            int shares = stock.GetNumberOfShares();

            if (shares != stock.Shares)
            {
                stock.Shares = shares;
                dataProvider.UpdateStock(stock);
            }

            return shares;
        }
    }
}
