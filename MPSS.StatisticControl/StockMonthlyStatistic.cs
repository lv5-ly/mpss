﻿using MPSS.Common.Model;
using System;
using System.Linq;
using System.Windows.Forms;

namespace MPSS.StatisticControl
{
    public partial class StockMonthlyStatistic : UserControl
    {
        private StockAsset stockAsset = null;

        public StockMonthlyStatistic()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set the currently select stock asset
        /// </summary>
        /// <param name="currentSelectedAsset">Selected stock asset</param>
        public void SetStockAsset(StockAsset currentSelectedAsset)
        {
            stockAsset = currentSelectedAsset;

            // Set the option play control's stock object, so it can 
            optionPlayControl.CurrentStock = stockAsset;
            // Then tell it to refresh, basically re-calculate all
            //  numbers
            optionPlayControl.Refresh();
        }

        /// <summary>
        /// Calculate and upate the values in all fields in the panel
        /// </summary>
        public void RefreshStockStatistics()
        {
            int currentMonth = DateTime.Now.Month;
            int currentYear = DateTime.Now.Year;
            if (stockAsset != null && 
                stockAsset.Transactions != null &&
                stockAsset.Transactions.Count > 0)
            {
                // For the case of the user enters only naked calls/puts, the stock does not 
                //  have a bought-share transaction, that means, average share price cannot
                //  be calculated. Also, it's best for the StockAsset object to do this 
                //  type of calculation, because it has a list of transactions.
                tbxAverageStockPrice.Text = stockAsset.GetAverageStockPrice().ToString("C");

                txbTicker.Text = stockAsset.Symbol;

                int s = stockAsset.Transactions
                    .Where(t => (t.TransactionAction == AssetTransactionAction.Bought) && (t.TransactionType == AssetTransactionType.Share))
                    .Select(t => t.Quantity)
                    .Sum();
                txbShares.Text = s.ToString("N");

                /// Populate P&L text box
                double PandL = stockAsset.GetMonthlyPandL(currentMonth, currentYear);
                txbMonthlyPL.Text = PandL.ToString("C");

                /// Populate P&L %
                /// Note: 
                ///     If the average is 0, that means the stock does not have a
                ///     transaction, of which it bought the shares. The stock may
                ///     only have option play transactions.
                double PandLRatio = 0.0;
                if (stockAsset.GetAverageStockPrice() != 0)
                {
                    PandLRatio = PandL / (stockAsset.GetAverageStockPrice() * s);
                }
                txbMonthlyPLRatio.Text = PandLRatio.ToString("P");
            }
        }

        /*
        private void label2_Click(object sender, EventArgs e)
        {

        }
        
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        */
    }
}
