﻿
namespace MPSS.StatisticControl
{
    partial class StockMonthlyStatistic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optionPlayControl = new MPSS.OptionPlayControl.OptionPlayView();
            this.label5 = new System.Windows.Forms.Label();
            this.txbMonthlyPLRatio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbMonthlyPL = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbShares = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txbTicker = new System.Windows.Forms.TextBox();
            this.tbxAverageStockPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.optionPlayControl);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txbMonthlyPLRatio);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txbMonthlyPL);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txbShares);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txbTicker);
            this.groupBox1.Controls.Add(this.tbxAverageStockPrice);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.groupBox1.Size = new System.Drawing.Size(1027, 506);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stock Monthly Statistics";
            // 
            // optionPlayControl
            // 
            this.optionPlayControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.optionPlayControl.Location = new System.Drawing.Point(5, 138);
            this.optionPlayControl.Margin = new System.Windows.Forms.Padding(0);
            this.optionPlayControl.Name = "optionPlayControl";
            this.optionPlayControl.Size = new System.Drawing.Size(1017, 362);
            this.optionPlayControl.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(770, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(158, 30);
            this.label5.TabIndex = 9;
            this.label5.Text = "Monthly P&&L %";
            // 
            // txbMonthlyPLRatio
            // 
            this.txbMonthlyPLRatio.Location = new System.Drawing.Point(770, 70);
            this.txbMonthlyPLRatio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbMonthlyPLRatio.Name = "txbMonthlyPLRatio";
            this.txbMonthlyPLRatio.ReadOnly = true;
            this.txbMonthlyPLRatio.Size = new System.Drawing.Size(175, 35);
            this.txbMonthlyPLRatio.TabIndex = 8;
            this.txbMonthlyPLRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(600, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 30);
            this.label4.TabIndex = 7;
            this.label4.Text = "Monthly P&&L";
            // 
            // txbMonthlyPL
            // 
            this.txbMonthlyPL.Location = new System.Drawing.Point(600, 70);
            this.txbMonthlyPL.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbMonthlyPL.Name = "txbMonthlyPL";
            this.txbMonthlyPL.ReadOnly = true;
            this.txbMonthlyPL.Size = new System.Drawing.Size(146, 35);
            this.txbMonthlyPL.TabIndex = 6;
            this.txbMonthlyPL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(333, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 30);
            this.label3.TabIndex = 5;
            this.label3.Text = "Shares";
            // 
            // txbShares
            // 
            this.txbShares.Location = new System.Drawing.Point(317, 70);
            this.txbShares.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbShares.Name = "txbShares";
            this.txbShares.ReadOnly = true;
            this.txbShares.Size = new System.Drawing.Size(109, 35);
            this.txbShares.TabIndex = 4;
            this.txbShares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 30);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ticker";
            // 
            // txbTicker
            // 
            this.txbTicker.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txbTicker.Location = new System.Drawing.Point(171, 70);
            this.txbTicker.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbTicker.Name = "txbTicker";
            this.txbTicker.ReadOnly = true;
            this.txbTicker.Size = new System.Drawing.Size(126, 35);
            this.txbTicker.TabIndex = 2;
            this.txbTicker.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbxAverageStockPrice
            // 
            this.tbxAverageStockPrice.BackColor = System.Drawing.SystemColors.Control;
            this.tbxAverageStockPrice.Location = new System.Drawing.Point(449, 70);
            this.tbxAverageStockPrice.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.tbxAverageStockPrice.Name = "tbxAverageStockPrice";
            this.tbxAverageStockPrice.ReadOnly = true;
            this.tbxAverageStockPrice.Size = new System.Drawing.Size(119, 35);
            this.tbxAverageStockPrice.TabIndex = 1;
            this.tbxAverageStockPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(441, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Average Price";
            // 
            // StockMonthlyStatistic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "StockMonthlyStatistic";
            this.Size = new System.Drawing.Size(1027, 506);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxAverageStockPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbTicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbShares;
        private System.Windows.Forms.TextBox txbMonthlyPL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txbMonthlyPLRatio;
        private OptionPlayControl.OptionPlayView optionPlayControl;
    }
}
